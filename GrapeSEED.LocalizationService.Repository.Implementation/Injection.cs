﻿using GrapeSEED.LocalizationService.Repository.Implementation.Concretes;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace GrapeSEED.LocalizationService.Repository.Implementation
{
    public static class RepositoryServicesInjection
    {
        public static IServiceCollection RegisterRepositoryServices(this IServiceCollection services)
        {
            services
                .AddScoped<RepositoryContextBase, RepositoryContext>()
                .AddScoped(typeof(IGeneralRepository<>), typeof(GeneralRepository<>))
                .AddScoped(typeof(IGeneralRepository<,>), typeof(GeneralRepository<,>))
                .AddScoped(typeof(ICrudRepository<>), typeof(CrudRepository<>))
                .AddScoped(typeof(ICrudRepository<,>), typeof(CrudRepository<,>))
                .AddScoped(typeof(IRelationsRepository<>), typeof(RelationsRepository<>))
                .AddScoped<IStoredProcedureRespository, StoredProcedureRepository>()
                .AddScoped<IApplicationRepository, ApplicationRepository>();
            
            return services;
        }
    }
}
