﻿using GrapeSEED.LocalizationService.Repository.Shared;

namespace GrapeSEED.LocalizationService.Repository.Implementation
{
    public class RepositoryContext : RepositoryContextBase
    {
        public RepositoryContext(gclocalizationdbContext dbContext) : base(dbContext)
        {

        }
    }
}
