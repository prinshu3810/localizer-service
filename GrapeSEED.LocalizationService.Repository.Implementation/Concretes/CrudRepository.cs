﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Foundation.Utils;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Repository.Implementation.Concretes
{
    public class CrudRepository<TEntity> : CrudRepository<TEntity, int>, ICrudRepository<TEntity>
        where TEntity : class, IEntityBase
    {
        public CrudRepository(gclocalizationdbContext dbContext) : base(dbContext)
        {
        }
    }

    public class CrudRepository<TEntity, TKey> : ICrudRepository<TEntity, TKey>
        where TEntity : class, IEntityBase
        where TKey : IEquatable<TKey>
    {
        protected gclocalizationdbContext DbContext { get; private set; }

        public CrudRepository(gclocalizationdbContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual Task AddAsync(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual Task AddMultipleAsync(IEnumerable<TEntity> entity)
        {
            DbContext.Set<TEntity>().AddRange(entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(TKey id)
        {
            TEntity entity = await this.GetByIdAsync(id);
            await this.DeleteAsync(entity);
        }

        public virtual Task DeleteMultipleAsync(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().RemoveRange(entities);
            return DbContext.SaveChangesAsync();
        }

        public virtual Task EditAsync(TEntity entity, params string[] properties)
        {
            if (entity is IAuditedEntityBase)
            {
                (entity as IAuditedEntityBase).UpdatedDate = DateTimeUtils.UtcDateTime;
            }

            DbContext.Set<TEntity>().Attach(entity);
            foreach (var prop in properties)
            {
                DbContext.Entry(entity).Property(prop).IsModified = true;
            }

            return DbContext.SaveChangesAsync();
        }

        public virtual Task EditAsync(TEntity entity)
        {
            if (entity is IAuditedEntityBase)
            {
                (entity as IAuditedEntityBase).UpdatedDate = DateTimeUtils.UtcDateTime;
            }

            DbContext.Entry(entity).State = EntityState.Modified;
            return DbContext.SaveChangesAsync();
        }

        public virtual Task EditMultipleAsync(IEnumerable<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                if (entity is IAuditedEntityBase)
                {
                    (entity as IAuditedEntityBase).UpdatedDate = DateTimeUtils.UtcDateTime;
                }
            }

            DbContext.Set<TEntity>().UpdateRange(entities);
            return DbContext.SaveChangesAsync();
        }

        public Task EditMultipleAsync(IEnumerable<TEntity> entities, params string[] properties)
        {
            if (!entities.Any())
            {
                return Task.FromResult(0);
            }
            foreach (var entity in entities)
            {
                DbContext.Set<TEntity>().Attach(entity);
                foreach (var prop in properties)
                {
                    DbContext.Entry(entity).Property(prop).IsModified = true;
                }
            }
            return DbContext.SaveChangesAsync();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>().AsQueryable();
        }

        public virtual Task<IQueryable<TEntity>> GetAllAsync()
        {
            return Task.FromResult(this.GetAll());
        }

        public virtual TEntity GetById(TKey id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public virtual Task<TEntity> GetById(params object[] ids)
        {
            return DbContext.Set<TEntity>().FindAsync(ids);
        }

        public virtual Task<TEntity> GetByIdAsync(TKey id)
        {
            return DbContext.Set<TEntity>().FindAsync(id);
        }
    }
}
