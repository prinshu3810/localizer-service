﻿using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Repository.Implementation.Concretes
{
    public class ApplicationRepository : GeneralRepository<Application>, IApplicationRepository
    {
        private readonly gclocalizationdbContext _dbContext;

        public ApplicationRepository(gclocalizationdbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<dynamic> GetUngroupedKeysCount()
        {
            var textAppUngroupedKeys = await (
                        from app in _dbContext.Application.AsNoTracking()
                        join section in _dbContext.Section.AsNoTracking()
                        on app.Id equals section.ApplicationId
                        into appsection
                        from aps in appsection.DefaultIfEmpty()
                        join text in _dbContext.Text.AsNoTracking()
                        on aps.Id equals text.SectionId
                        into appsectiontext
                        from apst in appsectiontext.DefaultIfEmpty()
                        where (app.IsVisible && app.TranslationTypeId == (int)TranslationTypeId.Text)
                        group apst by app.Id into resultText
                        select new
                        {
                            AppId = resultText.Key,
                            UngroupedKeys = resultText != null ? resultText.Count(r => r != null && r.GroupId == null) : 0
                        }
                    ).ToListAsync();

            var pdfAppUngroupedKeys = await(
                        from app in _dbContext.Application.AsNoTracking()
                        join section in _dbContext.Section.AsNoTracking()
                        on app.Id equals section.ApplicationId
                        into appsection
                        from aps in appsection.DefaultIfEmpty()
                        join pdf in _dbContext.Pdf.AsNoTracking()
                        on aps.Id equals pdf.SectionId
                        into appsectionpdf
                        from apsp in appsectionpdf.DefaultIfEmpty()
                        where (app.IsVisible && app.TranslationTypeId == (int)TranslationTypeId.Pdf)
                        group apsp by app.Id into resultPdf
                        select new
                        {
                            AppId = resultPdf.Key,
                            UngroupedKeys = resultPdf != null ? resultPdf.Count(r => r != null && r.GroupId == null) : 0
                        }
                        ).ToListAsync();

            return new { textAppUngroupedKeys, pdfAppUngroupedKeys };
        }
    }
}
