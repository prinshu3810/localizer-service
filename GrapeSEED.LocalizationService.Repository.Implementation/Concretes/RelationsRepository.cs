﻿using System;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Repository.Implementation.Concretes
{
    public class RelationsRepository<TEntity> : GeneralRepository<TEntity>, IRelationsRepository<TEntity>
        where TEntity : class, IEntityBase
    {
        private const string EXCEPTION_MESSAGE = "No single primary key column present on this entity. Use the composite key overload.";
        
        public RelationsRepository(gclocalizationdbContext dbContext) : base(dbContext)
        {
        }

        public override TEntity GetById(int id)
        {
            throw new InvalidOperationException(EXCEPTION_MESSAGE);
        }

        public override Task<TEntity> GetByIdAsync(int id)
        {
            return Task.FromResult(this.GetById(id));
        }
    }
}
