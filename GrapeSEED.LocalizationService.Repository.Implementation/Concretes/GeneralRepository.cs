﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Repository.Implementation.Concretes
{
    public class GeneralRepository<TEntity> : GeneralRepository<TEntity, int>, IGeneralRepository<TEntity>
        where TEntity : class, IEntityBase
    {
        public GeneralRepository(gclocalizationdbContext dbContext) : base(dbContext)
        {

        }
    }

    public class GeneralRepository<TEntity, TKey> : CrudRepository<TEntity, TKey>, IGeneralRepository<TEntity, TKey>
        where TEntity : class, IEntityBase
        where TKey : IEquatable<TKey>
    {
        public GeneralRepository(gclocalizationdbContext dbContext) : base(dbContext)
        {
        }

        public virtual Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().AnyAsync(predicate);
        }

        public virtual IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().Where(predicate);
        }

        public virtual Task<IQueryable<TEntity>> GetAllIncludingAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] propertySelectors)
        {
            IQueryable<TEntity> query = DbContext.Set<TEntity>().AsQueryable();
            if(predicate != null)
            {
                query = query.Where(predicate);
            }
            propertySelectors?.ToList()?.ForEach(s => query = query.Include(s));
            return Task.FromResult(query);
        }

        public virtual IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] propertySelectors)
        {
            IQueryable<TEntity> query = DbContext.Set<TEntity>().AsQueryable();
            propertySelectors?.ToList()?.ForEach(s => query = query.Include(s));
            return query;
        }

        public virtual Task SaveChangesAsync()
        {
            return DbContext.SaveChangesAsync();
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().FirstOrDefault(predicate);
        }

        public virtual Task<List<TEntity>> List(Expression<Func<TEntity, bool>> predicate = null, bool needTracking = false)
        {
            IQueryable<TEntity> query = needTracking ? DbContext.Set<TEntity>().AsQueryable() : DbContext.Set<TEntity>().AsQueryable().AsNoTracking();
            return query.Where(predicate).ToListAsync();
        }
    }
}
