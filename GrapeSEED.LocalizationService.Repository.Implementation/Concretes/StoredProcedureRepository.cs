﻿using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using System.Data.Common;

namespace GrapeSEED.LocalizationService.Repository.Implementation.Concretes
{
    public class StoredProcedureRepository : IStoredProcedureRespository
    {
        private readonly gclocalizationdbContext _dbContext;

        public StoredProcedureRepository(gclocalizationdbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbCommand GetStoredProcedure(string name, params (string, object)[] nameValueParams)
        {
            return _dbContext
                        .LoadStoredProcedure(name)
                        .WithSqlParams(nameValueParams);
        }

        public DbCommand GetStoredProcedure(string name)
        {
            return _dbContext.LoadStoredProcedure(name);
        }
    }
}
