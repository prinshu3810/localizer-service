﻿using GrapeSEED.LocalizationService.Domain.Implementation.Concretes;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace GrapeSEED.LocalizationService.Domain.Implementation
{
    public static class DomainServicesInjection
    {
        public static IServiceCollection RegisterDomainServices(this IServiceCollection services)
        {
            return services
                    .AddScoped<IUtilsManager, UtilsManager>()
                    .AddScoped<ITranslationManager, TranslationManager>()
                    .AddScoped<ISectionManager, SectionManager>()
                    .AddScoped<IApplicationManager, ApplicationManager>()
                    .AddScoped<ILocalizationStorageManager, LocalizationStorageManager>()
                    .AddScoped<INotificationManager, NotificationManager>()
                    .AddScoped<IUserManager, UserManager>()
                    .AddScoped<ILanguageManager, LanguageManager>()
                    .AddScoped<IRoleManager, RoleManager>()
                    .AddScoped<IGroupManager, GroupManager>();

        }
    }
}
