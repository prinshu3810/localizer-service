﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Helper
{
    public class BlobStorage
    {
        //private readonly ILogger<BlobEntities> _logger;
        private readonly CloudStorageAccount _account;
        private StorageCredentials storageCredentials;
        private StorageCredentials amsStorageCredentials;

        public string StorageUrl { get { return "_options.Value.MSAzureBlobCDNEndPoint.TrimEnd('/')"; } }

        public BlobStorage()
        {
            // _options = options;

            //storageCredentials = new StorageCredentials("options.Value.AccountName", "options.Value.AccountKey");
            //amsStorageCredentials = new StorageCredentials("options.Value.AMSAccountName", "options.Value.AMSAccountKey");
            // _account = new CloudStorageAccount(storageCredentials, false);
            //_account = new CloudStorageAccount(storageCredentials, false);

        }

        public StorageCredentials StorageCredentials
        {
            get
            {
                return storageCredentials;
            }
            set
            {
                storageCredentials = value;
            }
        }
        public StorageCredentials AMSStorageCredentials
        {
            get
            {
                return amsStorageCredentials;
            }
            set
            {
                amsStorageCredentials = value;
            }
        }
        public string GetSharedAccessSignature(string container, string blob)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);
            CloudBlob cloudBlob = cloudBlobContainer.GetBlobReference(blob);
            return cloudBlob.GetSharedAccessSignature(GeneratePolicy(30));
        }

        public Task<CloudBlob> GetBlobFile(string container, string blob)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);

            CloudBlob cloudBlob = cloudBlobContainer.GetBlobReference(blob);
            return Task.FromResult(cloudBlob);
        }

        public string GetSharedAccessSignature(string container)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);

            return cloudBlobContainer.GetSharedAccessSignature(GeneratePolicy(30));
        }

        public async Task CreateOrReplaceBlob(string container, string blob, Stream stream)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);

            await cloudBlobContainer.CreateIfNotExistsAsync();

            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blob);

            stream.Seek(0, SeekOrigin.Begin);

            await blockBlob.UploadFromStreamAsync(stream);
        }

        public CloudBlockBlob GetBlobReference(string container, string blob)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);

            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blob);

            return blockBlob;
        }
        public CloudBlockBlob GetAMSBlobReference(string container, string blob)
        {
            CloudStorageAccount _amsAccount = new CloudStorageAccount(amsStorageCredentials, false);

            CloudBlobClient client = _amsAccount.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container);

            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blob);

            return blockBlob;
        }
        public async Task CopyBlobToAMSStorage(string containerId, string blobId)
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();
            var sourceContainer = client.GetContainerReference(containerId);
            var sourceBlob = sourceContainer.GetBlockBlobReference(blobId);
            // Get SAS of that policy.
            var sourceBlobToken = sourceBlob.GetSharedAccessSignature(GeneratePolicy(30));
            // Make a full uri with the sas for the blob.
            var sourceBlobSAS = string.Format("{0}{1}", sourceBlob.Uri, sourceBlobToken);
            CloudStorageAccount _amsAccount = new CloudStorageAccount(amsStorageCredentials, false);
            CloudBlobClient amsClient = _amsAccount.CreateCloudBlobClient();
            var targetContainer = amsClient.GetContainerReference(containerId);
            await targetContainer.CreateIfNotExistsAsync();
            var targetBlob = targetContainer.GetBlockBlobReference(blobId);
            await targetBlob.DeleteIfExistsAsync();
            await targetBlob.StartCopyAsync(new Uri(sourceBlobSAS));
        }
        public SharedAccessBlobPolicy GeneratePolicy(int durationMinutes = 30)
        {
            SharedAccessBlobPolicy sharedAccessPolicy = new SharedAccessBlobPolicy();
            sharedAccessPolicy.Permissions = SharedAccessBlobPermissions.Read;
            sharedAccessPolicy.SharedAccessStartTime = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            sharedAccessPolicy.SharedAccessExpiryTime = DateTime.UtcNow + TimeSpan.FromMinutes(durationMinutes);

            return sharedAccessPolicy;
        }

    }
}
