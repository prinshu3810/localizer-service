﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Domain.Implementation
{
    public class Functions
    {
        public static async Task<T1> Retry<T1>(Func<Task<T1>> func, TimeSpan retryInterval, int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    return await func();
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                    Thread.Sleep(retryInterval);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
