﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class SectionManager : ISectionManager
    {
        private readonly IGeneralRepository<Section> _sectionRepository;
        private readonly IGeneralRepository<Text> _textRepository;
        private readonly IGeneralRepository<Pdf> _pdfRepository;
        private readonly IGeneralRepository<TextTranslation> _textTranslationRepository;
        private readonly IGeneralRepository<PdfTranslation> _pdfTranslationRepository;
        private readonly IRelationsRepository<LanguageSectionIgnore> _langSectionIgnoreRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SectionManager"/> class.
        /// </summary>
        /// <param name="sectionRepository">The section repository.</param>
        /// <param name="textRepository">The text repository</param>
        /// <param name="pdfRepository">The pdf repository.</param>
        /// <param name="textTranslationRepository">The text translation repository.</param>
        /// <param name="pdfTranslationRepository">The pdf translation repository.</param>
        public SectionManager(
            IGeneralRepository<Section> sectionRepository,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<Pdf> pdfRepository,
            IGeneralRepository<TextTranslation> textTranslationRepository,
            IGeneralRepository<PdfTranslation> pdfTranslationRepository, 
            IRelationsRepository<LanguageSectionIgnore> langSectionIgnoreRepository)
        {
            _sectionRepository = sectionRepository;
            _textRepository = textRepository;
            _pdfRepository = pdfRepository;
            _textTranslationRepository = textTranslationRepository;
            _pdfTranslationRepository = pdfTranslationRepository;
            _langSectionIgnoreRepository = langSectionIgnoreRepository;
        }

        /// <summary>
        /// Gets the application for a given section.
        /// </summary>
        /// <param name="sectionId">The id of the section for which the application is required.</param>
        /// <returns>ApplicationInfoApiModel</returns>
        public async Task<ApplicationInfoApiModel> GetApplicationAsync(int sectionId)
        {
            if (sectionId <= 0)
            {
                throw new ArgumentIsNullException($"{nameof(sectionId)} is {sectionId} and hence invalid.");
            }

            try
            {
                Section section = await _sectionRepository
                                        .Where(sect => sect.Id == sectionId)
                                        .Include(sect => sect.Application)
                                        .FirstOrDefaultAsync();

                if (section == null)
                {
                    throw new EntityDoesNotExistException($"No {nameof(Section)} exists with id ${sectionId}");
                }

                return new ApplicationInfoApiModel()
                {
                    ApplicationId = section.Application.Id,
                    IsVisible = section.Application.IsVisible,
                    TranslationTypeId = section.Application.TranslationTypeId,
                    LockedCode = section.Application.IsLocked ? (long)StatusCodes.ApplicationLocked : (long)StatusCodes.ApplicationUnlocked,
                    Sections = section.Application.Section.Select(s => new SectionApiModel()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        DisplayName = s.DisplayName,
                        IsVisible = s.IsVisible,
                        ApplicationId = s.ApplicationId,
                        Note = s.Note
                    })
                };
            }
            catch(EntityDoesNotExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException($"Something went wrong on getting applicationId from section. {nameof(sectionId)}={sectionId}.", ex);
            }
        }

        public async Task<SectionApiModel> GetSectionAsync(int sectionId, params Expression<Func<Section, object>>[] navigationPropertyPaths)
        {
            if (sectionId <= 0)
            {
                throw new ArgumentIsNullException($"{nameof(sectionId)} is {sectionId} and hence invalid.");
            }

            try
            {
                var sections = _sectionRepository.Where(s => s.Id == sectionId);

                if (navigationPropertyPaths != null)
                {
                    foreach (var path in navigationPropertyPaths)
                    {
                        sections = sections.Include(path);
                    }
                }

                Section section = await sections.FirstOrDefaultAsync();
                return new SectionApiModel()
                {
                    Id = section.Id,
                    Name = section.Name,
                    DisplayName = section.DisplayName,
                    LanguageSectionIgnores = section.LanguageSectionIgnore,
                    Note = section.Note,
                    ApplicationId = section.ApplicationId,
                };
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException($"Something went wrong on getting applicationId from section. {nameof(sectionId)}={sectionId}.", ex);
            }
        }

        /// <summary>
        /// Gets the texts for the section.
        /// </summary>
        /// <param name="sectionId">The id of the section for which texts are required.</param>
        /// <returns>Collection of texts</returns>
        public Task<IEnumerable<Text>> GetTextsAsync(int sectionId, params Expression<Func<Text, object>>[] navigationPropertyPaths)
        {
            var texts = _textRepository.Where(text => text.SectionId == sectionId);

            if (navigationPropertyPaths != null)
            {
                foreach (var path in navigationPropertyPaths)
                {
                    texts = texts.Include(path);
                }
            }

            return Task.FromResult<IEnumerable<Text>>(texts);
        }

        /// <summary>
        /// Adds note to a given section
        /// </summary>
        /// <param name="sectionId">The id of the section for which note has to be added.</param>
        /// <param name="note">The note</param>
        /// <returns>Task.</returns>
        public async Task AddNoteToSection(int sectionId, string note) {

            var section = await _sectionRepository.GetByIdAsync(sectionId);
            section.Note = note;
            await _sectionRepository.EditAsync(section);
        }

        /// <summary>
        /// Adds sections to the application.
        /// </summary>
        /// <param name="sectionsToAdd">The api section model.</param>
        /// <returns>Task.</returns>
        public Task AddSectionsAsync(IEnumerable<SectionApiModel> sectionsToAdd)
        {
            if (sectionsToAdd.Count() == 0)
            {
                return Task.FromResult<object>(null);
            }

            IEnumerable<Section> sections = sectionsToAdd.Select(section => section.ToEntity());
            return _sectionRepository.AddMultipleAsync(sections);
        }

        /// <summary>
        /// Adds sections to the application.
        /// </summary>
        /// <param name="sectionsToAdd">The section entity.</param>
        /// <returns>Task.</returns>
        public Task AddSectionsAsync(IEnumerable<Section> sectionsToAdd)
        {
            if (sectionsToAdd.Count() == 0)
            {
                return Task.FromResult<object>(null);
            }

            return _sectionRepository.AddMultipleAsync(sectionsToAdd);
        }

        /// <summary>
        /// Edits sections of the application.
        /// </summary>
        /// <param name="sectionsToEdit">The api models of section to be edited.</param>
        /// <returns>Task.</returns>
        public async Task EditSectionsAsync(IEnumerable<SectionApiModel> sectionsToEdit)
        {
            if (sectionsToEdit.Count() == 0)
            {
                return;
            }

            var sections = _sectionRepository.Where(section => sectionsToEdit.Any(s => s.Id == section.Id));

            await sections
                .ForEachAsync(section => 
                {
                    var sectionToEdit = sectionsToEdit.FirstOrDefault(item => item.Id == section.Id);
                    section.Name = sectionToEdit.Name;
                    section.DisplayName = sectionToEdit.DisplayName;
                    section.Note = sectionToEdit.Note;
                    section.IsVisible = sectionToEdit.IsVisible;
                });

            await _sectionRepository.EditMultipleAsync(sections);
        }

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection id ids of sections to delete.</param>
        /// <returns>Task.</returns>
        public async Task DeleteSectionsAsync(int translationTypeId, IEnumerable<int> sectionsToDelete)
        {
            if (!sectionsToDelete.Any())
            {
                return;
            }

            if (translationTypeId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(translationTypeId)} is {translationTypeId} and hence, invalid");
            }

            List<Section> sections = _sectionRepository.Where(section => sectionsToDelete.Contains(section.Id))
                                        .Include(section => section.LanguageSectionIgnore)
                                        .Include(section => section.Text)
                                            .ThenInclude(text => text.TextTranslation)
                                        .Include(section => section.Pdf)
                                            .ThenInclude(pdf => pdf.PdfTranslation).ToList();

            if (translationTypeId == 1)
            {
                var texts = sections.SelectMany(section => section.Text);
                var textTranslations = texts.SelectMany(text => text.TextTranslation);
                await _textTranslationRepository.DeleteMultipleAsync(textTranslations);
                await _textRepository.DeleteMultipleAsync(texts);
            }
            else
            {
                var pdfs = sections.SelectMany(section => section.Pdf);
                var pdfTranslations = pdfs.SelectMany(pdf => pdf.PdfTranslation);
                await _pdfTranslationRepository.DeleteMultipleAsync(pdfTranslations);
                await _pdfRepository.DeleteMultipleAsync(pdfs);
            }

            var sectionIgnores = sections.SelectMany(section => section.LanguageSectionIgnore);
            await _langSectionIgnoreRepository.DeleteMultipleAsync(sectionIgnores);
            await _sectionRepository.DeleteMultipleAsync(sections);
        }

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection of section api models to delete.</param>
        /// <returns>Task.</returns>
        public async Task DeleteSectionsAsync(int translationTypeId, IEnumerable<SectionApiModel> sectionsToDelete)
        {
            if (sectionsToDelete.Count() == 0)
            {
                return;
            }

            if (translationTypeId != 1 || translationTypeId != 2)
            {
                throw new ArgumentIsInvalidException($"{nameof(translationTypeId)} is {translationTypeId} and hence, invalid");
            }

            if (translationTypeId == 1)
            {
                var texts = _textRepository.Where(text => sectionsToDelete.Any(section => section.Id == text.SectionId));
                var translations = _textTranslationRepository.Where(translation => texts.Any(text => text.Id == translation.TextId));
                await _textTranslationRepository.DeleteMultipleAsync(translations);
                await _textRepository.DeleteMultipleAsync(texts);
            }
            else
            {
                var pdfs = _pdfRepository.Where(text => sectionsToDelete.Any(section => section.Id == text.SectionId));
                var translations = _pdfTranslationRepository.Where(translation => pdfs.Any(pdf => pdf.Id == translation.PdfId));
                await _pdfTranslationRepository.DeleteMultipleAsync(translations);
                await _pdfRepository.DeleteMultipleAsync(pdfs);
            }

            IQueryable<Section> sections = _sectionRepository.Where(section => sectionsToDelete.Any(s => s.Id == section.Id));
            await _sectionRepository.DeleteMultipleAsync(sections);
        }

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection of section entities to delete.</param>
        /// <returns>Task.</returns>
        public async Task DeleteSectionsAsync(int translationTypeId, IEnumerable<Section> sectionsToDelete)
        {
            if (sectionsToDelete.Count() == 0)
            {
                return;
            }

            if (translationTypeId != 1 || translationTypeId != 2)
            {
                throw new ArgumentIsInvalidException($"{nameof(translationTypeId)} is {translationTypeId} and hence, invalid");
            }

            if (translationTypeId == 1)
            {
                var texts = _textRepository.Where(text => sectionsToDelete.Any(section => section.Id == text.SectionId));
                var translations = _textTranslationRepository.Where(translation => texts.Any(text => text.Id == translation.TextId));
                await _textTranslationRepository.DeleteMultipleAsync(translations);
                await _textRepository.DeleteMultipleAsync(texts);
            }
            else
            {
                var pdfs = _pdfRepository.Where(text => sectionsToDelete.Any(section => section.Id == text.SectionId));
                var translations = _pdfTranslationRepository.Where(translation => pdfs.Any(pdf => pdf.Id == translation.PdfId));
                await _pdfTranslationRepository.DeleteMultipleAsync(translations);
                await _pdfRepository.DeleteMultipleAsync(pdfs);
            }

            await _sectionRepository.DeleteMultipleAsync(sectionsToDelete);
        }

        /// <summary>
        /// To Get all the groups, related texts and translation for an application Id.
        /// </summary>
        /// <param name="applicationId">Application Id to fetch all the sections and translations.</param>
        /// <returns>Task<IEnumerable<SectionApiModel>></returns>
        public async Task<IEnumerable<SectionApiModel>> GetSectionsAndTextTranslations(int applicationId)
        {
            try
            {
                var sections = await _sectionRepository.GetAllIncludingAsync(section => section.ApplicationId == applicationId, section => section.Text);

                var sectionAndTranslations = sections
                                                     .Select(section => new SectionApiModel()
                                                     {
                                                         Id = section.Id,
                                                         Name = section.Name,
                                                         ApplicationId = section.ApplicationId,
                                                         Texts = section.Text.Select(text => new TextApiModel()
                                                         {
                                                             Id = text.Id,
                                                             Key = text.Key,
                                                             ReleaseId = text.VersionId,
                                                             SectionId = text.SectionId,
                                                             GroupId = text.GroupId,
                                                             TextTranslations = text.TextTranslation.Where(tt => tt.LanguageId == 1).Select(tt => new TextTranslationApiModel()
                                                             {
                                                                 Id = tt.Id,
                                                                 Translation = tt.Translation
                                                             }).ToList()
                                                         }).ToList(),
                                                         Pdfs = new List<PdfApiModel>()
                                                     })
                                                     .ToList();
                return sectionAndTranslations;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<SectionApiModel>> GetSectionAndPdfTranslations(int applicationId)
        {
            try
            {
                var sections = await _sectionRepository.GetAllIncludingAsync(section => section.ApplicationId == applicationId, section => section.Pdf);

                var sectionAndTranslations = sections
                                                     .Select(section => new SectionApiModel()
                                                     {
                                                         Id = section.Id,
                                                         Name = section.Name,
                                                         ApplicationId = section.ApplicationId,
                                                         Pdfs = section.Pdf.Select(pdf => new PdfApiModel()
                                                         {
                                                             Id = pdf.Id,
                                                             Key = pdf.Key,
                                                             SectionId = pdf.SectionId,
                                                             GroupId = pdf.GroupId,
                                                             PdfTranslations = pdf.PdfTranslation.Where(pt => pt.LanguageId == 1).Select(pt => new PdfTranslationApiModelForGroup()
                                                             {
                                                                 Id = pt.Id,
                                                                 Translation = pt.FileName
                                                             }).ToList()
                                                         }).ToList(),
                                                         Texts = new List<TextApiModel>()
                                                     })
                                                     .ToList();
                return sectionAndTranslations;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
