﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Implementation.Helper;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class LocalizationStorageManager : ILocalizationStorageManager
    {
        private readonly StorageSettingOptions _settings;
        private readonly IGeneralRepository<Image> _imageRepository;
        private readonly IGeneralRepository<GroupImage> _groupImageRepository;

        public LocalizationStorageManager(
            IOptions<StorageSettingOptions> options,
            IGeneralRepository<Image> imageRepository,
            IGeneralRepository<GroupImage> groupImageRepository)
        {
            _settings = options.Value;
            _imageRepository = imageRepository;
            _groupImageRepository = groupImageRepository;
        }

        public async Task DeleteFileFromStorageAsync(int imageId, string path)
        {
            try
            {
                await DeleteFile(path);
            }
            catch (Exception ex)
            {
                throw new AzureStorageException("Something went wrong when deleting file from storage", ex);
            }

            try
            {
                GroupImage groupImage = null;

                if (imageId <= 0)
                {
                    groupImage = _groupImageRepository.FirstOrDefault(img => string.Compare(img.Path, path, StringComparison.OrdinalIgnoreCase) == 0);
                }
                else
                {
                    groupImage = await _groupImageRepository.GetByIdAsync(imageId);
                }

                await _groupImageRepository.DeleteAsync(groupImage);
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on deleting image from repository", ex);
            }
        }

        public async Task<object> GetFileFromStorageAsync(int pageId)
        {
            string path = null;

            if (pageId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(pageId)} is invalid");
            }

            try
            {
                var imageData = _groupImageRepository.Where(image => image.GroupId == pageId).ToList();

                if (imageData == null)
                {
                    throw new ArgumentIsNullException($"{nameof(imageData)} is null");
                }

                imageData.ForEach(item =>
                {
                    item.Path = ReadFileFromStorage(item.Path);
                });

                return imageData;
            }
            catch (Exception ex)
            {
                throw new AzureStorageException("Something went wrong in getting file from storage.", ex);
            }
        }

        public async Task<List<GroupImage>> GetMultipleImagesFromStorageAsync(List<int> groupIds)
        {
            try
            {
                List<GroupImage> imageData = _groupImageRepository.Where(image => groupIds.Contains(image.GroupId)).ToList();

                if (imageData == null)
                {
                    throw new ArgumentIsNullException($"{nameof(imageData)} is null");
                }

                imageData.ForEach(item =>
                {
                    item.Path = ReadFileFromStorage(item.Path);
                });

                return imageData;
            }
            catch (Exception ex)
            {
                throw new AzureStorageException("Something went wrong in getting file from storage.", ex);
            }
        }

        public string ReadFileFromStorage(string path)
        {
            try
            {
                StorageCredentials credentials = new StorageCredentials(_settings.AccountName, _settings.AccountKey);
                CloudStorageAccount account = new CloudStorageAccount(credentials, _settings.IsSSLEnabled);
                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.GetContainerReference(_settings.ContainerName);
                BlobStorage helper = new BlobStorage();
                SharedAccessBlobPolicy policy = helper.GeneratePolicy();
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
                return blockBlob.StorageUri.PrimaryUri.AbsoluteUri + blockBlob.GetSharedAccessSignature(policy);
            }
            catch (Exception ex)
            {
                throw new AzureStorageException("Something went wrong in getting file from storage.", ex);
            }
        }

        public async Task<bool> DeleteFile(string path)
        {
            CloudBlockBlob blockBlob = await GetBlockBlobReference(path);
            return await blockBlob.DeleteIfExistsAsync();
        }

        public async Task<string> UploadFileToStorageAsync(int pageId, string path, Stream fileStream)
        {
            try
            {
                CloudBlockBlob blockBlob = await GetBlockBlobReference(path);
                // int imageId = 0;

                using (fileStream)
                {
                    fileStream.Seek(0, SeekOrigin.Begin);
                    await blockBlob.UploadFromStreamAsync(fileStream);
                }

                //await _imageRepository.AddAsync(image);
                //imageId = image.Id;

                BlobStorage helper = new BlobStorage();
                string accessUrl = blockBlob.StorageUri.PrimaryUri.AbsoluteUri + blockBlob.GetSharedAccessSignature(helper.GeneratePolicy());
                return accessUrl;
            }
            catch (Exception ex)
            {
                throw new AzureStorageException("Something went wrong on uploading file to storage", ex);
            }
        }

        public async Task CreateOrReplaceBlob(string blob, Stream stream, string container = null)
        {
            StorageCredentials credentials = new StorageCredentials(_settings.AccountName, _settings.AccountKey);
            CloudStorageAccount account = new CloudStorageAccount(credentials, _settings.IsSSLEnabled);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(container !=null ? container : _settings.ContainerName);

            await cloudBlobContainer.CreateIfNotExistsAsync();

            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blob);

            stream.Seek(0, SeekOrigin.Begin);

            await blockBlob.UploadFromStreamAsync(stream);
        }

            private async Task<CloudBlockBlob> GetBlockBlobReference(string fileName)
        {
            StorageCredentials credentials = new StorageCredentials(_settings.AccountName, _settings.AccountKey);
            CloudStorageAccount account = new CloudStorageAccount(credentials, _settings.IsSSLEnabled);
            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer container = client.GetContainerReference(_settings.ContainerName);
            await container.CreateIfNotExistsAsync();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            return blockBlob;
        }
    }
}
