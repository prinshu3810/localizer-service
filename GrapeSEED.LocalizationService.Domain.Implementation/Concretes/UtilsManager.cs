using System;
using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class UtilsManager : IUtilsManager
    {
        public UtilsManager()
        {

        }

        public Dictionary<string, int> GetAllStatusCodesAsync()
        {
            return Enum
                    .GetNames(typeof(StatusCodes))
                    .ToDictionary(name => name, value => (int)Enum.Parse<StatusCodes>(value));
        }
    }
}