﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using Microsoft.Extensions.Options;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class NotificationManager : INotificationManager
    {
        private readonly NotificationSettingOptions _options;

        public NotificationManager(IOptions<NotificationSettingOptions> options)
        {
            _options = options.Value;
        }

        /// <summary>
        /// Synchronous notification.
        /// </summary>
        /// <param name="model">The input model.</param>
        /// <returns>Result of the operation containing message, exceptions and status code.</returns>
        public NotificationResultModel Notify(NotificationApiModel model)
        {
            try
            {
                int port = model.Port != null ? Convert.ToInt32(model.Port) : _options.Port;
                string host = model.Host ?? _options.Host;

                using (SmtpClient client = new SmtpClient(host, port))
                using (MailMessage message = CreateMailMessage(model))
                {
                    client.EnableSsl = model.EnableSsl == null ? _options.EnableSsl : Convert.ToBoolean(model.EnableSsl);
                    client.Credentials = new NetworkCredential(model.FromAddress ?? _options.SenderEmail, model.Password ?? _options.Password);
                    client.UseDefaultCredentials = false;
                    client.Send(message);
                    return new NotificationResultModel()
                    {
                        NotificationStatusCode = StatusCodes.SuccessfullyNotified,
                        Messages = new[] { "Notified Successfully" }
                    };
                }
            }
            catch (Exception ex)
            {
                return new NotificationResultModel()
                {
                    NotificationStatusCode = StatusCodes.NotificationUnsuccessfull,
                    Exceptions = new[] { ex },
                    Messages = new[] { ex.Message, "Notification Unsuccessfull" }
                };
            }
        }

        /// <summary>
        /// <para>
        /// Does not block the calling thread and allows the caller to pass an object to
        /// the method that is invoked when the operation completes.
        /// </para>
        /// </summary>
        /// <param name="model">The input model</param>
        /// <param name="userToken">User defined object passed to handler when the operation completes</param>
        /// <returns>Result of the operation containing message, exceptions and status code.</returns>
        public async Task<NotificationResultModel> NotifyAsync(NotificationApiModel model, object userToken)
        {
            int port = model.Port != null ? Convert.ToInt32(model.Port) : _options.Port;
            string host = model.Host ?? _options.Host;

            SmtpClient client = new SmtpClient(host, port);
            MailMessage message = CreateMailMessage(model);

            string senderEmail = model.FromAddress ?? _options.SenderEmail;
            string password = model.Password ?? _options.Password;
            client.EnableSsl = model.EnableSsl == null ? _options.EnableSsl : Convert.ToBoolean(model.EnableSsl);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(senderEmail, password);

            if (model.OperationEventHandler != null)
            {
                client.SendCompleted += model.OperationEventHandler;
            }

            if (userToken == null)
            {
                userToken = model;
            }

            if (model.AwaitOperation)
            {
                await client.SendMailAsync(message);
            }
            else
            {
                client.SendAsync(message, userToken);
            }

            return new NotificationResultModel()
            {
                NotificationStatusCode = StatusCodes.NotificationDeferred,
                Messages = new[] { "See the result in the operation event handler", "Notification Deferred" }
            };

        }

        private MailMessage CreateMailMessage(NotificationApiModel model)
        {
            MailMessage message = new MailMessage();
            string senderEmail = model.FromAddress ?? _options.SenderEmail;

            message.From = new MailAddress(senderEmail);

            if (model.ToAddresses?.Count() > 0)
            {
                message.To.Add(string.Join(',', model.ToAddresses));
            }

            if (model.CcAddresses?.Count() > 0)
            {
                message.CC.Add(string.Join(',', model.CcAddresses));
            }

            if (model.BccAddresses?.Count() > 0)
            {
                message.Bcc.Add(string.Join(',', model.BccAddresses));
            }

            if (model.IsHtml != null)
            {
                message.IsBodyHtml = Convert.ToBoolean(model.IsHtml);
            }

            message.Body = Convert.ToString(model.Body);
            message.Subject = model.Subject;
            
            return message;
        }
    }
}
