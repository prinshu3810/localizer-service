﻿using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class LanguageManager : ILanguageManager
    {
        private readonly IGeneralRepository<Language> _languageRepository;

        public LanguageManager(IGeneralRepository<Language> languageRepository)
        {
            _languageRepository = languageRepository;
        }

        /// <summary>
        /// To get all the languages.
        /// </summary>
        /// <returns>async Task<IEnumerable<LanguageModel>></returns>
        public async Task<IEnumerable<LanguageModel>> GetLanguages()
        {
            IQueryable<Language> languages;

            try
            {
                languages = await _languageRepository.GetAllAsync();
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }

            if (languages != null)
            {
                return languages.Select(language => new LanguageModel()
                {
                    Id = language.Id,
                    Name = language.Name,
                    IsVisible = language.IsVisible,
                    LanguageCode = language.LanguageCode
                }).OrderBy(language => language.Name).ToList();
            }

            return null;
        }
    }
}
