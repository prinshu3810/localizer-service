﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Implementation;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class ApplicationManager : IApplicationManager
    {
        private readonly ISectionManager _sectionManager;
        private readonly IStoredProcedureRespository _storedProcedureRespository;
        private readonly IApplicationRepository _applicationRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationManager"/> class.
        /// </summary>
        /// <param name="sectionManager">The section manager.</param>
        /// <param name="storedProcedureRespository">The Stored Procedure Repository.</param>
        /// <param name="applicationRepository">The application repository.</param>
        public ApplicationManager(
            ISectionManager sectionManager,
            IStoredProcedureRespository storedProcedureRespository,
            IApplicationRepository applicationRepository)
        {
            _applicationRepository = applicationRepository;
            _sectionManager = sectionManager;
            _storedProcedureRespository = storedProcedureRespository;
        }

        /// <summary>
        /// Edits the application as per the provided information in the model.
        /// </summary>
        /// <param name="model">the input containing the information of application to edit.</param>
        /// <returns>Task.</returns>
        public async Task EditApplicationAsync(ApplicationInfoApiModel model)
        {
            try
            {
                Application application = null;

                if (model.SectionsToDelete.Any())
                {
                    await _sectionManager.DeleteSectionsAsync(model.TranslationTypeId, model.SectionsToDelete);
                }

                if (model.SectionsToAdd.Any())
                {
                    await _sectionManager.AddSectionsAsync(model.SectionsToAdd);
                }

                if (model.SectionsToEdit.Any())
                {
                    await _sectionManager.EditSectionsAsync(model.SectionsToEdit);
                }

                if (model.SectionsToUpload.Any())
                {
                    if (model.RemoveEnglishForOtherLang)
                    {
                        application = _applicationRepository
                                        .Where(app => app.Id == model.ApplicationId)
                                        .Include(app => app.Section)
                                            .ThenInclude(section => section.Text)
                                                .ThenInclude(text => text.TextTranslation)
                                        .FirstOrDefault();
                    }
                    else
                    {
                        application = _applicationRepository
                                        .Where(app => app.Id == model.ApplicationId)
                                        .Include(app => app.Section)
                                            .ThenInclude(section => section.Text)
                                        .FirstOrDefault();
                    }

                    bool isEditRequred = false;

                    foreach (var section in model.SectionsToUpload)
                    {
                        Section sectionEntity = application
                                                    .Section
                                                    .FirstOrDefault(s => !string.IsNullOrEmpty(s.Name)
                                                        && string.Compare(
                                                            s.Name.Trim(), 
                                                            section.Name.Trim(), 
                                                            StringComparison.OrdinalIgnoreCase) == 0);

                        section.ApplicationId = application.Id;

                        if (sectionEntity == null)
                        {
                            await _sectionManager.AddSectionsAsync(new[] { section });
                            continue;
                        }

                        section.Id = sectionEntity.Id;
                        var texts = sectionEntity.Text.Where(text => text.SectionId == section.Id);
                        

                        foreach (var text in section.Texts)
                        {
                            var textEntity = texts.FirstOrDefault(item => item.Key == text.Key);

                            if (textEntity == null)
                            {
                                continue;
                            }

                            TextTranslation englishTranslation = null;

                            if (model.RemoveEnglishForOtherLang)
                            {
                                englishTranslation = textEntity.TextTranslation.FirstOrDefault(t => t.LanguageId == 1);

                                if (englishTranslation != null)
                                {
                                    text.TextTranslations = text.TextTranslations.Where(t => string.Compare(t.Translation.Trim(), englishTranslation.Translation.Trim()) != 0);
                                }
                            }

                            if (text.TextTranslations.Any())
                            {
                                text.Id = textEntity.Id;
                                text.SectionId = textEntity.SectionId;

                                /* The textId update in the next loop will not take place if not converted to list.
                                 * See best upvoted answer: 
                                 * https://stackoverflow.com/questions/9104181/updating-an-item-property-within-ienumerable-but-the-property-doesnt-stay-set
                                 */
                                text.TextTranslations = text.TextTranslations.ToList();

                                foreach (var translation in text.TextTranslations)
                                {
                                    translation.TextId = text.Id;
                                    textEntity.TextTranslation.Add(translation.ToEntity());
                                }
                                isEditRequred = true;
                            }
                        }
                    }

                    if (isEditRequred)
                    {
                        await _applicationRepository.EditAsync(application);
                    }
                }

                if (application == null)
                {
                    application = _applicationRepository.FirstOrDefault(app => app.Id == model.ApplicationId);
                }

                if (!string.IsNullOrEmpty(model.Name) && string.Compare(model.Name.Trim(), application.Name.Trim(), StringComparison.OrdinalIgnoreCase) != 0)
                {
                    application.Name = model.Name;
                    await _applicationRepository.EditAsync(application);
                }
            }
            catch (Exception e)
            {
                throw new DatabaseOpException("Something went wrong while editing the application", e);
            }
        }

        public async Task<object> GetNotifications(int userId, int roleId, int applicationId = 0, int languageId = 0, bool isGroupView = false, int? sectionId = null,  int? groupId = null)
        {
            if (isGroupView)
            {
                var response = await _storedProcedureRespository.GetStoredProcedure("Sp_NotificationCounter",
                                                             ("applicationId", applicationId < 1 ? null : (int?)applicationId),
                                                             ("languageId", languageId),
                                                             ("groupView", isGroupView),
                                                             ("groupId", groupId),
                                                             ("sectionId", sectionId),
                                                             ("role", roleId),
                                                             ("userId", userId))
                                                 .ExecuteStoredProcedureAsync<NotificationResponseGroup>();
                return new { Notifications = response };
            }
            else
            {
                var response = await _storedProcedureRespository.GetStoredProcedure("Sp_NotificationCounter",
                                                             ("applicationId", applicationId < 1 ? null : (int?)applicationId),
                                                             ("languageId", languageId),
                                                             ("groupView", isGroupView),
                                                             ("groupId", groupId),
                                                             ("sectionId", sectionId),
                                                             ("role", roleId),
                                                             ("userId", userId))
                                                 .ExecuteStoredProcedureAsync<NotificationResponse>();
                return new { Notifications = response };
            }
            
        }

        public async Task ChangeAllowPublish(int applicationId, bool allowPublish)
        {
            var application = await _applicationRepository.GetByIdAsync(applicationId);

            if(application == null)
            {
                throw new ArgumentIsNullException("Application does not exist");
            }

            application.AllowPublish = allowPublish;

            await _applicationRepository.EditAsync(application);
        }

        public async Task<dynamic> GetUngroupedKeysCount()
        {
            return await _applicationRepository.GetUngroupedKeysCount();
        }
    }
}
