﻿using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class RoleManager : IRoleManager
    {
        private readonly IGeneralRepository<Role> _roleRepository;

        public RoleManager(IGeneralRepository<Role> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// To get all the roles.
        /// </summary>
        /// <returns>async Task<IEnumerable<RoleModel>></returns>
        public async Task<IEnumerable<RoleModel>> GetRoles()
        {
            IQueryable<Role> roles;

            try
            {
                roles = await _roleRepository.GetAllAsync();
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }

            if (roles != null)
            {
                return roles.Select(language => new RoleModel()
                {
                    Id = language.Id,
                    Name = language.Name
                }).OrderBy(role => role.Name).ToList();
            }

            return null;
        }
    }
}
