﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class TranslationManager : ITranslationManager
    {
        private readonly StorageSettingOptions _settings;
        private readonly IGeneralRepository<Image> _imageRepository;
        private readonly IGeneralRepository<Pdf> _pdfRepository;
        private readonly IGeneralRepository<Text> _textRepository;
        private readonly IGeneralRepository<Application> _applicationRepository;
        private readonly IGeneralRepository<Section> _sectionRepository;
        private readonly IRelationsRepository<LanguageApplicationIgnore> _langAppIgnoreRepository;
        private readonly IRelationsRepository<LanguageSectionIgnore> _langSectionIgnoreRepository;
        private readonly IGeneralRepository<PdfTranslation> _pdfTranslationRepository;
        private readonly IGeneralRepository<TextTranslation> _textTranslationRepository;
        private readonly ILocalizationStorageManager _storageManager;
        private readonly IGeneralRepository<GroupImage> _groupImageRepository;
        private readonly IGeneralRepository<Group> _groupRepository;
        private readonly IGeneralRepository<LanguageGroupIgnore> _langGroupIgnoreRepository;

        public TranslationManager(
            IGeneralRepository<Pdf> pdfRepository,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<Image> imageRepository,
            IGeneralRepository<PdfTranslation> pdfTranslationRepository,
            IGeneralRepository<TextTranslation> textTranslationRepository,
            IGeneralRepository<Application> applicationRepository,
            IGeneralRepository<Section> sectionRepository,
            IRelationsRepository<LanguageApplicationIgnore> langAppIgnoreRepository,
            IRelationsRepository<LanguageSectionIgnore> langSectionIgnoreRepository,
            ILocalizationStorageManager storageManager,
            IGeneralRepository<GroupImage> groupImageRepository,
            IGeneralRepository<Group> groupRepository,
            IGeneralRepository<LanguageGroupIgnore> langGroupRepository
         )
        {
            _imageRepository = imageRepository;
            _pdfRepository = pdfRepository;
            _textRepository = textRepository;
            _pdfTranslationRepository = pdfTranslationRepository;
            _textTranslationRepository = textTranslationRepository;
            _applicationRepository = applicationRepository;
            _sectionRepository = sectionRepository;
            _langAppIgnoreRepository = langAppIgnoreRepository;
            _langSectionIgnoreRepository = langSectionIgnoreRepository;
            _storageManager = storageManager;
            _groupImageRepository = groupImageRepository;
            _groupRepository = groupRepository;
            _langGroupIgnoreRepository = langGroupRepository;
        }

        public async Task<int> SaveUploadedImage(int pageId, string fileName)
        {
            GroupImage groupImage = new GroupImage()
            {
                Path = fileName,
                GroupId = pageId
            };

            try
            {
                await _groupImageRepository.AddAsync(groupImage);
                return groupImage.Id;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on saving an uploaded image", ex);
            }

        }

        public async Task<Pdf> SaveEnglishPdf(PdfTranslationApiModel data, int sectionId, string fileName, List<LanguageSectionIgnore> sectionsIgnored)
        {
            try
            {
                Pdf pdf = new Pdf()
                {
                    Key = data.Key,
                    SectionId = sectionId,
                    VersionId = data.ReleaseId,
                    PdfTranslation = new List<PdfTranslation>
                    {
                        new PdfTranslation
                        {
                            LanguageId = data.LanguageId,
                            StatusId = data.StatusId,
                            Path = data.Path,
                            FileName = fileName,
                            IsVisible = data.IsVisible
                        }
                    }
                };

                await _pdfRepository.AddAsync(pdf);
                if (data.LanguageId == 1)
                {
                    // Add new translations with isVisble = false for ignored sections for other languages.
                    List<PdfTranslation> ignoredPdfTranslations = new List<PdfTranslation>();
                    var languageIdList = sectionsIgnored.Where(x => x.SectionId == sectionId && x.LanguageId != 1).Select(x => x.LanguageId).ToList();
                    languageIdList.ForEach(languageId =>
                    {
                        ignoredPdfTranslations.Add(new PdfTranslation
                        {
                            Id = 0,
                            PdfId = pdf.Id,
                            LanguageId = languageId,
                            StatusId = 1,
                            Path = String.Empty,
                            FileName = String.Empty,
                            IsVisible = false
                        });
                    });

                    if (ignoredPdfTranslations.Any())
                    {
                        await _pdfTranslationRepository.AddMultipleAsync(ignoredPdfTranslations);
                    }
                }
                return pdf;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on saving an uploaded pdf.", ex);
            }
        }


        public async Task<PdfTranslation> SaveTranslatedPdf(PdfTranslationApiModel data, int sectionId, string fileName)
        {
            PdfTranslation pdfTranslation = new PdfTranslation
            {
                Id = data.PdfTranslationId == null || data.IsAddMultiple ? 0 : (int)data.PdfTranslationId,
                PdfId = data.PdfId,
                LanguageId = data.LanguageId,
                StatusId = data.StatusId,
                Path = data.Path,
                FileName = fileName,
                IsVisible = data.IsVisible
            };

            if (data.PdfTranslationId == null || data.IsAddMultiple)
            {
                await _pdfTranslationRepository.AddAsync(pdfTranslation);
            }
            else
            {
                await _pdfTranslationRepository.EditAsync(pdfTranslation, nameof(pdfTranslation.FileName), nameof(pdfTranslation.Path),
                    nameof(pdfTranslation.StatusId), nameof(pdfTranslation.UpdatedDate));
            }
            if (data.LanguageId == 1)
            {
                // on adding new english translation, update status of other translations that have status other than updated for the same pdf id. 
                var entityList = _pdfTranslationRepository.Where(x => x.PdfId == pdfTranslation.PdfId && x.StatusId != 2).ToList();
                entityList.ForEach(entity =>
                {
                    entity.StatusId = (int)Enums.Status.Updated;
                });
                await _pdfTranslationRepository.EditMultipleAsync(entityList);
            }
            return pdfTranslation;
        }

        public async Task<long> ToggleApplicationLockAsync(int applicationId, bool isLocked)
        {
            if (applicationId <= 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(applicationId)} is {applicationId} and hence invalid.");
            }

            try
            {
                Application application = await _applicationRepository.GetByIdAsync(applicationId);

                if (application == null)
                {
                    throw new EntityDoesNotExistException($"{nameof(Application)} with id {applicationId} does not exist");
                }

                application.IsLocked = isLocked;
                await _applicationRepository.SaveChangesAsync();
                return application.IsLocked ? (long)StatusCodes.ApplicationLocked : (long)StatusCodes.ApplicationUnlocked;
            }
            catch (EntityDoesNotExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on locking an application.", ex);
            }
        }

        /// <summary>
        /// Saves the provided text translations.
        /// </summary>
        /// <param name="textTranslations">The text translations to save.</param>
        /// <returns>Task.</returns>
        public Task SaveTextTranslationsAsync(IEnumerable<TextTranslationApiModel> textTranslations)
        {
            return _textTranslationRepository.AddMultipleAsync(textTranslations.Select(item => item.ToEntity()));
        }

        public async Task ToggleIgnoreAsync(IgnoreApiModel ignoreApiModel)
        {
            try
            {
                if (!ignoreApiModel.Groups.Any()
                    && !ignoreApiModel.TextTranslations.Any()
                    && !ignoreApiModel.PdfTranslations.Any())
                {
                    await ToggleApplicationVisibility(ignoreApiModel);
                    return;
                }
        
                if (ignoreApiModel.Groups.Any())
                {
                    List<Group> groups = _groupRepository
                                .Where(group => group.ApplicationId == ignoreApiModel.ApplicationId)
                                .Include(group => group.LanguageGroupIgnore)
                                .Include(group => group.Text)
                                    .ThenInclude(text => text.TextTranslation)
                                .Include(group => group.Pdf)
                                    .ThenInclude(pdf => pdf.PdfTranslation)
                                .Include(group => group.Application)
                                    .ThenInclude(app => app.LanguageApplicationIgnore).ToList(); ;
                    await ToggleGroupVisiblity(groups.Where(s => ignoreApiModel.Groups.Any(ignore => ignore.Id == s.Id)).ToList(), 0, ignoreApiModel);
                    int groupIgnoreCount = groups.Count(group => group.LanguageGroupIgnore.Any(ignore => ignore.LanguageId == ignoreApiModel.LanguageId));

                    // If all groups have been ignored, then the app also needs to be ignored.
                    if (groups.Count == groupIgnoreCount)
                    {
                        groups.First().Application.LanguageApplicationIgnore.Add(new LanguageApplicationIgnore()
                        {
                            LanguageId = ignoreApiModel.LanguageId,
                            ApplicationId = ignoreApiModel.ApplicationId
                        });
                    }

                    if (ignoreApiModel.IsVisible)
                    {
                        var applicationIgnores = groups.SelectMany(group => group.Application.LanguageApplicationIgnore);

                        // If a group is un-ignored, then its corresponding application should also be un-ignored.
                        if (applicationIgnores.Any())
                        {
                            await _langAppIgnoreRepository.DeleteMultipleAsync(applicationIgnores);
                        }
                    }

                    await _groupRepository.EditMultipleAsync(groups);
                    return;

                }

                if (ignoreApiModel.TextTranslations.Any())
                {
                    var translations = _textTranslationRepository
                                        .Where(translation => ignoreApiModel.TextTranslations.Any(t => t.TextId == translation.TextId) && translation.LanguageId == ignoreApiModel.LanguageId);

                    await ToggleTextTranslationsVisibility(translations, ignoreApiModel.TextTranslations, ignoreApiModel.LanguageId);
                }

                if (ignoreApiModel.PdfTranslations.Any())
                {
                    var translations = _pdfTranslationRepository
                                        .Where(translation => ignoreApiModel.PdfTranslations.Any(t => t.PdfId == translation.PdfId) && translation.LanguageId == ignoreApiModel.LanguageId);

                    await TogglePdfTranslationsVisibility(translations, ignoreApiModel.PdfTranslations, ignoreApiModel.LanguageId);
                }
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong in ignore operation", ex);
            }
        }

        /// <summary>
        /// Returns the download links for the visible links of a translation
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="languageId"></param>
        /// <param name="includeIgnored"></param>
        /// <param name="selectedLanguage"></param>
        /// <returns>Task<List<string>></returns>
        public List<string> DownloadAllFiles(int sectionId, int languageId, bool includeIgnored, int? selectedLanguage)
        {
            List<string> sasUrlList = new List<string>();
            var pdfTranslationList = GetDownloadablePdfTranslations(sectionId, languageId, includeIgnored, selectedLanguage);
            pdfTranslationList.ForEach(translation =>
            {
                sasUrlList.Add(_storageManager.ReadFileFromStorage(translation.Path));
            });
            return sasUrlList;
        }

        private async Task ToggleApplicationVisibility(IgnoreApiModel ignoreApiModel)
        {
            Application application = await _applicationRepository
                .Where(app => app.Id == ignoreApiModel.ApplicationId)
                .Include(app => app.LanguageApplicationIgnore)
                .Include(app => app.Group)
                    .ThenInclude(group => group.LanguageGroupIgnore)
                .Include(app => app.Section)
                    .ThenInclude(group => group.Text)
                        .ThenInclude(text => text.TextTranslation)
                .Include(app => app.Group)
                    .ThenInclude(group => group.Pdf)
                        .ThenInclude(pdf => pdf.PdfTranslation)
                .FirstOrDefaultAsync();
            await ToggleGroupVisiblity(application.Group, application.TranslationTypeId, ignoreApiModel);

            if (ignoreApiModel.IsVisible)
            {
                await _langAppIgnoreRepository.DeleteMultipleAsync(application.LanguageApplicationIgnore);
            }
            else
            {
                application.LanguageApplicationIgnore.Add(new LanguageApplicationIgnore()
                {
                    LanguageId = ignoreApiModel.LanguageId,
                    ApplicationId = ignoreApiModel.ApplicationId
                });
            }

            await _applicationRepository.EditAsync(application);
        }

        private async Task ToggleGroupVisiblity(ICollection<Group> groups, int translationTypeId, IgnoreApiModel ignoreApiModel)
        {
            List<TextTranslation> textTranslationsToDelete = new List<TextTranslation>();
            List<PdfTranslation> pdfTranslationsToDelete = new List<PdfTranslation>();

            foreach (var group in groups)
            {
                if (!ignoreApiModel.IsVisible
                    && !group
                        .LanguageGroupIgnore
                        .Any(ignore => ignore.GroupId == group.Id && ignore.LanguageId == ignoreApiModel.LanguageId))
                {
                    group.LanguageGroupIgnore.Add(new LanguageGroupIgnore()
                    {
                        LanguageId = ignoreApiModel.LanguageId,
                        GroupId = group.Id
                    });
                }

                if (translationTypeId == 0 && group.Application != null)
                {
                    translationTypeId = group.Application.TranslationTypeId;
                }

                if (translationTypeId == 1)
                {
                    foreach (var text in group.Text)
                    {
                        if (text.TextTranslation.Any())
                        {
                            var langTranslations = text.TextTranslation.Where(translation => translation.LanguageId == ignoreApiModel.LanguageId);

                            if (langTranslations.Any())
                            {
                                foreach (var translation in langTranslations)
                                {
                                    translation.IsVisible = ignoreApiModel.IsVisible;
                                }

                                // All non-english translations that are visible and have 'New' status should be deleted when they are un-ignored.
                                if (ignoreApiModel.LanguageId != 1 && ignoreApiModel.IsVisible)
                                {
                                    var deleteTranslations = langTranslations.Where(translation => translation.StatusId == (int)Enums.Status.New && translation.IsVisible == true && translation.LanguageId != 1);

                                    if (deleteTranslations.Any())
                                    {
                                        textTranslationsToDelete.AddRange(deleteTranslations);
                                        text.TextTranslation = text.TextTranslation.Except(deleteTranslations).ToList();
                                    }
                                }
                            }
                            else
                            {
                                text.TextTranslation.Add(new TextTranslation()
                                {
                                    TextId = text.Id,
                                    LanguageId = ignoreApiModel.LanguageId,
                                    StatusId = (int)Enums.Status.New,
                                    IsVisible = ignoreApiModel.IsVisible,
                                    Translation = string.Empty
                                });
                            }
                        }
                        else
                        {
                            var englishTranslations = _textTranslationRepository.Where(translation => translation.TextId == text.Id && translation.LanguageId == 1);
                            text.TextTranslation = englishTranslations.Select(english => new TextTranslation()
                            {
                                TextId = english.TextId,
                                LanguageId = ignoreApiModel.LanguageId,
                                StatusId = (int)Enums.Status.New,
                                IsVisible = false,
                                Translation = string.Empty
                            }).ToList();
                        }
                    }
                }
                else
                {
                    foreach (var pdf in group.Pdf)
                    {
                        if (pdf.PdfTranslation.Any())
                        {
                            var langTranslations = pdf.PdfTranslation.Where(translation => translation.LanguageId == ignoreApiModel.LanguageId);

                            if (langTranslations.Any())
                            {
                                foreach (var translation in langTranslations)
                                {
                                    translation.IsVisible = ignoreApiModel.IsVisible;
                                }

                                // All non-english translations that are visible and have 'New' status should be deleted when they are un-ignored.
                                if (ignoreApiModel.LanguageId != 1 && ignoreApiModel.IsVisible)
                                {
                                    var deleteTranslations = langTranslations.Where(translation => translation.StatusId == (int)Foundation.Common.Enums.Status.New && translation.IsVisible == true && translation.LanguageId != 1);

                                    if (deleteTranslations.Any())
                                    {
                                        pdfTranslationsToDelete.AddRange(deleteTranslations);
                                        pdf.PdfTranslation = pdf.PdfTranslation.Except(deleteTranslations).ToList();
                                    }
                                }
                            }
                            else
                            {
                                pdf.PdfTranslation.Add(new PdfTranslation()
                                {
                                    PdfId = pdf.Id,
                                    LanguageId = ignoreApiModel.LanguageId,
                                    StatusId = (int)Foundation.Common.Enums.Status.New,
                                    IsVisible = ignoreApiModel.IsVisible,
                                    Path = string.Empty
                                });
                            }
                        }
                        else
                        {
                            var englishTranslations = _pdfTranslationRepository.Where(translation => translation.PdfId == pdf.Id && translation.LanguageId == 1);
                            pdf.PdfTranslation = englishTranslations.Select(english => new PdfTranslation()
                            {
                                PdfId = english.PdfId,
                                LanguageId = ignoreApiModel.LanguageId,
                                StatusId = (int)Foundation.Common.Enums.Status.New,
                                IsVisible = false,
                                Path = string.Empty
                            }).ToList();
                        }
                    }
                }
            }

            if (textTranslationsToDelete.Any())
            {
                await _textTranslationRepository.DeleteMultipleAsync(textTranslationsToDelete);
            }

            if (pdfTranslationsToDelete.Any())
            {
                await _pdfTranslationRepository.DeleteMultipleAsync(pdfTranslationsToDelete);
            }

            if (ignoreApiModel.IsVisible)
            {
                await _langGroupIgnoreRepository.DeleteMultipleAsync(groups.SelectMany(group => group.LanguageGroupIgnore));
            }
        }

        private async Task ToggleTextTranslationsVisibility(IQueryable<TextTranslation> textTranslations, IEnumerable<TextTranslationApiModel> translationsToIgnore, int languageId)
        {
            if (textTranslations.Any())
            {
                foreach (var translation in textTranslations)
                {
                    bool isVisible = translationsToIgnore.FirstOrDefault(t => t.TextId == translation.TextId).IsVisible;
                    translation.IsVisible = isVisible;
                }

                // ToList() is required otherwise it does not filter properly, vague explanation: queryable may not be in memory to filter.
                var translations = textTranslations.ToList();

                // If a translation is un-ignored, then its corresponding group and application should also be un-ignored.
                if (translations.Any(translation => translation.IsVisible == true))
                {
                    translations = textTranslations
                        .Include(t => t.Text.Group.LanguageGroupIgnore)
                        .Include(t => t.Text.Group.Application.LanguageApplicationIgnore)
                        .ToList();

                    var groupIgnores = translations
                        .Select(t => t.Text.Group.LanguageGroupIgnore
                            .Where(ignore => ignore.GroupId == t.Text.Group.Id && ignore.LanguageId == t.LanguageId))
                        .Aggregate((ignore1, ignore2) => ignore1.Concat(ignore2));

                    if (groupIgnores.Any())
                    {
                        await _langGroupIgnoreRepository.DeleteMultipleAsync(groupIgnores);
                    }

                    var applicationIgnores = translations
                        .ToList()
                        .Select(t => t.Text.Group.Application.LanguageApplicationIgnore
                            .Where(ignore => ignore.ApplicationId == t.Text.Section.Application.Id && ignore.LanguageId == t.LanguageId))
                        .Aggregate((ignore1, ignore2) => ignore1.Concat(ignore2));

                    if (applicationIgnores.Any())
                    {
                        await _langAppIgnoreRepository.DeleteMultipleAsync(applicationIgnores);
                    }
                }

                // All non-english translations that are visible and have 'New' status should be deleted when they are un-ignored.
                if (languageId != 1)
                {
                    var deleteTranslations = translations.Where(translation => translation.StatusId == (int)Foundation.Common.Enums.Status.New && translation.IsVisible == true && translation.LanguageId != 1);

                    if (deleteTranslations.Any())
                    {
                        await _textTranslationRepository.DeleteMultipleAsync(deleteTranslations);
                        textTranslations = textTranslations.Except(deleteTranslations);
                    }
                }

                await _textTranslationRepository.EditMultipleAsync(textTranslations);
            }
            else
            {
                IQueryable<TextTranslation> englishTranslations = _textTranslationRepository.Where(translation => translationsToIgnore.Any(t => t.TextId == translation.TextId) && translation.LanguageId == 1);
                var translations = englishTranslations.Select(english => new TextTranslation()
                {
                    TextId = english.TextId,
                    LanguageId = languageId,
                    StatusId = (int)Foundation.Common.Enums.Status.New,
                    IsVisible = false,
                    Translation = string.Empty
                });

                await _textTranslationRepository.AddMultipleAsync(translations);
            }
        }

        private async Task TogglePdfTranslationsVisibility(IQueryable<PdfTranslation> pdfTranslations, IEnumerable<PdfTranslationApiModel> translationsToIgnore, int languageId)
        {
            if (pdfTranslations.Any())
            {
                foreach (var translation in pdfTranslations)
                {
                    bool isVisible = translationsToIgnore.First().IsVisible;
                    translation.IsVisible = isVisible;
                }

                // ToList() is required otherwise it does not filter properly, vague explanation: queryable may not be in memory to filter.
                var translations = pdfTranslations.ToList();

                // If a translation is un-ignored, then its corresponding group and application should also be un-ignored.
                if (translations.Any(translation => translation.IsVisible == true))
                {
                    translations = pdfTranslations
                        .Include(t => t.Pdf.Group.LanguageGroupIgnore)
                        .Include(t => t.Pdf.Group.Application.LanguageApplicationIgnore)
                        .ToList();

                    var groupIgnores = translations
                        .Select(t => t.Pdf.Group.LanguageGroupIgnore
                            .Where(ignore => ignore.GroupId == t.Pdf.Group.Id && ignore.LanguageId == t.LanguageId))
                        .Aggregate((ignore1, ignore2) => ignore1.Concat(ignore2));

                    if (groupIgnores.Any())
                    {
                        await _langGroupIgnoreRepository.DeleteMultipleAsync(groupIgnores);
                    }

                    var applicationIgnores = translations
                        .ToList()
                        .Select(t => t.Pdf.Group.Application.LanguageApplicationIgnore
                            .Where(ignore => ignore.ApplicationId == t.Pdf.Group.Application.Id && ignore.LanguageId == t.LanguageId))
                        .Aggregate((ignore1, ignore2) => ignore1.Concat(ignore2));

                    if (applicationIgnores.Any())
                    {
                        await _langAppIgnoreRepository.DeleteMultipleAsync(applicationIgnores);
                    }
                }

                // All non-english translations that are visible and have 'New' status should be deleted when they are un-ignored.
                if (languageId != 1)
                {
                    var deleteTranslations = translations.Where(translation => translation.StatusId == (int)Foundation.Common.Enums.Status.New && translation.IsVisible == true && translation.LanguageId != 1);

                    if (deleteTranslations.Any())
                    {
                        await _pdfTranslationRepository.DeleteMultipleAsync(deleteTranslations);
                        pdfTranslations = pdfTranslations.Except(deleteTranslations);
                    }
                }

                await _pdfTranslationRepository.EditMultipleAsync(pdfTranslations);
            }
            else
            {
                IQueryable<PdfTranslation> englishTranslations = _pdfTranslationRepository.Where(translation => translationsToIgnore.Any(t => t.PdfId == translation.PdfId) && translation.LanguageId == 1);
                var translations = englishTranslations.Select(english => new PdfTranslation()
                {
                    PdfId = english.PdfId,
                    LanguageId = languageId,
                    StatusId = (int)Foundation.Common.Enums.Status.New,
                    IsVisible = false,
                    Path = string.Empty
                });

                await _pdfTranslationRepository.AddMultipleAsync(translations);
            }
        }

        private List<PdfTranslation> GetDownloadablePdfTranslations(int sectionId, int languageId, bool includeIgnored, int? selectedLanguage)
        {
            var pdfList = _pdfRepository.Where(pdf => pdf.SectionId == sectionId)
                .Include(pdf => pdf.PdfTranslation)
                .ToList();

            if (includeIgnored)
            {
                return pdfList
                .SelectMany(pdf => pdf.PdfTranslation.Where(translation => translation.LanguageId == languageId))
                .ToList();
            }

            if (languageId == selectedLanguage || selectedLanguage == null)
            {
                return pdfList
                .SelectMany(pdf => pdf.PdfTranslation
                    .Where(translation => translation.LanguageId == languageId && translation.IsVisible == true))
                .ToList();
            }
            else
            {
                return pdfList
                .SelectMany(pdf => pdf.PdfTranslation
                    .Where(translation => translation.LanguageId == languageId
                        && CheckTranslationVisiblity((int)selectedLanguage, pdfList, pdf.Id)))
                .ToList();
            }
        }

        // checks if a translation is present for a english language.
        // if present then checks the visibility and return true
        // if doesn't exist then too returns true.
        private bool CheckTranslationVisiblity(int selectedLanguage, List<Pdf> pdfList, int pdfId)
        {
            var currentPdfEntry = pdfList.Where(p => p.Id == pdfId).ToList();

            if (!currentPdfEntry.Any())
            {
                return true;
            }

            var selectedLanguageTranslation = currentPdfEntry.FirstOrDefault().PdfTranslation.Where(pt => pt.LanguageId == selectedLanguage).FirstOrDefault();
            if (selectedLanguageTranslation == null || selectedLanguageTranslation.IsVisible == true)
            {
                return true;
            }

            return false;

        }
    }
}
