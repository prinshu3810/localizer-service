﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Implementation;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class GroupManager : IGroupManager
    {
        private IConfiguration _config;
        private readonly IGeneralRepository<Group> _groupRepository;
        private readonly IGeneralRepository<Text> _textRepository;
        private readonly IGeneralRepository<GroupImage> _groupImageRepository;
        private readonly RepositoryContextBase _repositoryContext;
        private readonly IGeneralRepository<Pdf> _pdfRepository;
        private readonly IGeneralRepository<LanguageGroupIgnore> _languageGroupIgnoreRepository;

        public GroupManager(
            IConfiguration config,
            IGeneralRepository<Group> groupRepository,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<GroupImage> groupImageRepository,
            RepositoryContextBase repositoryContext,
            IGeneralRepository<Pdf> pdfRepository,
            IGeneralRepository<LanguageGroupIgnore> languageGroupIgnoreRepository
        )
        {
            _config = config;
            _groupRepository = groupRepository;
            _textRepository = textRepository;
            _groupImageRepository = groupImageRepository;
            _pdfRepository = pdfRepository;
            _repositoryContext = repositoryContext;
            _languageGroupIgnoreRepository = languageGroupIgnoreRepository;
        }

        /// <summary>
        /// To get all the groups for a particular application.
        /// </summary>
        /// <param name="applicationId">Application Id to fetch all the groups.</param>
        /// <returns>Task<IEnumerable<Group>></returns>
        public async Task<IEnumerable<Group>> GetGroups(int applicationId)
        {
            try
            {
                var allGroups = await _groupRepository.GetAllAsync();

                IEnumerable<Group> groups = new List<Group>();

                if (allGroups != null)
                {
                    groups = await allGroups.Where(g => g.ApplicationId == applicationId).Select(group => new Group()
                    {
                        Id = group.Id,
                        Name = group.Name,
                        Sno = group.Sno
                    }).OrderBy(group => group.Sno).ToListAsync();
                }

                return groups;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To change name of the group.
        /// </summary>
        /// <param name="group">Group for which the name has to be changed.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> ChangeGroupName(Group group)
        {
            try
            {
                var groupFromStore = await _groupRepository.GetByIdAsync(group.Id);

                if (groupFromStore == null)
                {
                    return false;
                }

                groupFromStore.Name = group.Name;

                await _groupRepository.EditAsync(groupFromStore, nameof(groupFromStore.Name));

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// Deletes the group with the given currentGroupId and moves its Texts and Images to a new group with given newGroupId.
        /// </summary>
        /// <param name="currentGroupId">Id of the group to delete.</param>
        /// <param name="newGroupId">Id of the group to move texts and images.</param>
        /// <returns></returns>
        public async Task<bool> DeleteGroup(int currentGroupId, int newGroupId)
        {
            try
            {
                using (var transacton = _repositoryContext.DbContext.Database.BeginTransaction())
                {
                    var groupFromStore = await _groupRepository.GetAllIncludingAsync(group => group.Id == currentGroupId, group => group.Text, group => group.GroupImage, group => group.Pdf);

                    var currentGroup = groupFromStore.FirstOrDefault(x => x.Id == currentGroupId);

                    if (currentGroup == null || currentGroup.Id != currentGroupId)
                    {
                        return false;
                    }

                    // The newGroupId will be not be 0 when user wants to move texts and images to new group.
                    if (newGroupId != 0)
                    {
                        var newGroup = await _groupRepository.GetByIdAsync(newGroupId);

                        if (newGroup == null)
                        {
                            return false;
                        }

                        //if there are associated text with current group then move them to new group
                        if (currentGroup.Text.Any())
                        {
                            IEnumerable<Text> textsForCurrentGroup = currentGroup.Text.ToList();

                            foreach (Text t in textsForCurrentGroup)
                            {
                                t.GroupId = newGroup.Id;
                            }

                            await _textRepository.EditMultipleAsync(textsForCurrentGroup);
                        }

                        //if there are associated images with current group then move them to new group
                        if (currentGroup.GroupImage.Any())
                        {
                            IEnumerable<GroupImage> groupImagesForCurrentGroup = currentGroup.GroupImage.ToList();

                            foreach (GroupImage gi in groupImagesForCurrentGroup)
                            {
                                gi.GroupId = newGroup.Id;
                            }

                            await _groupImageRepository.EditMultipleAsync(groupImagesForCurrentGroup);
                        }

                        if (currentGroup.Pdf.Any())
                        {
                            IEnumerable<Pdf> pdfsForCurrentGroup = currentGroup.Pdf.ToList();

                            foreach (Pdf p in pdfsForCurrentGroup)
                            {
                                p.GroupId = newGroup.Id;
                            }

                            await _pdfRepository.EditMultipleAsync(pdfsForCurrentGroup);
                        }
                    }
                    else if (newGroupId == 0) // If the newGroupId is 0 then simply make the groupId null for all text related to current groupId.
                    {
                        if (currentGroup.Text.Any())
                        {
                            IEnumerable<Text> textsForCurrentGroup = currentGroup.Text.ToList();

                            foreach (Text t in textsForCurrentGroup)
                            {
                                t.GroupId = null;
                            }

                            await _textRepository.EditMultipleAsync(textsForCurrentGroup);
                        }

                        //If there are associated images with current group delete them.
                        if (currentGroup.GroupImage.Any())
                        {
                            IEnumerable<GroupImage> groupImagesForCurrentGroup = currentGroup.GroupImage.ToList();

                            await _groupImageRepository.DeleteMultipleAsync(groupImagesForCurrentGroup);
                        }

                        if (currentGroup.Pdf.Any())
                        {
                            IEnumerable<Pdf> pdfsForCurrentGroup = currentGroup.Pdf.ToList();

                            foreach (Pdf p in pdfsForCurrentGroup)
                            {
                                p.GroupId = null;
                            }

                            await _pdfRepository.EditMultipleAsync(pdfsForCurrentGroup);
                        }
                    }

                    var languageGroupIgnore = await _languageGroupIgnoreRepository.List(lg => lg.GroupId == currentGroupId);

                    if(languageGroupIgnore.Count > 0)
                    {
                        await _languageGroupIgnoreRepository.DeleteMultipleAsync(languageGroupIgnore);
                    }

                    await _groupRepository.DeleteAsync(currentGroup);

                    transacton.Commit();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// Gets the number of texts which which are not related to any group for an application.
        /// </summary>
        /// <param name="applicationId">Application Id of the application.</param>
        /// <returns>Task<int></returns>
        public async Task<int> GetOrphanTextsCount(int applicationId)
        {
            try
            {
                var texts = await _textRepository.GetAllIncludingAsync(text => text.Section != null && text.Section.ApplicationId == applicationId);

                if (texts != null)
                {
                    int orphanTexts = await texts.Where(text => text.GroupId == null && text.SectionId != 0).CountAsync();

                    return orphanTexts;
                }

                return 0;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<int> GetOrphanPdfsCount(int applicationId)
        {
            try
            {
                var pdfs = await _pdfRepository.GetAllIncludingAsync(pdf => pdf.Section != null && pdf.Section.ApplicationId == applicationId);

                if (pdfs != null)
                {
                    int orphanPdfs = await pdfs.Where(pdf => pdf.GroupId == null && pdf.SectionId != 0).CountAsync();

                    return orphanPdfs;
                }

                return 0;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// Adds a new Group.
        /// </summary>
        /// <param name="group">Details of the new group to be added.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> AddGroup(Group group)
        {
            try
            {
                if (group != null)
                {
                    await _groupRepository.AddAsync(group);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To Get all the groups, related texts and translation for an application Id.
        /// </summary>
        /// <param name="applicationId">Application Id fetch all the groups and translations.</param>
        /// <returns>Task<IEnumerable<GroupAndTranslationsModel>></returns>
        public async Task<IEnumerable<GroupAndTranslationsModel>> GetGroupsAndTranslations(int applicationId)
        {
            try
            {
                var groups = await _groupRepository.GetAllIncludingAsync(group => group.ApplicationId == applicationId, group => group.Text);

                var groupsAndTranslations = await groups
                                                     .Select(group => new GroupAndTranslationsModel()
                                                     {
                                                         Id = group.Id,
                                                         Name = group.Name,
                                                         Sno = group.Sno,
                                                         ApplicationId = group.ApplicationId,
                                                         Text = group.Text.Select(text => new TextApiModel()
                                                         {
                                                             Id = text.Id,
                                                             Key = text.Key,
                                                             SectionId = text.SectionId,
                                                             GroupId = text.GroupId,
                                                             TextTranslations = text.TextTranslation.Where(tt => tt.LanguageId == 1).Select(tt => new TextTranslationApiModel()
                                                             {
                                                                 Id = tt.Id,
                                                                 Translation = tt.Translation
                                                             }).ToList()
                                                         }).ToList(),
                                                         Pdf = new List<PdfApiModel>()
                                                     })
                                                     .ToListAsync();
                return groupsAndTranslations;

            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<IEnumerable<GroupAndTranslationsModel>> GetGroupsAndPdfTranslations(int applicationId)
        {
            try
            {
                var groups = await _groupRepository.GetAllIncludingAsync(group => group.ApplicationId == applicationId, group => group.Pdf);

                var groupsAndTranslations = await groups
                                                     .Select(group => new GroupAndTranslationsModel()
                                                     {
                                                         Id = group.Id,
                                                         Name = group.Name,
                                                         Sno = group.Sno,
                                                         ApplicationId = group.ApplicationId,
                                                         Pdf = group.Pdf.Select(pdf => new PdfApiModel()
                                                         {
                                                             Id = pdf.Id,
                                                             Key = pdf.Key,
                                                             SectionId = pdf.SectionId,
                                                             GroupId = pdf.GroupId,
                                                             PdfTranslations = pdf.PdfTranslation.Where(tt => tt.LanguageId == 1).Select(tt => new PdfTranslationApiModelForGroup()
                                                             {
                                                                 Id = tt.Id,
                                                                 Translation = tt.FileName
                                                             }).ToList()
                                                         }).ToList(),
                                                         Text = new List<TextApiModel>()
                                                     })
                                                     .ToListAsync();
                return groupsAndTranslations;

            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To set the groupId of given TextId as null.
        /// </summary>
        /// <param name="textId">Text Id.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> RemoveTextFromGroup(int textId)
        {
            try
            {
                var text = await _textRepository.GetByIdAsync(textId);

                if (text == null)
                {
                    return false;
                }

                text.GroupId = null;

                await _textRepository.EditAsync(text, nameof(text.GroupId));

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<bool> RemovePdfFromGroup(int pdfId)
        {
            try
            {
                var pdf = await _pdfRepository.GetByIdAsync(pdfId);

                if (pdf == null)
                {
                    return false;
                }

                pdf.GroupId = null;

                await _pdfRepository.EditAsync(pdf, nameof(pdf.GroupId));

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To change the group Id of a Text.
        /// </summary>
        /// <param name="newGroupId">New groupId for the text.</param>
        /// <param name="textId">Id of the Text.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> MoveTextToAnotherGroup(int newGroupId, int textId)
        {
            try
            {
                var text = await _textRepository.GetByIdAsync(textId);

                if (text == null)
                {
                    return false;
                }

                text.GroupId = newGroupId;

                await _textRepository.EditAsync(text);

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<bool> MovePdfToAnotherGroup(int newGroupId, int pdfId)
        {
            try
            {
                var pdf = await _pdfRepository.GetByIdAsync(pdfId);

                if (pdf == null)
                {
                    return false;
                }

                pdf.GroupId = newGroupId;

                await _pdfRepository.EditAsync(pdf);

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }
        

        /// <summary>
        /// To update the GroupId for all the Texts. 
        /// </summary>
        /// <param name="texts">Information regarding Texts for which groupId is to be changed.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> UpdateTextsGroupId(IEnumerable<Text> texts)
        {
            try
            {
                List<int> ids = texts.Select(text => text.Id).ToList();

                IEnumerable<Text> textsFromStore = await _textRepository.List(t => ids.Contains(t.Id));

                if (textsFromStore != null)
                {
                    foreach (var item in textsFromStore)
                    {
                        item.GroupId = texts.FirstOrDefault(t => t.Id == item.Id)?.GroupId;
                    }

                    await _textRepository.EditMultipleAsync(textsFromStore);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<bool> UpdatePdfsGroupId(IEnumerable<Pdf> pdfs)
        {
            try
            {
                List<int> ids = pdfs.Select(pdf => pdf.Id).ToList();

                IEnumerable<Pdf> pdfsFromStore = await _pdfRepository.List(t => ids.Contains(t.Id));

                if (pdfsFromStore != null)
                {
                    foreach (var item in pdfsFromStore)
                    {
                        item.GroupId = pdfs.FirstOrDefault(t => t.Id == item.Id)?.GroupId;
                    }

                    await _pdfRepository.EditMultipleAsync(pdfsFromStore);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To update the GroupId for a single Text.
        /// </summary>
        /// <param name="text">Text for which groupId has to be updated.</param>
        /// <returns>Task<bool></returns>
        public async Task<bool> UpdateTextGroupId(Text text)
        {
            if (text == null)
            {
                return false;
            }

            try
            {
                var textFromStore = await _textRepository.GetByIdAsync(text.Id);

                if (textFromStore != null)
                {
                    textFromStore.GroupId = text.GroupId;

                    await _textRepository.EditAsync(textFromStore, nameof(textFromStore.GroupId));

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<bool> UpdatePdfGroupId(Pdf pdf)
        {
            if (pdf == null)
            {
                return false;
            }

            try
            {
                var pdfFromStore = await _pdfRepository.GetByIdAsync(pdf.Id);

                if (pdfFromStore != null)
                {
                    pdfFromStore.GroupId = pdf.GroupId;

                    await _pdfRepository.EditAsync(pdfFromStore, nameof(pdfFromStore.GroupId));

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<Application> GetApplicationAsync(int groupId)
        {
            if (groupId <= 0)
            {
                throw new ArgumentIsNullException($"{nameof(groupId)} is {groupId} and hence invalid.");
            }

            try
            {
                Group group = await _groupRepository
                                        .Where(gp => gp.Id == groupId)
                                        .Include(gp => gp.Application)
                                        .FirstOrDefaultAsync();

                if (group == null)
                {
                    throw new EntityDoesNotExistException($"No {nameof(Section)} exists with id ${groupId}");
                }

                return new Application
                {
                    Id = group.Application.Id,
                    IsVisible = group.Application.IsVisible,
                    TranslationTypeId = group.Application.TranslationTypeId,
                    IsLocked = group.Application.IsLocked
                };
            }
            catch (EntityDoesNotExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException($"Something went wrong on getting applicationId from group. {nameof(groupId)}={groupId}.", ex);
            }
        }

        public async Task<bool> AddNoteToGroup(int groupId, string note)
        {
            if(groupId <= 0)
            {
                return false;
            }

            try
            {
                var group = await _groupRepository.GetByIdAsync(groupId);

                if (group != null)
                {
                    group.Note = note;
                    await _groupRepository.EditAsync(group, nameof(group.Note));
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        public async Task<GroupApiModel> GetGroupAsync(int groupId, params Expression<Func<Group, object>>[] navigationPropertyPaths)
        {
            if (groupId <= 0)
            {
                throw new ArgumentIsNullException($"{nameof(groupId)} is {groupId} and hence invalid.");
            }

            try
            {
                var groups = _groupRepository.Where(g => g.Id == groupId);

                if (navigationPropertyPaths != null)
                {
                    foreach (var path in navigationPropertyPaths)
                    {
                        groups = groups.Include(path);
                    }
                }

                Group group = await groups.FirstOrDefaultAsync();
                return new GroupApiModel()
                {
                    Id = group.Id,
                    Name = group.Name,
                    LanguageGroupIgnores = group.LanguageGroupIgnore,
                    Note = group.Note,
                    ApplicationId = group.ApplicationId,
                };
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException($"Something went wrong on getting applicationId from group. {nameof(groupId)}={groupId}.", ex);
            }
        }
    }
}
