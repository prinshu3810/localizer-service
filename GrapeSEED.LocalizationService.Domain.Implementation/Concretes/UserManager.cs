﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Foundation;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.Extensions.Configuration;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Implementation.Concretes
{
    public class UserManager : IUserManager
    {
        private IConfiguration _config;
        private readonly IGeneralRepository<User> _userRepository;
        private readonly IEmailSender _emailSender;
        private readonly IGeneralRepository<UserLanguage> _userLanguageRepositry;
        private readonly IGeneralRepository<UserRole> _userRoleRepositry;

        public UserManager(
            IConfiguration config,
            IGeneralRepository<User> userRepository,
            INotificationManager notificationManager,
            IEmailSender emailSender,
            IGeneralRepository<UserLanguage> userLanguageRepository,
            IGeneralRepository<UserRole> userRoleRepositry)
        {
            _config = config;
            _userRepository = userRepository;
            _emailSender = emailSender;
            _userLanguageRepositry = userLanguageRepository;
            _userRoleRepositry = userRoleRepositry;
        }

        /// <summary>
        /// To set random password and send email to user's registered email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Task<int></returns>
        public async Task<ForgotPasswordStatus> ForgotPassword(string email)
        {
            ForgotPasswordStatus statusCode;
            try
            {
                var user = _userRepository.Where(u => u.Username.ToLower() == email.ToLower()).SingleOrDefault();
                if (user != null)
                {
                    if (user.Disabled)
                    {
                        statusCode = ForgotPasswordStatus.DisabledAccount;
                    }
                    else
                    {
                        //send email with temp password and update in 
                        var randomPassword = HelperMethods.GenerateRandomPassword();
                        string newEncPassword = EncryptionService.Encrypt(_config.GetSection("Settings")["encryptionKey"], randomPassword, true);
                        user.Password = newEncPassword;
                        user.IsPasswordVerified = false;
                        await _userRepository.EditAsync(user);
                        statusCode = ForgotPasswordStatus.Success;

                        // To send email with OTP and try again once in case of failure
                        await SendResetPasswordEmail(user.Username, randomPassword, true);
                    }
                }
                else
                {
                    statusCode = ForgotPasswordStatus.EmailNotExist;
                }
                return statusCode;
            }
            catch (EntityDoesNotExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on forgot password action.", ex);
            }
        }

        /// <summary>
        /// To send OPT email for forgot password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="randomPassword"></param>
        /// <param name="tryAgain"></param>
        private async Task SendResetPasswordEmail(string username, string randomPassword, bool tryAgain = false)
        {
            string name = username.Split('@')[0].Replace(".", " ");
            var response = await _emailSender.SendEmailAsync(null, null, username, Constants.FORGOT_PASSWORD_EMAIL_SUBJECT, GetForgotTemplate(name, randomPassword));
        }

        private static string GetForgotTemplate(string username, string randomPassword)
        {
            string bodyFirstLine = "Hi " + username + ",";
            string bodyMessageLine = Constants.FORGOT_PASSWORD_TEMPLATE.Replace("{OTP}", randomPassword);
            string bodyEndLine = "Thanks,<br/>Localizer Team";
            StringBuilder s = new StringBuilder();
            s.AppendLine("<div class=\"emailer\" style=\"background: #f4f4f4;padding: 30px;box-sizing: border-box;\">");
            s.AppendLine("<div class=\"emailer-content\" style=\"background: #FFF;max-width: 700px;min-width: 400px;margin: auto;box-shadow: 0px 5px 5px rgba(0,0,0,0.1);box-sizing: border-box;\">");
            s.AppendLine("<div class=\"emailer-body-message\" style=\"font-family: Arial, sans-serif; text-align: left; box-sizing: border-box; font-size: 16px; margin-bottom: 20px; color: #4a4a4a;\">" + bodyFirstLine + "<br/><br/>" + bodyMessageLine + "<br/><br/>" + bodyEndLine + "</div>");
            s.AppendLine("</div>");
            s.AppendLine("</div>");
            s.AppendLine("</div>");
            return s.ToString();
        }

        /// <summary>
        /// To update password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task UpdatePassword(User user)
        {
            try
            {
                await _userRepository.EditAsync(user);
            }
            catch (EntityDoesNotExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException("Something went wrong on updating user detail.", ex);
            }
        }

        public async Task<bool> ToggleNotificationEnabled(int userId)
        {
            // get the user from db
            var user = _userRepository.GetById(userId);
            // toggle the visiblity
            user.NotificationsEnabled = !user.NotificationsEnabled;
            // save to db
            await _userRepository.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// To get all the users with their roles and languages
        /// </summary>
        /// <returns>async Task<IEnumerable<UserModel>></returns>
        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            try
            {
                var allUsers = await _userRepository.GetAllIncludingAsync(user => user.InvitationStatus == true, user => user.UserRole, user => user.UserLanguage);

                var users =  allUsers
                .Select(user => new UserModel()
                {
                    Id = user.Id,
                    Name = user.Name == null || user.Name == "" ? user.Username : user.Name,
                    Email = user.Username,
                    Disabled = user.Disabled,
                    RoleIds = user.UserRole.Select(role => role.RoleId).ToList(),
                    LanguageIds = user.UserLanguage.Select(language => language.LanguageId).ToList()
                }).ToList();
                return users;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To save the user languages and roles.
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <param name="languageIds">Language Ids of the languages to be inserted in the UserLanguage table against the id of the user</param>
        /// <param name="roleIds">Role Ids of the roles to be inserted in the UserRole table against the id of the user</param>
        /// <returns>async Task<bool></returns>
        public async Task<bool> Save(int id, List<int> languageIds, List<int> roleIds)
        {
            try
            {
                var user = await _userRepository.GetByIdAsync(id);

                if (user == null)
                {
                    return false;
                }

                if (languageIds.Any())
                {
                    var currentUserLanguages = (await _userLanguageRepositry.GetAllAsync()).Where(role => role.UserId == id).ToList();

                    // Deletes the languages in UserLanguage table with UserId == id.
                    await _userLanguageRepositry.DeleteMultipleAsync(currentUserLanguages);

                    // Creates a list of UserId and LanguageId according to the languagesIds received to be inserted in UserLanguage table.
                    var newUserLanguages = languageIds
                                           .Select(languageId => new UserLanguage()
                                           {
                                               UserId = user.Id,
                                               LanguageId = languageId
                                           }).ToList();

                    await _userLanguageRepositry.AddMultipleAsync(newUserLanguages);
                }

                if (roleIds.Any())
                {
                    var currentUserRoles = (await _userRoleRepositry.GetAllAsync()).Where(role => role.UserId == id).ToList();

                    // Deletes the roles in UserRole table with UserId == id.
                    await _userRoleRepositry.DeleteMultipleAsync(currentUserRoles);

                    // Creates a list of UserId and RoleID according to the roleIds received to be inserted in UserRole table. 
                    var newUserRoles = roleIds
                                       .Select(roleId => new UserRole()
                                       {
                                           UserId = user.Id,
                                           RoleId = roleId
                                       }).ToList();

                    await _userRoleRepositry.AddMultipleAsync(newUserRoles);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// To save the invited user details and send an invite link to the user based on the email id received.
        /// </summary>
        /// <param name="email">Email Id of the user</param>
        /// <param name="languageIds">Language Ids of the languages to be inserted in the UserLanguage table against the id of the user</param>
        /// <param name="roleIds">Role Ids of the roles to be inserted in the UserRole table against the id of the user</param>
        /// <param name="name">Name of the user</param>
        /// <returns>async Task<bool></returns>
        public async Task<bool> InviteUser(string email, List<int> languageIds, List<int> roleIds, string name)
        {
            try
            {
                var invitationCode = HelperMethods.GenerateInvitationCode();

                var randomPassword = HelperMethods.GenerateRandomPassword();

                string newEncodedPassword = EncryptionService.Encrypt(_config.GetSection("Settings")["encryptionKey"], randomPassword, true);

                User user = new User()
                {
                    Username = email,
                    Password = newEncodedPassword,
                    IsPasswordVerified = false,
                    NotificationsEnabled = false,
                    Disabled = false,
                    InvitationStatus = false,
                    InvitationCode = invitationCode,
                    Name = name
                };

                if (languageIds.Any())
                {
                    // Creates a list of UserId and LanguageId according to the languagesIds received to be inserted in UserLanguage table.
                    var newUserLanguages = languageIds
                                            .Select(languageId => new UserLanguage()
                                            {
                                                UserId = user.Id,
                                                LanguageId = languageId
                                            }).ToList();

                    user.UserLanguage = newUserLanguages;
                }

                if (roleIds.Any())
                {
                    // Creates a list of UserId and RoleID according to the roleIds received to be inserted in UserRole table .
                    var newUserRoles = roleIds
                                            .Select(roleId => new UserRole()
                                            {
                                                UserId = user.Id,
                                                RoleId = roleId
                                            }).ToList();

                    user.UserRole = newUserRoles;
                }

                await _userRepository.AddAsync(user);

                var response = await SendInviteEmail(email, invitationCode);

                if (response)
                {
                    return true;
                }

                return false;
            }
            catch (SendMailFailed ex)
            {
                throw new SendMailFailed(ApplicationError.FAILED_EMAIL_SEND, ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// Sends and invitation to specified email.
        /// </summary>
        /// <param name="email">Email Id of the user</param>
        /// <param name="invitationCode">Invitation code of the user</param>
        /// <returns>async Task<bool></returns>
        private async Task<bool> SendInviteEmail(string email, string invitationCode)
        {
            string name = email.Split('@')[0].Replace(".", " ");

            // Gets the base url from the configuration file according to the envioronment.
            string baseUrl = _config.GetSection("Settings")["portalUrl"];

            string inviteLink = $"{baseUrl}ActivateAccount/{invitationCode}";

            string templatePath = "Inviteuser.html";

            string[] args = new string[2] { name, inviteLink };

            var response = await _emailSender.SendEmailAsync(email, Constants.INVITE_USER_SUBJECT, templatePath, args);

            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// To activate a user account which has a valid invitationcode.
        /// </summary>
        /// <param name="invitationCode">Invitation code</param>
        /// <param name="newPassword">New Password of the user</param>
        /// <returns>async Task<bool></returns>
        public async Task<bool> ActivateAccount(string invitationCode, string newPassword)
        {
            try
            {
                var user = _userRepository
                           .Where(u => u.InvitationStatus == false && u.IsPasswordVerified == false && string.Equals(u.InvitationCode, invitationCode))
                           ?.FirstOrDefault();

                if (user != null)
                {
                    var encryptedNewPassword = EncryptionService.Encrypt(_config.GetSection("Settings")["encryptionKey"], newPassword, true);

                    user.Password = encryptedNewPassword;

                    user.IsPasswordVerified = true;

                    user.InvitationStatus = true;

                    await UpdatePassword(user);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }

        /// <summary>
        /// Sets the Disabled property of the user to true or false.
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>async Task<bool></returns>
        public async Task<bool> ToggleDisabled(int id, bool disabled)
        {
            try
            {
                var user = await _userRepository.GetByIdAsync(id);

                if (user == null)
                {
                    return false;
                }

                user.Disabled = disabled;

                await _userRepository.EditAsync(user);

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseOpException(ApplicationError.FETCH_DATA, ex);
            }
        }
    }
}
