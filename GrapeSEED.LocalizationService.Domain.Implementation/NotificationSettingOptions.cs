﻿namespace GrapeSEED.LocalizationService.Domain.Implementation
{
    public class NotificationSettingOptions
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public string SenderEmail { get; set; }

        public string Password { get; set; }
    }
}
