﻿namespace GrapeSEED.LocalizationService.WebJob
{
    public class NotificationSettingOptions
    {
        public string LocalizerEmail { get; set; }

        /// <summary>
        /// Gets or sets the interval in days to pick changes made to translations.
        /// </summary>
        public int TranslationInterval { get; set; }

        public string Subject { get; set; }
    }
}
