﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GrapeSEED.LocalizationService.Foundation;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Foundation.Utils;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace GrapeSEED.LocalizationService.WebJob
{
    public class Program
    {
        private static IConfiguration Configuration { get; set; }

        private static string CurrentRunTimeEnvironment { get; set; }

        private static NotificationSettingOptions Options { get; set; }

        private static AuthMessageSenderOptions AppContext { get; set; }

        private static IEmailSender EmailSender { get; set; }

        private const string ENGLISH_LANG_NAME = "English";

        public static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            Console.WriteLine($"Web Job started on {CurrentRunTimeEnvironment} at {DateTime.Now.ToLongDateString()}");
            gclocalizationdbContext context = InjectDependencies(services);
            Execute(context);
            Cleanup();
        }

        private static gclocalizationdbContext InjectDependencies(IServiceCollection services)
        {
            ServiceProvider provider = services.BuildServiceProvider();
            gclocalizationdbContext context = provider.GetService<gclocalizationdbContext>();
            Options = provider.GetService<IOptions<NotificationSettingOptions>>().Value;
            AppContext = provider.GetService<IOptions<AuthMessageSenderOptions>>().Value;
            EmailSender = provider.GetService<IEmailSender>();
            return context;
        }

        private static void Execute(gclocalizationdbContext context)
        {
            var languages = context.Set<Language>().ToList();
            var languageList = languages.Select(x => new { x.Name, x.Id }).ToList();

            // for developer
            var languageSectionIgnoreList = context.Set<LanguageSectionIgnore>().ToList();
            // for translator
            var languageGroupIgnoreList = context.Set<LanguageGroupIgnore>().ToList();

            var languageApplicationIgnoreList = context.Set<LanguageApplicationIgnore>().ToList();

            // data for developer(according to Section for TextTranslations)
            var data = context
                        .Set<TextTranslation>()
                         .Where(t => (DateTimeUtils.UtcDateTime.Subtract(t.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)
                        || DateTimeUtils.UtcDateTime.Subtract(Convert.ToDateTime(t.UpdatedDate)) <= TimeSpan.FromDays(Options.TranslationInterval)))
                        .Include(t => t.Language)
                        .Include(t => t.Text)
                        .ThenInclude(t => t.Section)
                        .ThenInclude(t => t.Application).ToList();

            var textTranslationsByLanguage = data
                                     .GroupBy(t => new { LanguageName = t.Language.Name, t.LanguageId })
                                     .OrderBy(t => t.Key.LanguageName != ENGLISH_LANG_NAME).ToList();

            var textTranslationsByApplication = textTranslationsByLanguage.Select(grpByApp => new
            {
                grpByApp.Key.LanguageName,
                grpByApp.Key.LanguageId,
                textTranslations = grpByApp.GroupBy(t => t.Text.Section.Application.Name)
            }).ToList();

            // data for translator(according to Group for TextTranslations)
            var dataForGroup = context
                        .Set<TextTranslation>()
                        .Where(t => (DateTimeUtils.UtcDateTime.Subtract(t.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)
                        || DateTimeUtils.UtcDateTime.Subtract(Convert.ToDateTime(t.UpdatedDate)) <= TimeSpan.FromDays(Options.TranslationInterval))
                        && t.Text.Group != null)
                        .Include(t => t.Language)
                        .Include(t => t.Text)
                        .ThenInclude(t => t.Group)
                        .ThenInclude(t => t.Application).ToList();

            var textTranslationsByLanguageForGroup = dataForGroup
                                     .GroupBy(t => new { LanguageName = t.Language.Name, t.LanguageId })
                                     .OrderBy(t => t.Key.LanguageName != ENGLISH_LANG_NAME).ToList();

            var textTranslationsByApplicationForGroup = textTranslationsByLanguageForGroup.Select(grpByApp => new
            {
                grpByApp.Key.LanguageName,
                grpByApp.Key.LanguageId,
                textTranslations = grpByApp.Where(t => t.Text.Group != null).GroupBy(t => t.Text.Group.Application.Name)
            }).ToList();

            // for developer(according to Section for PdfTranslations)
            var pdfTranslations = context
                                    .Set<PdfTranslation>()
                                     .Include(t => t.Language)
                                     .Include(t => t.Pdf)
                                     .ThenInclude(t => t.Section)
                                     .ThenInclude(t => t.Application)
                                     .Where(t => t.Pdf.Section.Application.IsVisible && t.Pdf.Section.IsVisible).ToList();

            var pdfTranslationsByLanguage = pdfTranslations.Where(t => (DateTimeUtils.UtcDateTime.Subtract(t.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)
                                       || DateTimeUtils.UtcDateTime.Subtract(Convert.ToDateTime(t.UpdatedDate)) <= TimeSpan.FromDays(Options.TranslationInterval)))
                                        .GroupBy(t => new { LanguageName = t.Language.Name, t.LanguageId })
                                     .OrderBy(t => t.Key.LanguageName != ENGLISH_LANG_NAME).ToList();

            var pdfTranslationsByApplication = pdfTranslationsByLanguage.Select(grpByApp => new
            {
                grpByApp.Key.LanguageName,
                grpByApp.Key.LanguageId,
                pdfTranslations = grpByApp.GroupBy(t => t.Pdf.Section.Application.Name)
            }).ToList();

            //for translator(according to Group for PdfTranslations)
            var pdfTranslationsForGroup = context
                                    .Set<PdfTranslation>()
                                     .Include(t => t.Language)
                                     .Include(t => t.Pdf)
                                     .ThenInclude(t => t.Group)
                                     .ThenInclude(t => t.Application)
                                     .Where(t => t.Pdf.Group != null && t.Pdf.Group.Application.IsVisible && t.Pdf.Group.IsVisible).ToList();

            var pdfTranslationsByLanguageForGroup = pdfTranslationsForGroup.Where(t => (DateTimeUtils.UtcDateTime.Subtract(t.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)
                                        || DateTimeUtils.UtcDateTime.Subtract(Convert.ToDateTime(t.UpdatedDate)) <= TimeSpan.FromDays(Options.TranslationInterval)))
                                         .GroupBy(t => new { LanguageName = t.Language.Name, t.LanguageId })
                                      .OrderBy(t => t.Key.LanguageName != ENGLISH_LANG_NAME).ToList();

            var pdfTranslationsByApplicationForGroup = pdfTranslationsByLanguageForGroup.Select(grpByApp => new
            {
                grpByApp.Key.LanguageName,
                grpByApp.Key.LanguageId,
                pdfTranslations = grpByApp.Where(t => t.Pdf.Group != null).GroupBy(t => t.Pdf.Group.Application.Name)
            }).ToList();

            if (!textTranslationsByApplication.Any() && !pdfTranslationsByApplication.Any() && !textTranslationsByApplicationForGroup.Any() && !pdfTranslationsByApplicationForGroup.Any())
            {
                Console.WriteLine("No translations found.");
                return;
            }

            List<Notification> notifications = new List<Notification>();
            // find missing languages in text and file translations.
            var missingTextLanguages = languageList.Where(item => !textTranslationsByLanguage.Select(x => x.Key.LanguageName).Contains(item.Name)).ToList();
            var missingPdfLanguages = languageList.Where(item => !pdfTranslationsByLanguage.Select(x => x.Key.LanguageName).Contains(item.Name)).ToList();
            
            // find missing languages in text and file translations for group.
            var missingTextLanguagesForGroup = languageList.Where(item => !textTranslationsByLanguageForGroup.Select(x => x.Key.LanguageName).Contains(item.Name)).ToList();
            var missingPdfLanguagesForGroup = languageList.Where(item => !pdfTranslationsByLanguageForGroup.Select(x => x.Key.LanguageName).Contains(item.Name)).ToList();

            //TODO: Remove the except clause. Run with the except clause for now.
            var developers = context
                         .Set<User>()
                         .Include(u => u.UserRole)
                         .Where(u => u.UserRole.FirstOrDefault().RoleId == 1)
                         .Select(u => u.Username);

            var translatorDetails = context
                        .Set<User>()
                        .Where(u => u.UserRole.FirstOrDefault().RoleId == 2 && u.NotificationsEnabled == true)
                        .Include(u => u.UserRole)
                        .Include(u => u.UserLanguage);
            
            var translatorLanguageMapping = translatorDetails.Select(t => new { t.Id, t.Username, userLanguage = t.UserLanguage.Select(l => l.LanguageId) });

            // Notifcations for developer
            List<LanguageNotificationInfo> notificationInfo = new List<LanguageNotificationInfo>();

            // Notifications for translator.
            List<LanguageNotificationInfo> notificationInfoForGroup = new List<LanguageNotificationInfo>();

            // for developer
            if (textTranslationsByApplication.Any())
            {
                List<AppTranslationMapping> appTranslationMapping = new List<AppTranslationMapping>();

                foreach (var translatedTranslation in textTranslationsByApplication)
                {
                    var ignoredAppIds = languageApplicationIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.ApplicationId);
                    var ignoredSectionIds = languageSectionIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.SectionId);

                    if (translatedTranslation.LanguageName != ENGLISH_LANG_NAME)
                    {
                        var appInTranslations = translatedTranslation.textTranslations.Select(x => x.Key).ToList();
                        var notExistingApps = appTranslationMapping.Where(x => !appInTranslations.Contains(x.ApplicationName) && !ignoredAppIds.Contains(x.ApplicationId)).Select(x => x.ApplicationName).ToList();

                        if (notExistingApps.Any())
                        {
                            notExistingApps.ForEach(app =>
                            {
                                if (appTranslationMapping.First(x => x.ApplicationName == app).Translations.Any())
                                {
                                    notificationInfo.Add(new LanguageNotificationInfo()
                                    {
                                        LanguageName = translatedTranslation.LanguageName,
                                        ApplicationName = app,
                                        CreatedCount = appTranslationMapping.First(x => x.ApplicationName == app).Translations.Count(),
                                        UpdatedCount = 0,
                                        PublishedCount = 0,
                                        TranslationTypeId = 1
                                    });
                                }
                            });
                        }
                    }

                    foreach (var translationsInApp in translatedTranslation.textTranslations)
                    {
                        string appName = translationsInApp.FirstOrDefault().Text.Section.Application.Name;
                        int appId = translationsInApp.FirstOrDefault().Text.Section.Application.Id;

                        var visibleTranslations = translationsInApp
                            .Where(t => !ignoredAppIds.Contains(t.Text.Section.ApplicationId) && !ignoredSectionIds.Contains(t.Text.SectionId) && t.IsVisible == true);

                        int createdCount = 0;
                        int updatedCount = visibleTranslations.Count(item => item.StatusId == 2);
                        int publishedCount = visibleTranslations.Count(item => item.StatusId == 3);
                        if (translatedTranslation.LanguageName == ENGLISH_LANG_NAME)
                        {
                            // add new english translations in  mapping list
                            var newTranslations = translationsInApp.Where(x => DateTimeUtils.UtcDateTime.Subtract(x.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)).ToList();
                            AppTranslationMapping mappingItem = new AppTranslationMapping { ApplicationName = appName, Translations = newTranslations, ApplicationId = appId };
                            appTranslationMapping.Add(mappingItem);

                            createdCount = visibleTranslations.Where(t => t.StatusId == 1).Count();
                            if (newTranslations.Any())
                            {
                                missingTextLanguages.ForEach(language =>
                                {
                                    if (!languageApplicationIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.ApplicationId).Contains(appId))
                                    {
                                        var ignoredSections = languageSectionIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.SectionId);

                                        if (newTranslations.Where(x => !ignoredSections.Contains(x.Text.SectionId)).Any())
                                        {
                                            notificationInfo.Add(new LanguageNotificationInfo()
                                            {
                                                LanguageName = language.Name,
                                                ApplicationName = appName,
                                                CreatedCount = newTranslations.Where(x => !ignoredSections.Contains(x.Text.SectionId)).Count(),
                                                UpdatedCount = 0,
                                                PublishedCount = 0,
                                                TranslationTypeId = 1
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        else
                        {
                            if (appTranslationMapping.Exists(x => x.ApplicationName == appName))
                            {
                                var newEnglishTranslations = appTranslationMapping.Find(x => x.ApplicationName == appName).Translations.Distinct();
                                List<int> newTranslatedTranslations = new List<int>();

                                if (!ignoredAppIds.Contains(appId))
                                {
                                    newEnglishTranslations.Where(x => !ignoredSectionIds.Contains(x.Text.SectionId)).ToList().ForEach(engTranslation =>
                                    {
                                        if (!translationsInApp.ToList().Exists(translation => translation.TextId == engTranslation.TextId))
                                        {
                                            newTranslatedTranslations.Add(engTranslation.Id);
                                        }
                                    });
                                }

                                createdCount = newTranslatedTranslations.Count();
                            }
                        }
                        if (createdCount > 0 || updatedCount > 0 || publishedCount > 0)
                        {
                            notificationInfo.Add(new LanguageNotificationInfo()
                            {
                                LanguageName = translationsInApp.FirstOrDefault().Language.Name,
                                ApplicationName = appName,
                                CreatedCount = createdCount,
                                UpdatedCount = updatedCount,
                                PublishedCount = publishedCount,
                                TranslationTypeId = 1
                            });
                        }

                    }
                }
            }

            if (textTranslationsByApplicationForGroup.Any())
            {
                List<AppTranslationMapping> appTranslationMapping = new List<AppTranslationMapping>();

                foreach (var translatedTranslation in textTranslationsByApplicationForGroup)
                {
                    var ignoredAppIds = languageApplicationIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.ApplicationId);

                    var ignoredGroupIds = languageGroupIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.GroupId);

                    if (translatedTranslation.LanguageName != ENGLISH_LANG_NAME)
                    {
                        var appInTranslations = translatedTranslation.textTranslations.Select(x => x.Key).ToList();
                        var notExistingApps = appTranslationMapping.Where(x => !appInTranslations.Contains(x.ApplicationName) && !ignoredAppIds.Contains(x.ApplicationId)).Select(x => x.ApplicationName).ToList();


                        if (notExistingApps.Any())
                        {
                            notExistingApps.ForEach(app =>
                            {
                                if (appTranslationMapping.First(x => x.ApplicationName == app).Translations.Any())
                                {
                                    notificationInfoForGroup.Add(new LanguageNotificationInfo()
                                    {
                                        LanguageName = translatedTranslation.LanguageName,
                                        ApplicationName = app,
                                        CreatedCount = appTranslationMapping.First(x => x.ApplicationName == app).Translations.Count(),
                                        UpdatedCount = 0,
                                        PublishedCount = 0,
                                        TranslationTypeId = 1
                                    });
                                }
                            });
                        }
                    }

                    foreach (var translationsInApp in translatedTranslation.textTranslations)
                    {
                        string appName = translationsInApp.FirstOrDefault().Text.Group.Application.Name;
                        int appId = translationsInApp.FirstOrDefault().Text.Group.Application.Id;

                        //TODO: visibleTranslation will now be taken from ignored group ids -> LanguageGroupIgnore
                        var visibleTranslations = translationsInApp
                            .Where(t => !ignoredAppIds.Contains(t.Text.Group.ApplicationId) && !ignoredGroupIds.Contains(t.Text.GroupId ?? 0) && t.IsVisible == true);

                        int createdCount = 0;
                        int updatedCount = visibleTranslations.Count(item => item.StatusId == 2);
                        int publishedCount = visibleTranslations.Count(item => item.StatusId == 3);
                        if (translatedTranslation.LanguageName == ENGLISH_LANG_NAME)
                        {
                            // add new english translations in  mapping list
                            var newTranslations = translationsInApp.Where(x => DateTimeUtils.UtcDateTime.Subtract(x.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)).ToList();
                            AppTranslationMapping mappingItem = new AppTranslationMapping { ApplicationName = appName, Translations = newTranslations, ApplicationId = appId };
                            appTranslationMapping.Add(mappingItem);

                            createdCount = visibleTranslations.Where(t => t.StatusId == 1).Count();
                            if (newTranslations.Any())
                            {
                                missingTextLanguagesForGroup.ForEach(language =>
                                {
                                    if (!languageApplicationIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.ApplicationId).Contains(appId))
                                    {
                                        var ignoredGroups = languageGroupIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.GroupId);

                                        if (newTranslations.Where(x => !ignoredGroups.Contains(x.Text.GroupId ?? 0)).Any())
                                        {
                                            notificationInfoForGroup.Add(new LanguageNotificationInfo()
                                            {
                                                LanguageName = language.Name,
                                                ApplicationName = appName,
                                                CreatedCount = newTranslations.Where(x => !ignoredGroups.Contains(x.Text.GroupId ?? 0)).Count(),
                                                UpdatedCount = 0,
                                                PublishedCount = 0,
                                                TranslationTypeId = 1
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        else
                        {
                            if (appTranslationMapping.Exists(x => x.ApplicationName == appName))
                            {
                                var newEnglishTranslations = appTranslationMapping.Find(x => x.ApplicationName == appName).Translations.Distinct();
                                List<int> newTranslatedTranslations = new List<int>();

                                if (!ignoredAppIds.Contains(appId))
                                {
                                    newEnglishTranslations.Where(x => !ignoredGroupIds.Contains(x.Text.GroupId ?? 0)).ToList().ForEach(engTranslation =>
                                    {
                                        if (!translationsInApp.ToList().Exists(translation => translation.TextId == engTranslation.TextId))
                                        {
                                            newTranslatedTranslations.Add(engTranslation.Id);
                                        }
                                    });
                                }

                                createdCount = newTranslatedTranslations.Count();
                            }
                        }
                        if (createdCount > 0 || updatedCount > 0 || publishedCount > 0)
                        {
                            notificationInfoForGroup.Add(new LanguageNotificationInfo()
                            {
                                LanguageName = translationsInApp.FirstOrDefault().Language.Name,
                                ApplicationName = appName,
                                CreatedCount = createdCount,
                                UpdatedCount = updatedCount,
                                PublishedCount = publishedCount,
                                TranslationTypeId = 1
                            });
                        }

                    }
                }
            }

            if (pdfTranslationsByApplication.Any())
            {
                List<AppPdfTranslationMapping> appPdfTranslationMapping = new List<AppPdfTranslationMapping>();
                foreach (var translatedTranslation in pdfTranslationsByApplication)
                {
                    var ignoredAppIds = languageApplicationIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.ApplicationId);

                    var ignoredSectionIds = languageSectionIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.SectionId);

                    if (translatedTranslation.LanguageName != ENGLISH_LANG_NAME)
                    {
                        var appInTranslations = translatedTranslation.pdfTranslations.Select(x => x.Key).ToList();
                        //add any missing apps in translated language with only created count (if any)
                        var notExistingApps = appPdfTranslationMapping.Where(x => !appInTranslations.Contains(x.ApplicationName) && !ignoredAppIds.Contains(x.ApplicationId))
                                             .Select(x => x.ApplicationName).ToList();

                        if (notExistingApps.Any())
                        {
                            notExistingApps.ForEach(app =>
                            {
                                if (appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Any())
                                {
                                    if (appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Select(x => x.PdfId).Distinct().Any())
                                    {
                                        notificationInfo.Add(new LanguageNotificationInfo()
                                        {
                                            LanguageName = translatedTranslation.LanguageName,
                                            ApplicationName = app,
                                            CreatedCount = appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Select(x => x.PdfId).Distinct().Count(),
                                            UpdatedCount = 0,
                                            PublishedCount = 0,
                                            TranslationTypeId = 2
                                        });
                                    }
                                }

                            });
                        }
                    }
                    foreach (var translationsInApp in translatedTranslation.pdfTranslations)
                    {
                        string appName = translationsInApp.FirstOrDefault().Pdf.Section.Application.Name;
                        int appId = translationsInApp.FirstOrDefault().Pdf.Section.Application.Id;

                        var visiblePdfTranslations = translationsInApp
                           .Where(t => !ignoredAppIds.Contains(t.Pdf.Section.ApplicationId) && !ignoredSectionIds.Contains(t.Pdf.SectionId) && t.IsVisible == true);
                        int createdCount = 0;
                        int updatedCount = visiblePdfTranslations.Where(t => t.StatusId == 2).Select(x => x.PdfId).Distinct().Count();
                        int publishedCount = visiblePdfTranslations.Where(t => t.StatusId == 3).Select(x => x.PdfId).Distinct().Count();
                        if (translatedTranslation.LanguageName == ENGLISH_LANG_NAME)
                        {
                            var newTranslations = pdfTranslations.Where(x => x.LanguageId == translatedTranslation.LanguageId && x.Pdf.Section.ApplicationId == appId)
                                .GroupBy(x => x.PdfId)
                                .Select(x => x.OrderBy(t => t.CreatedDate).FirstOrDefault())
                                .Where(x => DateTimeUtils.UtcDateTime.Subtract(x.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)).ToList();
                            //create a mapping of application name and newly added translations
                            AppPdfTranslationMapping mappingItem = new AppPdfTranslationMapping { ApplicationName = appName, Translations = newTranslations, ApplicationId = appId };
                            appPdfTranslationMapping.Add(mappingItem);
                            createdCount = visiblePdfTranslations.Where(t => t.StatusId == 1).Select(x => x.PdfId).Distinct().Count();
                            //add notification for any missing language with only created count (if any)
                            if (newTranslations.Any())
                            {
                                missingPdfLanguages.ForEach(language =>
                                {
                                    if (!languageApplicationIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.ApplicationId).Contains(appId))
                                    {
                                        var ignoredSections = languageSectionIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.SectionId);

                                        notificationInfo.Add(new LanguageNotificationInfo()
                                        {
                                            LanguageName = language.Name,
                                            ApplicationName = appName,
                                            CreatedCount = newTranslations.Where(x => !ignoredSections.Contains(x.Pdf.SectionId)).Select(x => x.PdfId).Distinct().Count(),
                                            UpdatedCount = 0,
                                            PublishedCount = 0,
                                            TranslationTypeId = 2

                                        });
                                    }
                                });
                            }

                        }
                        else
                        {
                            if (appPdfTranslationMapping.Exists(x => x.ApplicationName == appName))
                            {
                                var newEnglishTranslations = appPdfTranslationMapping.Find(x => x.ApplicationName == appName).Translations.Distinct();
                                List<int> newTranslatedTranslations = new List<int>();

                                if (!ignoredAppIds.Contains(appId))
                                {
                                    newEnglishTranslations.Where(x => !ignoredSectionIds.Contains(x.Pdf.SectionId)).ToList().ForEach(engTranslation =>
                                    {
                                        if (!translationsInApp.ToList().Exists(translation => translation.PdfId == engTranslation.PdfId))
                                        {
                                            newTranslatedTranslations.Add(engTranslation.PdfId);
                                        }
                                    });
                                }

                                createdCount = newTranslatedTranslations.Distinct().Count();
                            }
                        }
                        if (createdCount > 0 || updatedCount > 0 || publishedCount > 0)
                        {
                            notificationInfo.Add(new LanguageNotificationInfo()
                            {
                                LanguageName = translationsInApp.FirstOrDefault().Language.Name,
                                ApplicationName = appName,
                                CreatedCount = createdCount,
                                UpdatedCount = updatedCount,
                                PublishedCount = publishedCount,
                                TranslationTypeId = 2
                            });
                        }
                    }
                }
            }

            if (pdfTranslationsByApplicationForGroup.Any())
            {
                List<AppPdfTranslationMapping> appPdfTranslationMapping = new List<AppPdfTranslationMapping>();
                foreach (var translatedTranslation in pdfTranslationsByApplicationForGroup)
                {
                    var ignoredAppIds = languageApplicationIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.ApplicationId);

                    var ignoredGroupIds = languageGroupIgnoreList.Where(x => x.LanguageId == translatedTranslation.LanguageId).Select(x => x.GroupId);

                    if (translatedTranslation.LanguageName != ENGLISH_LANG_NAME)
                    {
                        var appInTranslations = translatedTranslation.pdfTranslations.Select(x => x.Key).ToList();
                        //add any missing apps in translated language with only created count (if any)
                        var notExistingApps = appPdfTranslationMapping.Where(x => !appInTranslations.Contains(x.ApplicationName) && !ignoredAppIds.Contains(x.ApplicationId))
                                             .Select(x => x.ApplicationName).ToList();

                        if (notExistingApps.Any())
                        {
                            notExistingApps.ForEach(app =>
                            {
                                if (appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Any())
                                {
                                    if (appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Select(x => x.PdfId).Distinct().Any())
                                    {
                                        notificationInfoForGroup.Add(new LanguageNotificationInfo()
                                        {
                                            LanguageName = translatedTranslation.LanguageName,
                                            ApplicationName = app,
                                            CreatedCount = appPdfTranslationMapping.First(x => x.ApplicationName == app).Translations.Select(x => x.PdfId).Distinct().Count(),
                                            UpdatedCount = 0,
                                            PublishedCount = 0,
                                            TranslationTypeId = 2
                                        });
                                    }
                                }

                            });
                        }
                    }
                    foreach (var translationsInApp in translatedTranslation.pdfTranslations)
                    {
                        string appName = translationsInApp.FirstOrDefault().Pdf.Group.Application.Name;
                        int appId = translationsInApp.FirstOrDefault().Pdf.Group.Application.Id;

                        var visiblePdfTranslations = translationsInApp
                           .Where(t => !ignoredAppIds.Contains(t.Pdf.Group.ApplicationId) && !ignoredGroupIds.Contains(t.Pdf.GroupId ?? 0) && t.IsVisible == true);
                        int createdCount = 0;
                        int updatedCount = visiblePdfTranslations.Where(t => t.StatusId == 2).Select(x => x.PdfId).Distinct().Count();
                        int publishedCount = visiblePdfTranslations.Where(t => t.StatusId == 3).Select(x => x.PdfId).Distinct().Count();
                        if (translatedTranslation.LanguageName == ENGLISH_LANG_NAME)
                        {
                            var newTranslations = pdfTranslationsForGroup.Where(x => x.LanguageId == translatedTranslation.LanguageId && x.Pdf.Group.ApplicationId == appId)
                                .GroupBy(x => x.PdfId)
                                .Select(x => x.OrderBy(t => t.CreatedDate).FirstOrDefault())
                                .Where(x => DateTimeUtils.UtcDateTime.Subtract(x.CreatedDate) <= TimeSpan.FromDays(Options.TranslationInterval)).ToList();
                            //create a mapping of application name and newly added translations
                            AppPdfTranslationMapping mappingItem = new AppPdfTranslationMapping { ApplicationName = appName, Translations = newTranslations, ApplicationId = appId };
                            appPdfTranslationMapping.Add(mappingItem);
                            createdCount = visiblePdfTranslations.Where(t => t.StatusId == 1).Select(x => x.PdfId).Distinct().Count();
                            //add notification for any missing language with only created count (if any)
                            if (newTranslations.Any())
                            {
                                missingPdfLanguagesForGroup.ForEach(language =>
                                {
                                    if (!languageApplicationIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.ApplicationId).Contains(appId))
                                    {
                                        var ignoredGroups = languageGroupIgnoreList.Where(x => x.LanguageId == language.Id).Select(x => x.GroupId);

                                        notificationInfoForGroup.Add(new LanguageNotificationInfo()
                                        {
                                            LanguageName = language.Name,
                                            ApplicationName = appName,
                                            CreatedCount = newTranslations.Where(x => !ignoredGroupIds.Contains(x.Pdf.GroupId ?? 0)).Select(x => x.PdfId).Distinct().Count(),
                                            UpdatedCount = 0,
                                            PublishedCount = 0,
                                            TranslationTypeId = 2
                                        });
                                    }
                                });
                            }

                        }
                        else
                        {
                            if (appPdfTranslationMapping.Exists(x => x.ApplicationName == appName))
                            {
                                var newEnglishTranslations = appPdfTranslationMapping.Find(x => x.ApplicationName == appName).Translations.Distinct();
                                List<int> newTranslatedTranslations = new List<int>();

                                if (!ignoredAppIds.Contains(appId))
                                {
                                    newEnglishTranslations.Where(x => !ignoredGroupIds.Contains(x.Pdf.GroupId ?? 0)).ToList().ForEach(engTranslation =>
                                    {
                                        if (!translationsInApp.ToList().Exists(translation => translation.PdfId == engTranslation.PdfId))
                                        {
                                            newTranslatedTranslations.Add(engTranslation.PdfId);
                                        }
                                    });
                                }

                                createdCount = newTranslatedTranslations.Distinct().Count();
                            }
                        }
                        if (createdCount > 0 || updatedCount > 0 || publishedCount > 0)
                        {
                            notificationInfoForGroup.Add(new LanguageNotificationInfo()
                            {
                                LanguageName = translationsInApp.FirstOrDefault().Language.Name,
                                ApplicationName = appName,
                                CreatedCount = createdCount,
                                UpdatedCount = updatedCount,
                                PublishedCount = publishedCount,
                                TranslationTypeId = 2
                            });
                        }
                    }
                }
            }

            if (notificationInfo.Any())
            {
                StringBuilder developerEmailBody = CreateTableHeader();
                var notificationBody = notificationInfo.OrderBy(x => x.LanguageName).ThenBy(x => x.ApplicationName)
                                    .GroupBy(info => new { info.LanguageName })
                                    .Select(info => new
                                    {
                                        info.Key.LanguageName,
                                        notificationDetails = info.GroupBy(grp => grp.ApplicationName).Select(not => new LanguageNotificationInfo
                                        {
                                            ApplicationName = not.Key,
                                            CreatedCount = not.Sum(x => x.CreatedCount),
                                            UpdatedCount = not.Sum(x => x.UpdatedCount),
                                            PublishedCount = not.Sum(x => x.PublishedCount)
                                        }).ToList()
                                    }).ToList();

                var developerNotificationList = notificationBody;
                foreach (var notification in developerNotificationList)
                {
                    developerEmailBody = CreateTable(developerEmailBody, notification.LanguageName.ToString());

                    developerEmailBody = developerEmailBody.Replace("{{rows}}", string.Join('\n', notification.notificationDetails.Select(x => x.ToString())));
                    developerEmailBody = EndTable(developerEmailBody);
                }
                developerEmailBody = AddLocalizerLink(developerEmailBody);
                developerEmailBody = EndDocument(developerEmailBody);

                NotificationModel developerModel = new NotificationModel()
                {
                    FromAddress = Options.LocalizerEmail,
                    ToAddresses = new[] { Options.LocalizerEmail },
                    BccAddresses = developers,
                    Subject = Options.Subject,
                    Body = Convert.ToString(developerEmailBody),
                };

                var emailResponse = EmailSender.SendEmailsAsync(developerModel).GetAwaiter().GetResult();

                notifications.Add(new Notification()
                {
                    Body = Convert.ToString(developerEmailBody),
                    Recipients = string.Join(',', developers),
                    RoleId = 1,
                    NotificationStatusId = emailResponse.StatusCode == System.Net.HttpStatusCode.Accepted ? 1 : 2
                });

                if (emailResponse.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    Console.WriteLine($"Email Sent to developers {string.Join(',', developers)}");
                }

                context.Set<Notification>().AddRange(notifications.Where(n => n.RoleId == 1));
                context.SaveChanges();
            }

            // for translator.
            if (notificationInfoForGroup.Any())
            {
                var notificationBody = notificationInfoForGroup.OrderBy(x => x.LanguageName).ThenBy(x => x.ApplicationName)
                                    .GroupBy(info => new { info.LanguageName })
                                    .Select(info => new
                                    {
                                        info.Key.LanguageName,
                                        notificationDetails = info.GroupBy(grp => grp.ApplicationName).Select(not => new LanguageNotificationInfo
                                        {
                                            ApplicationName = not.Key,
                                            CreatedCount = not.Sum(x => x.CreatedCount),
                                            UpdatedCount = not.Sum(x => x.UpdatedCount),
                                            PublishedCount = not.Sum(x => x.PublishedCount)
                                        }).ToList()
                                    }).ToList();

                translatorLanguageMapping.ToList().ForEach(translator =>
                {
                    var translatorEmailBody = CreateTableHeader();
                    notifications = new List<Notification>();
                    var translatorLanguages = languages.Where(l => translator.userLanguage.Contains(l.Id)).Select(l => l.Name);
                    var translatorNotificationList = notificationBody.Where(n => translatorLanguages.Contains(n.LanguageName));
                    //create email body
                    foreach (var notification in translatorNotificationList)
                    {
                        translatorEmailBody = CreateTable(translatorEmailBody, notification.LanguageName.ToString());
                        translatorEmailBody = translatorEmailBody.Replace("{{rows}}", string.Join('\n', notification.notificationDetails.Select(x => x.ToString())));
                        translatorEmailBody = EndTable(translatorEmailBody);
                    }
                    translatorEmailBody = AddLocalizerLink(translatorEmailBody);
                    translatorEmailBody = EndDocument(translatorEmailBody);
                    NotificationModel translatorModel = new NotificationModel()
                    {
                        FromAddress = Options.LocalizerEmail,
                        ToAddresses = new[] { Options.LocalizerEmail },
                        BccAddresses = new List<string> { translator.Username },
                        Subject = Options.Subject,
                        Body = Convert.ToString(translatorEmailBody),
                    };
                    var translatorEmailResponse = EmailSender.SendEmailsAsync(translatorModel).GetAwaiter().GetResult();
                    notifications.Add(new Notification()
                    {
                        Body = Convert.ToString(translatorEmailBody),
                        Recipients = string.Join(',', translator),
                        RoleId = 1,
                        NotificationStatusId = translatorEmailResponse.StatusCode == System.Net.HttpStatusCode.Accepted ? 1 : 2
                    });

                    context.Set<Notification>().AddRange(notifications.Where(n => n.RoleId == 2));
                    context.SaveChanges();
                });
            }
            // Delete Old Notifications greater than equal to 30 days
            var oldNotifications = context.Set<Notification>().Where(t => DateTimeUtils.UtcDateTime.Subtract(t.CreatedDate) > TimeSpan.FromDays(30));
            context.Set<Notification>().RemoveRange(oldNotifications);
            context.SaveChanges();
        }

        /// <summary>
        /// <para>
        /// This method is/would be essential in case this webjob is required to be a continuous task.
        /// This would enable them to be GC collected. 
        /// </para>
        /// </summary>
        private static void Cleanup()
        {
            Configuration = null;
            CurrentRunTimeEnvironment = null;
            Options = null;
            EmailSender = null;
            AppContext = null;
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "environmentConfig.json");

            using (StreamReader reader = new StreamReader(path))
            {
                string content = reader.ReadToEnd();
                EnvironmentConfigSettings settings = JsonConvert.DeserializeObject<EnvironmentConfigSettings>(content);

                if (settings.azurelocal)
                {
                    CurrentRunTimeEnvironment = nameof(settings.azurelocal);
                }
                if (settings.azuredev)
                {
                    CurrentRunTimeEnvironment = nameof(settings.azuredev);
                }
                if (settings.azureprod)
                {
                    CurrentRunTimeEnvironment = nameof(settings.azureprod);
                }
            }

            IConfigurationBuilder builder = new ConfigurationBuilder()
                                            .SetBasePath(Directory.GetCurrentDirectory())
                                            .AddJsonFile($"appsettings.json", true, true)
                                            .AddEnvironmentVariables();

            if (!string.IsNullOrEmpty(CurrentRunTimeEnvironment))
            {
                builder.AddJsonFile($"appsettings.{CurrentRunTimeEnvironment}.json", true, true);
            }

            Configuration = builder.Build();
            var connectString = Environment.GetEnvironmentVariable("LocalizerConnectionString") ?? Configuration.GetConnectionString("LocalizerConnectionString");

            services
                .AddSingleton(_ => Configuration)
                .AddDbContextPool<gclocalizationdbContext>(options => options.UseSqlServer(connectString))
                .AddScoped<IEmailSender, AuthMessageSender>()
                .Configure<NotificationSettingOptions>(Configuration.GetSection("NotificationSettings"))
                .Configure<AuthMessageSenderOptions>(Configuration.GetSection("Settings"));
        }

        private static StringBuilder CreateTableHeader()
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine("<div class=\"emailer\" style=\"background: #fff;padding: 0;box-sizing: border-box;\">");
            s.AppendLine("<div class=\"emailer-content\" style=\"background: #FFF;max-width: 700px;margin: auto;box-shadow: 0px 5px 5px rgba(0,0,0,0.1);box-sizing: border-box;\">");
            s.AppendLine("<div class=\"emailer - header\" style=\"background: #ab447a;padding: 20px;height:70px;\">");
            s.AppendLine("&nbsp;&nbsp;&nbsp;&nbsp;<div class=\"emailer-name\" style=\" padding: 0 20px;  align-items: center; justify-content: center; color: #FFF;font-family: Arial, sans-serif;font-size: 25px;letter-spacing: 0.4px;\"><span  style=\" color: #ab447a;font-size: 25px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>Localizer Notification</div>&nbsp;&nbsp;&nbsp;&nbsp;");
            s.AppendLine("</div>");
            s.AppendLine("<div class=\"emailer-body\" style=\"box-sizing: border-box; padding: 40px 0; background: #FFF; font-size: 16px; font-family: Arial, sans-serif; color: #6d6d6d;\">");
            s.AppendLine("<div class=\"emailer-body-title\" style=\"font-family: Arial, sans-serif; text-align: center; box-sizing: border-box; font-size: 21px; margin: 15px; color: #4a4a4a;\">Updates on Localizer</div>");
            return s;
        }

        private static StringBuilder CreateTable(StringBuilder s, string LanguageName)
        {
            s.AppendLine($"<div class=\"language-section\" style=\"font-family: Arial, sans-serif; text-align: left; box-sizing: border-box; font-size: 16px;font-weight:bold; margin: 10px; color: #4a4a4a;\">{LanguageName}</div>");
            s.AppendLine("<table style=\"width: 100%; border: 2px solid #DDD;\" cellpadding=\"0\" colspacing=\"0\">");
            s.AppendLine("<thead>");
            s.AppendLine("<tr>");
            s.AppendLine("<th style=\"text-align: left; padding: 8px; background: #EEE; color: #5d5d5d; font-size: 14px; text-transform: uppercase;\">Application</th>");
            s.AppendLine("<th style=\"text-align: left; padding: 8px; background: #EEE; color: #5d5d5d; font-size: 14px; text-transform: uppercase;\">Created</th>");
            s.AppendLine("<th style=\"text-align: left; padding: 8px; background: #EEE; color: #5d5d5d; font-size: 14px; text-transform: uppercase;\">Updated</th>");
            s.AppendLine("<th style=\"text-align: left; padding: 8px; background: #EEE; color: #5d5d5d; font-size: 14px; text-transform: uppercase;\">Published</th>");
            s.AppendLine("</tr>");
            s.AppendLine("</thead>");
            s.AppendLine("<tbody>");
            s.AppendLine("{{rows}}");
            return s;
        }

        private static StringBuilder EndTable(StringBuilder s)
        {
            s.AppendLine("</tbody>");
            s.AppendLine("</table>");
            return s;
        }

        private static StringBuilder EndDocument(StringBuilder s)
        {
            s.AppendLine("</div>");
            s.AppendLine("</div>");
            s.AppendLine("</div>");
            return s;
        }

        private static StringBuilder AddLocalizerLink(StringBuilder s)
        {
            s.AppendLine("<div class=\"localizer-link\" style=\"font-family: Arial, sans-serif; text-align: left; box-sizing: border-box; margin-top: 30px; color: #4a4a4a;\">");
            s.AppendLine($@"<a href='{AppContext.LocalizerAppUrl}'>Click here</a> to open the Localizer and confirm the translations. </div>");

            return s;
        }

        private class LanguageNotificationInfo
        {
            internal string LanguageName { get; set; }

            internal string ApplicationName { get; set; }

            internal int CreatedCount { get; set; }

            internal int UpdatedCount { get; set; }

            internal int PublishedCount { get; set; }

            internal int TranslationTypeId { get; set; }

            public override string ToString()
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("<tr>");
                builder.AppendLine($"<td style=\"padding: 8px; font-size: 14px; color: #6d6d6d; border-bottom: 1px solid #DDD;\"><b>{ApplicationName}</b></td>");
                builder.AppendLine($"<td style=\"padding: 8px; font-size: 14px; color: #6d6d6d; border-bottom: 1px solid #DDD;\"><b>{CreatedCount}</b></td>");
                builder.AppendLine($"<td style=\"padding: 8px; font-size: 14px; color: #6d6d6d; border-bottom: 1px solid #DDD;\"><b>{UpdatedCount}</b></td>");
                builder.AppendLine($"<td style=\"padding: 8px; font-size: 14px; color: #6d6d6d; border-bottom: 1px solid #DDD;\"><b>{PublishedCount}</b></td>");
                builder.AppendLine("</tr>");
                return builder.ToString();
            }
        }

        private class GroupedTranslationByApp
        {
            internal string LanguageName { get; set; }
            internal string ApplicationName { get; set; }
            internal List<TextTranslation> textTranslations { get; set; }
        }
        private class AppTranslationMapping
        {
            internal int ApplicationId { get; set; }
            internal string ApplicationName { get; set; }
            internal List<TextTranslation> Translations { get; set; }
        }
        private class AppPdfTranslationMapping
        {
            internal int ApplicationId { get; set; }
            internal string ApplicationName { get; set; }
            internal List<PdfTranslation> Translations { get; set; }
        }
        private class EnvironmentConfigSettings
        {
            public bool azurelocal { get; set; }

            public bool azuredev { get; set; }

            public bool azureprod { get; set; }
        }
    }
}
