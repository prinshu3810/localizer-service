This console application is a web job that is supposedly a notification job for translators and developers informing them about the changes being made by developers and translators respectively.

IMPORTANT: 

1. Kindly ensure the appsettings.json file matches with that present in  ../GrapeSEED.LocalizationService.

2. Use of environmentConfig.json: 

    a) This is the configuration as to which environment's data is to be picked up for the job. Ideally, only one environment should be true. Ensure the same. Although, the same job could be run for multiple environemnts if possibility arises in the future.

    b) Right now if more than one environments are true, then it will just override them assuming priority in descending order in which they are mentioned i.e. local '<' dev '<' prod. So if both dev and prod are true, environment setting of prod will override that of dev.

3. Settings.job:

    Example Cron job settings:

    Runs every minute
     "schedule": "0 * * * * *"

    Runs every 15 minutes
    "schedule": "0 */15 * * * *"

    Runs every hour (i.e. whenever the count of minutes is 0)
    "schedule": "0 0 * * * *"

    Runs every hour from 9 AM to 5 PM
    "schedule": "0 0 9-17 * * *"

    Runs at 9:30 AM every day
    "schedule": "0 30 9 * * *"

    Runs at 9:30 AM every week day
    "schedule": "0 30 9 * * 1-5"

	Current Cron Setting (see Settings.job)
	Run At 3:00 AM(UTC), only on Friday, Saturday, and Sunday
	"schedule": "0 0 3 * * FRI,SAT,SUN"

    Check cron expressions at https://cronexpressiondescriptor.azurewebsites.net/