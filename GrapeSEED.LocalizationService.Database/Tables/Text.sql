﻿CREATE TABLE [dbo].[Text](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[SectionId] [int] NOT NULL,
	[SNo] [int] NOT NULL,
	[GroupId] [int] NULL,
	[VersionId] [int] NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Text] ADD  DEFAULT ((1)) FOR [SNo]
GO

ALTER TABLE [dbo].[Text]  WITH CHECK ADD  CONSTRAINT [FK_Section_Text] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([Id])
GO

ALTER TABLE [dbo].[Text] CHECK CONSTRAINT [FK_Section_Text]
GO

ALTER TABLE [dbo].[Text]  WITH CHECK ADD  CONSTRAINT [FK_TextGroup] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO

ALTER TABLE [dbo].[Text] CHECK CONSTRAINT [FK_TextGroup]
GO

ALTER TABLE [dbo].[Text] CHECK CONSTRAINT [FK_Section_Text]
GO

ALTER TABLE [dbo].[Text]  WITH CHECK ADD  CONSTRAINT [FK_Text_Version] FOREIGN KEY([VersionId])
REFERENCES [dbo].[Version] ([Id])
GO

ALTER TABLE [dbo].[Text] CHECK CONSTRAINT [FK_Text_Version]
GO

