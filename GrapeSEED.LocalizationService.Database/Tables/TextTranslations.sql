﻿CREATE TABLE [dbo].[TextTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TextId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Translation] [nvarchar](max) NOT NULL,
	[PreviousTranslation] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[IsVisible] [bit] NOT NULL,
 CONSTRAINT [PK_Translations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uq_yourtablename] UNIQUE NONCLUSTERED 
(
	[TextId] ASC,
	[LanguageId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TextTranslation] ADD  DEFAULT ('') FOR [PreviousTranslation]
GO

ALTER TABLE [dbo].[TextTranslation] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[TextTranslation] ADD  DEFAULT ((1)) FOR [IsVisible]
GO

ALTER TABLE [dbo].[TextTranslation]  WITH CHECK ADD  CONSTRAINT [FK_Language] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[TextTranslation] CHECK CONSTRAINT [FK_Language]
GO

ALTER TABLE [dbo].[TextTranslation]  WITH CHECK ADD  CONSTRAINT [FK_Text_Translations] FOREIGN KEY([TextId])
REFERENCES [dbo].[Text] ([Id])
GO

ALTER TABLE [dbo].[TextTranslation] CHECK CONSTRAINT [FK_Text_Translations]
GO

ALTER TABLE [dbo].[TextTranslation]  WITH CHECK ADD  CONSTRAINT [FK_Translations_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO

ALTER TABLE [dbo].[TextTranslation] CHECK CONSTRAINT [FK_Translations_Status]
GO


