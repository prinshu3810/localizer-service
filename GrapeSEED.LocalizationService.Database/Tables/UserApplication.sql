﻿CREATE TABLE [dbo].[UserApplication](
	[UserId] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
 CONSTRAINT [PK_UserApplication] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ApplicationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserApplication]  WITH CHECK ADD  CONSTRAINT [FK_Application_UserApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO

ALTER TABLE [dbo].[UserApplication] CHECK CONSTRAINT [FK_Application_UserApplication]
GO

ALTER TABLE [dbo].[UserApplication]  WITH CHECK ADD  CONSTRAINT [FK_User_UserApplication] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[UserApplication] CHECK CONSTRAINT [FK_User_UserApplication]
GO
