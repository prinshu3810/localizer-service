﻿CREATE TABLE [dbo].[Group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[Sno] [int] NOT NULL,
	[Note] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Group] ADD  DEFAULT ((1)) FOR [IsVisible]
GO

ALTER TABLE [dbo].[Group] ADD  CONSTRAINT [Df_Sno]  DEFAULT ((1)) FOR [Sno]
GO

ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationGroup] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO

ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_ApplicationGroup]
GO