﻿CREATE TABLE [dbo].[LanguageGroupIgnore](
	[LanguageId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_LanguageGroupIgnore] PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LanguageGroupIgnore]  WITH CHECK ADD  CONSTRAINT [FK_LanguageGroupIgnore_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO

ALTER TABLE [dbo].[LanguageGroupIgnore] CHECK CONSTRAINT [FK_LanguageGroupIgnore_Group]
GO

ALTER TABLE [dbo].[LanguageGroupIgnore]  WITH CHECK ADD  CONSTRAINT [FK_LanguageGroupIgnore_Language] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[LanguageGroupIgnore] CHECK CONSTRAINT [FK_LanguageGroupIgnore_Language]
GO