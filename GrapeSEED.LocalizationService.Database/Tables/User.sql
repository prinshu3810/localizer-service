﻿CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsPasswordVerified] [bit] NOT NULL,
	[NotificationsEnabled] [bit] NOT NULL,
	[Disabled] [bit] NOT NULL,
	[InvitationCode] [varchar](26) NULL,
	[InvitationStatus] [bit] NOT NULL,
	[Name] [varchar](200) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_User_Username] UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[User] ADD  DEFAULT ((1)) FOR [IsPasswordVerified]
GO

ALTER TABLE [dbo].[User] ADD  DEFAULT ((1)) FOR [NotificationsEnabled]
GO

ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [Disabled]
GO

ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [InvitationStatus]
GO

