﻿CREATE TABLE [dbo].[PdfTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PdfId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Path] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[FileName] [nvarchar](300) NULL,
	[IsVisible] [bit] NOT NULL,
 CONSTRAINT [PK_PdfTranslations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PdfTranslation] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[PdfTranslation] ADD  DEFAULT ((1)) FOR [IsVisible]
GO

ALTER TABLE [dbo].[PdfTranslation]  WITH CHECK ADD  CONSTRAINT [FK_Language_PdfTranslations] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[PdfTranslation] CHECK CONSTRAINT [FK_Language_PdfTranslations]
GO

ALTER TABLE [dbo].[PdfTranslation]  WITH CHECK ADD  CONSTRAINT [FK_Pdf_PdfTranslations] FOREIGN KEY([PdfId])
REFERENCES [dbo].[Pdf] ([Id])
GO

ALTER TABLE [dbo].[PdfTranslation] CHECK CONSTRAINT [FK_Pdf_PdfTranslations]
GO

ALTER TABLE [dbo].[PdfTranslation]  WITH CHECK ADD  CONSTRAINT [FK_PdfTranslations_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO

ALTER TABLE [dbo].[PdfTranslation] CHECK CONSTRAINT [FK_PdfTranslations_Status]
GO