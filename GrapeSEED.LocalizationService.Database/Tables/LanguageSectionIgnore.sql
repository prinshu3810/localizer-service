﻿CREATE TABLE [dbo].[LanguageSectionIgnore](
	[LanguageId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
 CONSTRAINT [PK_LanguageSectionIgnore] PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC,
	[SectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LanguageSectionIgnore]  WITH CHECK ADD  CONSTRAINT [FK_Application_LanguageSectionIgnore] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([Id])
GO

ALTER TABLE [dbo].[LanguageSectionIgnore] CHECK CONSTRAINT [FK_Application_LanguageSectionIgnore]
GO

ALTER TABLE [dbo].[LanguageSectionIgnore]  WITH CHECK ADD  CONSTRAINT [FK_Language_LanguageSectionIgnore] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[LanguageSectionIgnore] CHECK CONSTRAINT [FK_Language_LanguageSectionIgnore]
GO