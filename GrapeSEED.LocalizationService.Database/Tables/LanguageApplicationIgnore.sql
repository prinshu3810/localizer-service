﻿CREATE TABLE [dbo].[LanguageApplicationIgnore](
	[LanguageId] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
 CONSTRAINT [PK_LanguageApplicationIgnore] PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LanguageApplicationIgnore]  WITH CHECK ADD  CONSTRAINT [FK_Application_LanguageApplicationIgnore] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO

ALTER TABLE [dbo].[LanguageApplicationIgnore] CHECK CONSTRAINT [FK_Application_LanguageApplicationIgnore]
GO

ALTER TABLE [dbo].[LanguageApplicationIgnore]  WITH CHECK ADD  CONSTRAINT [FK_Language_LanguageApplicationIgnore] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[LanguageApplicationIgnore] CHECK CONSTRAINT [FK_Language_LanguageApplicationIgnore]
GO
