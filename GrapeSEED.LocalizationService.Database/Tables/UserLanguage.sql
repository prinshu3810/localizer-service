﻿CREATE TABLE [dbo].[UserLanguage](
	[UserId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_UserLanguage] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LanguageId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserLanguage]  WITH CHECK ADD  CONSTRAINT [FK_LangUSer] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[UserLanguage] CHECK CONSTRAINT [FK_LangUSer]
GO

ALTER TABLE [dbo].[UserLanguage]  WITH CHECK ADD  CONSTRAINT [FK_UserLang] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO

ALTER TABLE [dbo].[UserLanguage] CHECK CONSTRAINT [FK_UserLang]
GO

