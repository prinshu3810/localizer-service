﻿CREATE TABLE [dbo].[Section](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Name] [nchar](50) NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Sections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Section] ADD  DEFAULT ('') FOR [DisplayName]
GO

ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationSections] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO

ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_ApplicationSections]
GO