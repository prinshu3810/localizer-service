﻿CREATE TABLE [dbo].[Application](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](50) NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[TranslationTypeId] [int] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[AllowPublish] [bit] NOT NULL,
 CONSTRAINT [PK_Website] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Application] ADD  DEFAULT ((0)) FOR [IsLocked]
GO

ALTER TABLE [dbo].[Application] ADD  DEFAULT ((0)) FOR [AllowPublish]
GO

ALTER TABLE [dbo].[Application]  WITH CHECK ADD  CONSTRAINT [FK_TranslationTypeApplication] FOREIGN KEY([TranslationTypeId])
REFERENCES [dbo].[TranslationType] ([Id])
GO

ALTER TABLE [dbo].[Application] CHECK CONSTRAINT [FK_TranslationTypeApplication]
GO

