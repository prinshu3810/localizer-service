﻿INSERT INTO [dbo].[User] VALUES 
('shobhon.chatterjee@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --1 grapeseed@123
('vinay.kumar@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --2
('anurag.chopra@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --3
('sajid.ali@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --4
('sagar.saini@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --5
('saurabh.pati@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1), --6
('kanika.kumar@grapecity.com', 'NkqUO/ewkOIuqvPxhqsqnw==',1) --7

INSERT INTO [dbo].[Role] VALUES
('Developer'), --1
('Translator'), --2
('Admin') --3

INSERT INTO [dbo].[Status] VALUES
('New'),
('Updated'),
('Published')

INSERT INTO [dbo].[Language] VALUES
('English', 1, 'en'), --1 
('Spanish', 1, 'es'), --2
('Japanese', 1, 'ja'), --3
('Chinese', 1, 'zh'), --4
('Mongolian', 1, 'mn'), --5
('Malaysian', 1, 'ms'), --6
('Russian', 1, 'ru'), --7
('Arabic', 1, 'ar'), --8
('Vietnamese', 1, 'vi'), -- 9
('Korean', 1, 'ko') -- 10

INSERT INTO [dbo].[TranslationType] VALUES
('Text'),
('PDF')

INSERT INTO [dbo].[UserRole] VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 2)

INSERT INTO [dbo].[UserLanguage] VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),

(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),

(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10),

(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),

(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(5, 10),

(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),

(7, 1),
(7, 2),
(7, 3)

INSERT INTO [dbo].[NotificationStatus] VALUES ('Success'), ('Failure')