﻿CREATE PROCEDURE [dbo].[Sp_NotificationCounter]  @applicationId int = NULL, @languageId int, @groupView bit = NULL, @groupId int = NULL , @sectionId int = NULL, @role int, @userId int = NULL
AS
Declare @start int, @end int--, @languageId int, @applicationId int, @sectionId int , @role int, @userId int
--SET @languageId = 0
--SET @applicationId = NULL
--SET @sectionId = NULL
--SET @UserId = 7
--SET @role = 2
-- Find all language Id's (1-13)
IF @languageId = 0
BEGIN 
	SET @start = (Select min(id) from Language)
	SET @end = (Select max(id) from Language)
END;
ELSE
BEGIN

	SET @start = @languageId
	SET @end = @languageId
END;

-- check if application id is 0
IF @applicationId = 0
BEGIN
	SET @applicationId = NULL;
END;

-- check if notification is for group view and create table to store results for groups
IF (@groupView = 'true')
BEGIN
	Create table #TempResultGroup (APplicationId int, GroupId int, LanguageId int, NotificationCount int);

	-- Join the neccessary tables for Text
	SELECT  g.ApplicationId, g.Id as GroupId, tt.LanguageId, tt.Id as ttId, tt.StatusId, tt.IsVisible as ttIsVisible INTO #TextTransTempGroup FROM Text as t
		JOIN TextTranslation as tt
		ON t.Id = tt.TextId
		JOIN [Group] as g
		ON t.GroupId = g.Id
		JOIN [Application] a
		ON g.ApplicationId = a.Id
		WHERE g.IsVisible = 1 AND a.IsVisible = 1;

	-- Join the necessary tables for PDF

	SELECT  g.ApplicationId, g.Id as GroupId, tt.Id as ttId, tt.LanguageId, tt.StatusId, tt.PdfId, tt.IsVisible as ttIsVisible INTO #PdfTransTempGroup 
		FROM [PdfTranslation] tt
		JOIN
		[Pdf] as pdf ON pdf.Id = tt.PdfId
		JOIN
		[Group] as g ON g.Id = pdf.GroupId
		JOIN [Application] a
		ON g.ApplicationId = a.Id
		WHERE g.IsVisible = 1 AND a.IsVisible = 1;


	WHILE @start <= @end
		BEGIN
			SET @languageId = @start;
			-- check if user have access to language
			if((SELECT count(*) as c From UserLanguage Where LanguageId = @start And UserId = @userId) = 0)
				BEGIN
					SET @start = @start + 1;
					continue;
		END;	
	
		-- Notifications for languages other than english.
		If @languageId <> NULL OR @languageId <> 1
		BEGIN
			INSERT INTO #TempResultGroup
			SELECT * From
			(-- Translations which are in english but not in other languages.
				(SELECT 
					l.ApplicationId, 
					l.GroupId, 
					@languageId as LanguageId,
					SUM(l.EnglishCount) - ISNULL(SUM(l.LanguageCount), 0 ) as NotificationCount -- NotTranslated
					FROM
				( -- Find the total english translations
					SELECT ApplicationId, GroupId,  count(ttId) EnglishCount, NULL as LanguageCount
					FROM #TextTransTempGroup
					WHERE (GroupId = @groupId OR @groupId IS NULL)
							And (ApplicationId = @applicationId OR @applicationId IS NULL)
							And LanguageId = 1
					GROUP BY ApplicationId, GroupId

				UNION
				-- Find the total translations that are translated.
					SELECT  ApplicationId, GroupId,  NULL as EnglishCount, count(ttId) LanguageCount
					FROM #TextTransTempGroup
					WHERE (GroupId = @groupId OR @groupId IS NULL)
							And (ApplicationId = @applicationId OR @applicationId IS NULL)
							And LanguageId = @languageId
					GROUP BY ApplicationId, GroupId) as l
					GROUP BY l.ApplicationId, l.GroupId
				)

			UNION
			-- PDF Translations
				SELECT 
					l.ApplicationId, 
					l.GroupID, 
					@languageId as LanguageId,
					SUM(l.EnglishCount) - ISNULL(SUM(l.LanguageCount), 0 ) as NotificationCount -- NotTranslated
					FROM
					( -- Find the total english translations
					SELECT  r.ApplicationId, r.GroupId,  r.Updated as EnglishCount, NULL as LanguageCount FROM 
						(
							SELECT x.ApplicationId, x.GroupId, x.LanguageId, count(x.Updated) as Updated FROM 
							(
								SELECT ApplicationId, GroupId, LanguageId, COUNT(ttId) as Updated, PdfId
								FROM #PdfTransTempGroup
								WHERE LanguageId = 1
											And (GroupId = @groupId OR @groupId IS NULL)
											And (ApplicationId = @applicationId OR @applicationId IS NULL)
								GROUP BY ApplicationId, GroupId, LanguageId, PdfId
							)as x
							GROUP BY x.ApplicationId, x.GroupId, x.LanguageId
						) as r

					UNION
					-- Find the total translations that are translated.
						Select ApplicationId, GroupId, EnglishCount, count(LanguageCount) From
						(
							SELECT ApplicationId, GroupId,  NULL as EnglishCount, count(ttId) LanguageCount
							FROM #PdfTransTempGroup
							WHERE (GroupId = @groupId OR @groupId IS NULL)
									And (ApplicationId = @applicationId OR @applicationId IS NULL)
									And LanguageId = @languageId
							GROUP BY ApplicationId, GroupId, pdfId
						) as l
						GROUP BY l.ApplicationId, l.GroupId, EnglishCount
					) as l
					GROUP BY l.ApplicationId, l.GroupId
		
			) as i
		END;
		ELSE -- Notification for english
			-- Notifications for text
		BEGIN
			INSERT INTO #TempResultGroup
			SELECT * FROM
			(SELECT  ApplicationId, GroupId, 1 as LanguageId, count(LanguageId) as NotificationCount  
			FROM #TextTransTempGroup
			WHERE ttIsVisible = 1 And
					  LanguageId = 1
					And (GroupId = @groupId OR @groupId IS NULL)
					And (ApplicationId = @applicationId OR @applicationId IS NULL)
					And StatusId <> 3
			GROUP BY ApplicationId, GroupId, LanguageId

			UNION
			-- Notifications for pdf
				SELECT x.ApplicationId, x.GroupId, 1 as LanguageId, count(x.Updated) as NotificationCount FROM 
			(
			SELECT ApplicationId, GroupId,  COUNT(ttId) as Updated, PdfId
			FROM #PdfTransTempGroup
			WHERE ttIsVisible=1 And
						 StatusId <> 3
						And LanguageId = 1
						And (GroupId = @groupId OR @groupId IS NULL)
						And (ApplicationId = @applicationId OR @applicationId IS NULL)
			GROUP BY ApplicationId, GroupId,  PdfId
			) as x
			GROUP BY x.ApplicationId, x.GroupId) bb
		END;
		SET @start = @start + 1	
	END; --while end
	BEGIN
	SELECT * FROM #TempResultGroup Order by ApplicationId, GroupId, LanguageId
	DROP TABLE #TempResultGroup
	END;
END;
ELSE IF(@groupView = 'false' OR @groupView IS NULL)
BEGIN
-- create a temp table to store the results
Create table #TempResult (ApplicationId int ,SectionId int, LanguageId int, NotificationCount int);

	-- Join the neccessary tables for Text
	SELECT  s.ApplicationId, s.Id as SectionId, tt.LanguageId, tt.Id as ttId, tt.StatusId, tt.IsVisible as ttIsVisible INTO #TextTransTemp FROM Text as t
		JOIN TextTranslation as tt
		ON t.Id = tt.TextId
		JOIN Section as s
		ON t.SectionId = s.Id
		JOIN [Application] a
		ON s.ApplicationId = a.Id
		WHERE s.IsVisible = 1 AND a.IsVisible = 1;

	-- Join the necessary tables for PDF

	SELECT  s.ApplicationId, s.Id as SectionId, tt.Id as ttId, tt.LanguageId, tt.StatusId, tt.PdfId, tt.IsVisible as ttIsVisible INTO #PdfTransTemp 
		FROM [PdfTranslation] tt
		JOIN
		[Pdf] as pdf ON pdf.Id = tt.PdfId
		JOIN
		[Section] as s ON s.Id = pdf.SectionId
		JOIN [Application] a
		ON s.ApplicationId = a.Id
		WHERE s.IsVisible = 1 AND a.IsVisible = 1;

	While @start <= @end
	BEGIN
		SET @languageId = @start;
	IF @role = 1
		-- Text translations
		INSERT INTO #TempResult SELECT * FROM
		(SELECT  ApplicationId, SectionId, LanguageId, count(LanguageId) as NotificationCount  FROM #TextTransTemp
		WHERE ttIsVisible = 1 And
				 StatusId = 2 
				And languageId = @languageId
				And (SectionId = @sectionId OR @sectionId IS NULL)
				And (ApplicationId = @applicationId OR @applicationId IS NULL)
		GROUP BY ApplicationId, SectionId, LanguageId

		UNION
		-- File (PDF) translations
		SELECT x.ApplicationId, x.SectionId, x.LanguageId, count(x.Updated) as NotificationCount FROM 
		(SELECT ApplicationId, sectionId, LanguageId, COUNT(ttId) as Updated, PdfId
		FROM #PdfTransTemp
		WHERE  ttIsVisible=1 And
					 StatusId = 2 
					And LanguageId = @languageId
					And (SectionId = @sectionId OR @sectionId IS NULL)
					And (ApplicationId = @applicationId OR @applicationId IS NULL)
		GROUP BY ApplicationId, SectionId, LanguageId, PdfId)
		as x
		GROUP BY x.ApplicationId, x.sectionId, x.LanguageId) b

	ELSE IF @role = 2
	BEGIN
		-- check if user have access to language
		if((SELECT count(*) as c From UserLanguage Where LanguageId = @start And UserId = @userId) = 0)
			BEGIN
				SET @start = @start + 1;
				continue;
			END;	
	
		-- Notifications for languages other than english.
		If @languageId <> NULL OR @languageId <> 1
		BEGIN
			INSERT INTO #TempResult
			SELECT * From
			(-- Translations which are in english but not in other languages.
				(SELECT 
					l.ApplicationId, 
					l.SectionId, 
					@languageId as LanguageId,
					SUM(l.EnglishCount) - ISNULL(SUM(l.LanguageCount), 0 ) as NotificationCount -- NotTranslated
					FROM
				( -- Find the total english translations
					SELECT ApplicationId, SectionId,  count(ttId) EnglishCount, NULL as LanguageCount
					FROM #TextTransTemp
					WHERE (SectionId = @sectionId OR @sectionId IS NULL)
							And (ApplicationId = @applicationId OR @applicationId IS NULL)
							And LanguageId = 1
					GROUP BY ApplicationId, SectionId

				UNION
				-- Find the total translations that are translated.
					SELECT  ApplicationId, SectionId,  NULL as EnglishCount, count(ttId) LanguageCount
					FROM #TextTransTemp
					WHERE (SectionId = @sectionId OR @sectionId IS NULL)
							And (ApplicationId = @applicationId OR @applicationId IS NULL)
							And LanguageId = @languageId
					GROUP BY ApplicationId, SectionId) as l
					GROUP BY l.ApplicationId, l.SectionId
				)

			UNION
			-- PDF Translations
				SELECT 
					l.ApplicationId, 
					l.SectionId, 
					@languageId as LanguageId,
					SUM(l.EnglishCount) - ISNULL(SUM(l.LanguageCount), 0 ) as NotificationCount -- NotTranslated
					FROM
					( -- Find the total english translations
					SELECT  r.ApplicationId, r.SectionId,  r.Updated as EnglishCount, NULL as LanguageCount FROM 
						(
							SELECT x.ApplicationId, x.SectionId, x.LanguageId, count(x.Updated) as Updated FROM 
							(
								SELECT ApplicationId, sectionId, LanguageId, COUNT(ttId) as Updated, PdfId
								FROM #PdfTransTemp
								WHERE LanguageId = 1
											And (SectionId = @sectionId OR @sectionId IS NULL)
											And (ApplicationId = @applicationId OR @applicationId IS NULL)
								GROUP BY ApplicationId, SectionId, LanguageId, PdfId
							)as x
							GROUP BY x.ApplicationId, x.sectionId, x.LanguageId
						) as r

					UNION
					-- Find the total translations that are translated.
						Select ApplicationId, SectionId, EnglishCount, count(LanguageCount) From
						(
							SELECT ApplicationId, SectionId,  NULL as EnglishCount, count(ttId) LanguageCount
							FROM #PdfTransTemp
							WHERE (SectionId = @sectionId OR @sectionId IS NULL)
									And (ApplicationId = @applicationId OR @applicationId IS NULL)
									And LanguageId = @languageId
							GROUP BY ApplicationId, SectionId, pdfId
						) as l
						GROUP BY l.ApplicationId, l.SectionId, EnglishCount
					) as l
					GROUP BY l.ApplicationId, l.SectionId
		
			) as i
		END;
		ELSE -- Notification for english
			-- Notifications for text
		BEGIN
			INSERT INTO #TempResult
			SELECT * FROM
			(SELECT  ApplicationId, SectionId, 1 as LanguageId, count(LanguageId) as NotificationCount  
			FROM #TextTransTemp
			WHERE ttIsVisible = 1 And
					  LanguageId = 1
					And (SectionId = @sectionId OR @sectionId IS NULL)
					And (ApplicationId = @applicationId OR @applicationId IS NULL)
					And StatusId <> 3
			GROUP BY ApplicationId, SectionId, LanguageId

			UNION
			-- Notifications for pdf
				SELECT x.ApplicationId, x.SectionId, 1 as LanguageId, count(x.Updated) as NotificationCount FROM 
			(
			SELECT ApplicationId, SectionId,  COUNT(ttId) as Updated, PdfId
			FROM #PdfTransTemp
			WHERE ttIsVisible=1 And
						 StatusId <> 3
						And LanguageId = 1
						And (SectionId = @sectionId OR @sectionId IS NULL)
						And (ApplicationId = @applicationId OR @applicationId IS NULL)
			GROUP BY ApplicationId, SectionId,  PdfId
			) as x
			GROUP BY x.ApplicationId, x.sectionId) bb
		END;
	END;
	SET @start = @start + 1	
	END; -- While End
	BEGIN
	SELECT * FROM #TempResult Order by ApplicationId, SectionId, LanguageId
	DROP TABLE #TempResult
	END;
END;

GO
