﻿namespace GrapeSEED.LocalizationService.Foundation
{
    public class AuthMessageSenderOptions
    {
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
        public string LocalizerAppUrl { get; set; }
    }
}
