﻿using GrapeSEED.LocalizationService.Foundation.Common;
using SendGrid;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Foundation
{
    public interface IEmailSender
    {
        Task<Response> SendEmailAsync(string email, string subject, string message);

        Task<Response> SendEmailAsync(string from, string sender, string email, string subject, string message);

        Task<Response> SendEmailsAsync(NotificationModel model);

        Task<Response> SendEmailAsync(string to, string subject, string templatePath, string[] args);
    }
}
