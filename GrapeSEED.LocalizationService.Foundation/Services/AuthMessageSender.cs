﻿using GrapeSEED.LocalizationService.Foundation.Common;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Foundation
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender
    {
        public AuthMessageSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        /// <summary>
        /// Sends email based on template.
        /// </summary>
        /// <param name="to">Email of reveiver</param>
        /// <param name="subject">Subject of the email</param>
        /// <param name="templatePath">Template for the email</param>
        /// <param name="args">Arguments to be appended to the email template</param>
        /// <returns></returns>
        public async Task<Response> SendEmailAsync(string to, string subject, string templatePath, string[] args)
        {
            string mailText = string.Empty;

            using (StreamReader sr = new StreamReader($"Templates/{templatePath}"))
            {
                mailText = sr.ReadToEnd();

                var realMailText = string.Format(mailText, args);

                return await Execute(Options.SendGridKey, subject, realMailText, to, null, null);
            }
        }

        public Task<Response> SendEmailAsync(string from, string sender, string email, string subject, string message)
        {
            return Execute(Options.SendGridKey, subject, message, email, from, sender);
        }

        public Task<Response> SendEmailAsync(string email, string subject, string message)
        {
            return Execute(Options.SendGridKey, subject, message, email);
        }

        public Task<Response> SendEmailsAsync(NotificationModel model)
        {
            return ExecuteMultipleAsync(Options.SendGridKey, model);
        }

        public Task<Response> Execute(string apiKey, string subject, string message, string email, string from = null, string sender = null)
        {
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(from ?? "no-reply@grapeseed.com", sender ?? "GrapeSEED"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            return client.SendEmailAsync(msg);
        }

        public Task<Response> ExecuteMultipleAsync(string apiKey, NotificationModel model)
        {
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(model.FromAddress ?? "no-reply@grapeseed.com", "GrapeSEED"),
                Subject = model.Subject,
                PlainTextContent = model.Body,
                HtmlContent = model.Body,
            };

            if (model.ToAddresses != null && model.ToAddresses.Any())
            {
                msg.AddTos(model.ToAddresses.Select(to => new EmailAddress(to)).ToList());
            }
            if (model.CcAddresses != null && model.CcAddresses.Any())
            {
                msg.AddCcs(model.CcAddresses.Select(cc => new EmailAddress(cc)).ToList());
            }
            if (model.BccAddresses != null && model.BccAddresses.Any())
            {
                msg.AddBccs(model.BccAddresses.Select(bcc => new EmailAddress(bcc)).ToList());
            }
            return client.SendEmailAsync(msg);
        }
    }
}
