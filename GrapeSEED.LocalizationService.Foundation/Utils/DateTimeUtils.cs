﻿using System;

namespace GrapeSEED.LocalizationService.Foundation.Utils
{
    public static class DateTimeUtils
    {
        public static DateTime UtcDateTime
        {
            get
            {
                return DateTime.UtcNow;
            }
        }

        public static DateTime UtcDate
        {
            get
            {
                return DateTime.UtcNow.Date;
            }
        }
    }
}
