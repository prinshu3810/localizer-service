﻿namespace GrapeSEED.LocalizationService.Foundation.Common
{
    public static class Enums
    {
        public enum Status
        {
            New = 1,
            Updated = 2,
            Published = 3
        }

        public enum Role
        {
            Developer = 1,
            Translator = 2,
            Admin = 3
        }

        public enum ForgotPasswordStatus
        {
            Success = 1,
            EmailNotExist = -1,
            DisabledAccount = 2
        }
        public enum UpdatePasswordStatus
        {
            Success = 1,
            CurrentPasswordWrong = -1,
            InvalidParameters = -2
        }

        public enum SearchOption
        {
            Selected_Page = 1,
            Selected_App = 2,
            All_Apps = 3
        }
        public enum StatusCodes
        {
            #region Exception 

            NoPermission = 500403,
            EntityDoesNotExist = 500404,
            Unknown = 500505,
            ArgumentNull = 500506,
            ArgumentInvalid = 500507,
            AzureStorage = 500508,
            DatabaseOp = 500509,
            NotificationUnsuccessfull = 500510,
            ApplicationIsLocked = 500515,

            #endregion

            #region Success 

            ApplicationLocked = 200214,
            ApplicationUnlocked = 200215,
            SuccessfullyNotified = 200216,

            #endregion

            #region Deferred

            NotificationDeferred = 600600

            #endregion
        }

        public enum TranslationTypeId
        {
            Text = 1,
            Pdf = 2
        }

        public enum PublishToEnv
        {
            Dev = 1,
            Test = 2,
            Prod = 3
        }
    }
}
