﻿namespace GrapeSEED.LocalizationService.Foundation.Common
{
    public class Constants
    {
        public const string PDF_TRNSLATION_FOLDER_NAME = "translated_pdfs";
        public const string UPLOADED_IMAGES = "images_per_group";
        public const string ROLE_NAMESPACE = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        public const string USERID_NAMESPACE = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        public const string FORGOT_PASSWORD_TEMPLATE = "Your password has been reset and your new password to access Localizer is : {OTP}";
        public const string FORGOT_PASSWORD_EMAIL_SUBJECT = "Localizer | Password Reset";
        public const string INVITE_USER_SUBJECT = "Invitation to use Localizer";
        public const string LocalizationFiles = "LocalizationFiles";
        public const string ProdLocalizationFiles = "ProdLocalizationFiles";
    }

    public class ApplicationError
    {
        public const string FETCH_DATA = "Something went wrong while getting details";
        public const string FAILED_EMAIL_SEND = "Failed to send email";
    }
}
