﻿namespace GrapeSEED.LocalizationService.Azure.Service
{
    public class StorageSettingOptions
    {
        public string AccountName { get; set; }

        public string AccountKey { get; set; }

        public string ContainerName { get; set; }

        public bool IsSSLEnabled { get; set; }
    }
}
