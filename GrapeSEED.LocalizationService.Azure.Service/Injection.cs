﻿using GrapeSEED.LocalizationService.Azure.Service.Concretes;
using GrapeSEED.LocalizationService.Azure.Service.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace GrapeSEED.LocalizationService.Azure.Service
{
    public static class AzureServicesInjection
    {
        public static IServiceCollection RegisterAzureServices(this IServiceCollection services)
        {
            return services.AddScoped(typeof(IAzureStorageRepository<>), typeof(AzureStorageRepository<>));
        }
    }
}
