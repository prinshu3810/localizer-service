﻿using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace GrapeSEED.LocalizationService.Azure.Service.Contracts
{
    public interface IAzureStorageRepository<TEntity> where TEntity : TableEntity
    {
        Task InsertAsync(TEntity entity);
    }
}
