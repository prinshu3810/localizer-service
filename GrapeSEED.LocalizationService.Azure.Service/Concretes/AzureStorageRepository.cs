﻿using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Azure.Service.Contracts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace GrapeSEED.LocalizationService.Azure.Service.Concretes
{
    public class AzureStorageRepository<TEntity> : IAzureStorageRepository<TEntity> where TEntity : TableEntity
    {
        private readonly CloudStorageAccount _account;
        private readonly StorageSettingOptions _options;
        private readonly ILogger<AzureStorageRepository<TEntity>> _logger;

        public AzureStorageRepository(IOptions<StorageSettingOptions> options,
            ILogger<AzureStorageRepository<TEntity>> logger)
        {
            _options = options.Value;
            _logger = logger;
            if (!string.IsNullOrWhiteSpace(_options.AccountName)
                && !string.IsNullOrWhiteSpace(_options.AccountKey))
            {
                StorageCredentials credential = new StorageCredentials(_options.AccountName, _options.AccountKey);
                _account = new CloudStorageAccount(credential, false);
            }
        }

        public async Task InsertAsync(TEntity entity)
        {
            if (_account == null)
            {
                return;
            }

            CloudTableClient tableClient = _account.CreateCloudTableClient();

            CloudTable errorLogTable = tableClient.GetTableReference(entity.GetType().Name);

            await errorLogTable.CreateIfNotExistsAsync();

            TableOperation insertOperation = TableOperation.Insert(entity);

            await errorLogTable.ExecuteAsync(insertOperation);
        }
    }
}
