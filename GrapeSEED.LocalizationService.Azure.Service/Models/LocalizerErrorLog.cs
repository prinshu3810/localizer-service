﻿using Microsoft.WindowsAzure.Storage.Table;

namespace GrapeSEED.LocalizationService.Azure.Service.Models
{
    public class LocalizerErrorLog : TableEntity
    {
        public LocalizerErrorLog(string source, string timestamp)
        {
            RowKey = timestamp;
            Source = PartitionKey = source;
        }

        public string Source { get; set; }

        public string RequestUrl { get; set; }

        public string HttpMethod { get; set; }

        public string QueryString { get; set; }

        public string Form { get; set; }

        public string Code { get; set; }

        public string Exception { get; set; }

        public string InnerException { get; set; }
    }
}
