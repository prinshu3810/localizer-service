﻿using System;
using System.Collections.Generic;
using System.Text;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Shared
{
    public abstract class BusinessException<TCode> : BaseException where TCode : IEquatable<TCode>
    {
        public TCode Code { get; set; }

        public BusinessException()
        {
        }

        public BusinessException(string message) : base(message)
        {
        }

        public BusinessException(string message, TCode code) : base(message)
        {
            Code = code;
        }

        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BusinessException(string message, Exception innerException, TCode code) : this(message, innerException)
        {
            Code = code;
        }
    }

    public class ArgumentIsInvalidException : BusinessException<long>
    {
        public ArgumentIsInvalidException(string message) : base(message)
        {
            Code = (long)StatusCodes.ArgumentInvalid;
        }

        public ArgumentIsInvalidException(string message, Exception exception) : base(message, exception, (long)StatusCodes.ArgumentInvalid)
        {
        }
    }
    public class ArgumentIsNullException : BusinessException<long>
    {
        public ArgumentIsNullException(string message) : base(message)
        {
            Code = (long)StatusCodes.ArgumentNull;
        }

        public ArgumentIsNullException(string message, Exception exception) : base(message, exception, (long)StatusCodes.ArgumentNull)
        {
        }
    }

    public class NoPermissionException : BusinessException<long>
    {
        public NoPermissionException(string message) : base(message)
        {
            Code = (long)StatusCodes.NoPermission;
        }

        public NoPermissionException(string message, Exception exception) : base(message, exception, (long)StatusCodes.NoPermission)
        {
        }
    }

    public class AzureStorageException : BusinessException<long>
    {
        public AzureStorageException(string message): base(message)
        {
            Code = (long)StatusCodes.AzureStorage;
        }

        public AzureStorageException(string message, Exception exception) : base(message, exception, (long)StatusCodes.AzureStorage)
        {
        }
    }

    public class DatabaseOpException : BusinessException<long>
    {
        public DatabaseOpException(string message) : base(message)
        {
            Code = (long)StatusCodes.AzureStorage;
        }

        public DatabaseOpException(string message, Exception exception) : base(message, exception, (long)StatusCodes.DatabaseOp)
        {
        }
    }

    public class EntityDoesNotExistException : BusinessException<long>
    {
        public EntityDoesNotExistException(string message) : base(message)
        {
            Code = (long)StatusCodes.EntityDoesNotExist;
        }

        public EntityDoesNotExistException(string message, Exception exception) : base(message, exception, (long)StatusCodes.EntityDoesNotExist)
        {
                
        }
    }

    public class ApplicationIsLockedException : BusinessException<long>
    {
        public ApplicationIsLockedException(string message) : base(message)
        {
            Code = (long)StatusCodes.ApplicationIsLocked;
        }

        public ApplicationIsLockedException(string message, Exception exception) : base(message, exception, (long)StatusCodes.ApplicationIsLocked)
        {

        }
    }

    public class UnknownException : BusinessException<long>
    {
        public UnknownException(string message) : base(message)
        {
            Code = (long)StatusCodes.AzureStorage;
        }

        public UnknownException(string message, Exception exception) : base(message, exception, (long)StatusCodes.Unknown)
        {
        }
    }

    public class SendMailFailed : BusinessException<long>
    {
        public SendMailFailed(string message, Exception ex) : base(message, ex) { }
    }

}
