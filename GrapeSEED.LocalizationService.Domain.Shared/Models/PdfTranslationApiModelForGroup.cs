﻿using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class PdfTranslationApiModelForGroup
    {
        public int Id { get; set; }

        public int PdfId { get; set; }

        public int LanguageId { get; set; }

        public string Translation { get; set; }

        public string Path { get; set; }

        public int StatusId { get; set; }

        public bool IsVisible { get; set; } = true;

        public PdfTranslation ToEntity()
        {
            return new PdfTranslation()
            {
                Id = Id,
                PdfId = PdfId,
                Path = Path,
                LanguageId = LanguageId,
                FileName = Translation,
                StatusId = StatusId,
                IsVisible = IsVisible
            };
        }
    }
}
