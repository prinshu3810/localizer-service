﻿using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class TextApiModel
    {
        public TextApiModel()
        {
            TextTranslations = new List<TextTranslationApiModel>();
        }

        public int Id { get; set; }

        public int SectionId { get; set; }

        public int ReleaseId { get; set; }

        public string Key { get; set; }

        public int? GroupId { get; set; }

        public IEnumerable<TextTranslationApiModel> TextTranslations { get; set; }

        public Text ToEntity()
        {
            return new Text()
            {
                Id = Id,
                SectionId = SectionId,
                Key = Key,
                TextTranslation = TextTranslations
                                    .Select(translation => translation.ToEntity())
                                    .ToList()
            };
        }
    }
}