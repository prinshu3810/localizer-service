﻿namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class LanguageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public string LanguageCode { get; set; }
    }
}
