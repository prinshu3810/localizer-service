﻿namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class PdfTranslationApiModel
    {
        public int? PdfTranslationId { get; set; }

        public int LanguageId { get; set; }

        public string Language { get; set; }

        public string Key { get; set; }

        public int SectionId { get; set; }
        public int GroupId { get; set; }

        public int StatusId { get; set; }

        public string Path { get; set; }

        public int PdfId { get; set; }

        public bool IsAddMultiple { get; set; }

        public bool IsVisible { get; set; }
        public int ReleaseId { get; set; }
    }
}
