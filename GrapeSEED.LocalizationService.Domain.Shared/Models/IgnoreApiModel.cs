﻿using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class IgnoreApiModel
    {
        public int ApplicationId { get; set; }

        public bool IsVisible { get; set; }

        public int LanguageId { get; set; }

        public IEnumerable<SectionApiModel> Sections { get; set; }

        public IEnumerable<TextTranslationApiModel> TextTranslations { get; set; }

        public IEnumerable<PdfTranslationApiModel> PdfTranslations { get; set; }

        public IEnumerable<GroupApiModel> Groups { get; set; }
    }
}
