﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class GroupApiModel
    {
        public GroupApiModel()
        {
            Texts = new HashSet<TextApiModel>();
            LanguageGroupIgnores = new HashSet<LanguageGroupIgnore>();
        }

        public int Id { get; set; }

        public int ApplicationId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public bool IsVisible { get; set; } = true;

        public IEnumerable<TextApiModel> Texts { get; set; }

        public IEnumerable<LanguageGroupIgnore> LanguageGroupIgnores { get; set; }

        public Group ToEntity()
        {
            return new Group()
            {
                Id = Id,
                ApplicationId = ApplicationId,
                Name = Name,
                Note = Note,
                IsVisible = IsVisible,
                LanguageGroupIgnore = LanguageGroupIgnores.ToList(),
                Text = Texts
                        .Select(text => text.ToEntity())
                        .ToList()
            };
        }
    }
}
