﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class PdfApiModel
    {
        public PdfApiModel()
        {
            PdfTranslations = new List<PdfTranslationApiModelForGroup>();
        }
        public int Id { get; set; }
        public int SectionId { get; set; }
        public string Key { get; set; }
        public int? GroupId { get; set; }
        public IEnumerable<PdfTranslationApiModelForGroup> PdfTranslations { get; set; }

    }
}
