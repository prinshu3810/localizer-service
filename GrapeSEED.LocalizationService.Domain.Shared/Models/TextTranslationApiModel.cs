﻿using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class TextTranslationApiModel
    {
        public int Id { get; set; }

        public int TextId { get; set; }

        public int LanguageId { get; set; }

        public string Translation { get; set; }

        public int StatusId { get; set; }

        public bool IsVisible { get; set; } = true;

        public TextTranslation ToEntity()
        {
            return new TextTranslation()
            {
                Id = Id,
                TextId = TextId,
                LanguageId = LanguageId,
                Translation = Translation,
                StatusId = StatusId,
                IsVisible = IsVisible
            };
        }
    }
}