﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class NotificationResponse
    {
        public int ApplicationId { get; set; }
        public int SectionId { get; set; }
        public int LanguageId { get; set; }
        public int NotificationCount { get; set; }

    }
}
