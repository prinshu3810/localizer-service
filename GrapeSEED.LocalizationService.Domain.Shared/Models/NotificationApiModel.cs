﻿using System.Collections.Generic;
using System.Net.Mail;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class NotificationApiModel
    {
        #region Setting any of these props will override the Email Settings in config

        /// <summary>
        /// Gets or sets the host.
        /// <para>
        /// This will override the host in the environment config.
        /// </para>
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// <para>
        /// If set, this will override the port in the environment config.
        /// </para>
        /// </summary>
        public int? Port { get; set; }

        /// <summary>
        /// Gets or sets the sender's address.
        /// <para>
        /// This will override the sender's address in the environment config.
        /// </para>
        /// </summary>
        public string FromAddress { get; set; }

        /// <summary>
        /// Gets or sets the sender's password.
        /// <para>
        /// This will override the sender's password in the environment config.
        /// </para>
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable secure socket layer.
        /// <para>
        /// This will override the environment config if set.
        /// </para>
        /// </summary>
        public bool? EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the email will be in html format.
        /// <para>
        /// If set, email will be sent with html format.
        /// </para>
        /// </summary>
        public bool? IsHtml { get; set; }

        #endregion

        public IEnumerable<string> ToAddresses { get; set; }

        public IEnumerable<string> CcAddresses { get; set; }

        public IEnumerable<string> BccAddresses { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the event handler.
        /// <para>
        /// Implement this method if deferred result is expected. e.g. in case of async operation.
        /// </para>
        /// </summary>
        public SendCompletedEventHandler OperationEventHandler { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the operation has to be awaited.
        /// </summary>
        public bool AwaitOperation { get; set; }
    }
}
