﻿using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class ApplicationInfoApiModel
    {
        public ApplicationInfoApiModel()
        {
            SectionsToAdd = new HashSet<SectionApiModel>();
            SectionsToDelete = new HashSet<int>();
            Sections = new HashSet<SectionApiModel>();
        }

        public int ApplicationId { get; set; }

        public string Name { get; set; }

        public bool IsVisible { get; set; } = true;

        public int TranslationTypeId { get; set; }

        public long LockedCode { get; set; }

        public bool AllowPublish { get; set; }

        #region Edit Application

        public IEnumerable<SectionApiModel> SectionsToAdd { get; set; }

        public IEnumerable<SectionApiModel> SectionsToEdit { get; set; }

        public IEnumerable<SectionApiModel> SectionsToUpload { get; set; }

        public IEnumerable<int> SectionsToDelete { get; set; }

        public bool RemoveEnglishForOtherLang { get; set; }

        #endregion

        public IEnumerable<SectionApiModel> Sections { get; set; }
    }
}
