﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Disabled { get; set; }
        public IEnumerable<int> RoleIds { get; set; }
        public IEnumerable<int> LanguageIds { get; set; }
    }
}
