﻿namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
