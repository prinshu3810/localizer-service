﻿using System;
using System.Collections.Generic;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class NotificationResultModel
    {
        public StatusCodes NotificationStatusCode { get; set; }

        public IEnumerable<Exception> Exceptions { get; set; }

        public IEnumerable<string> Messages { get; set; }
    }
}
