﻿using System;
using System.Collections.Generic;
using System.Text;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Models
{
    public class GroupAndTranslationsModel
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public int Sno { get; set; }
        public IEnumerable<TextApiModel> Text { get; set; }
        public IEnumerable<PdfApiModel> Pdf { get; set; }
    }
}
