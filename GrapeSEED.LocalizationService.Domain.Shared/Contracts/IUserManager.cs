﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface IUserManager
    {
        Task UpdatePassword(User user);

        Task<ForgotPasswordStatus> ForgotPassword(string email);

        Task<bool> ToggleNotificationEnabled(int userId);

        Task<IEnumerable<UserModel>> GetUsers();

        Task<bool> Save(int id, List<int> languageIds, List<int> roleIds);

        Task<bool> InviteUser(string email, List<int> languageIds, List<int> roleIds, string name);

        Task<bool> ActivateAccount(string invitationCode, string newPassword);

        Task<bool> ToggleDisabled(int id, bool disabled);
    }
}
