﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface ILocalizationStorageManager
    {
        Task<string> UploadFileToStorageAsync(int pageId, string path, Stream fileStream);

        Task DeleteFileFromStorageAsync(int imageId, string path);

        Task<object> GetFileFromStorageAsync(int pageId);

        Task<List<GroupImage>> GetMultipleImagesFromStorageAsync(List<int> groupIds);

        string ReadFileFromStorage(string path);

        Task<bool> DeleteFile(string path);

        Task CreateOrReplaceBlob(string blob, Stream stream, string container = null);

    }
}
