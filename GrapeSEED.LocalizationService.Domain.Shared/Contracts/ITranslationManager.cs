﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface ITranslationManager
    {
        Task<Pdf> SaveEnglishPdf(PdfTranslationApiModel data, int pageId, string fileName, List<LanguageSectionIgnore> sectionsIgnored);

        Task<PdfTranslation> SaveTranslatedPdf(PdfTranslationApiModel data, int pageId, string fileName);

        Task<int> SaveUploadedImage(int pageId, string fileName);

        Task<long> ToggleApplicationLockAsync(int applicationId, bool isLocked);

        Task ToggleIgnoreAsync(IgnoreApiModel ignoreApiModel);

        /// <summary>
        /// Saves the provided text translations.
        /// </summary>
        /// <param name="textTranslations">The text translations to save.</param>
        /// <returns>Task.</returns>
        Task SaveTextTranslationsAsync(IEnumerable<TextTranslationApiModel> textTranslations);

        List<string> DownloadAllFiles(int sectionId, int languageId, bool includeIgnored, int? selectedLanguage);
    }
}
