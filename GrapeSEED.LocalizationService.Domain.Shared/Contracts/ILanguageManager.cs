﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface ILanguageManager
    {
        Task<IEnumerable<LanguageModel>> GetLanguages();
    }
}
