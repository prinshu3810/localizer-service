using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface IUtilsManager
    {
        Dictionary<string, int> GetAllStatusCodesAsync();
    }
}