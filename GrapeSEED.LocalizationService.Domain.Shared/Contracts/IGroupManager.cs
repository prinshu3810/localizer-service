﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface IGroupManager
    {
        Task<IEnumerable<Group>> GetGroups(int applicationId);

        Task<int> GetOrphanTextsCount(int applicationId);

        Task<int> GetOrphanPdfsCount(int applicationId);

        Task<bool> ChangeGroupName(Group group);

        Task<bool> DeleteGroup(int currentGroupId, int newGroupId);

        Task<bool> AddGroup(Group group);

        Task<IEnumerable<GroupAndTranslationsModel>> GetGroupsAndTranslations(int applicationId);

        Task<IEnumerable<GroupAndTranslationsModel>> GetGroupsAndPdfTranslations(int applicationId);

        Task<bool> RemoveTextFromGroup(int textId);

        Task<bool> RemovePdfFromGroup(int pdfId);

        Task<bool> MoveTextToAnotherGroup(int newGroupId, int textId);

        Task<bool> MovePdfToAnotherGroup(int newGroupId, int pdfId);

        Task<bool> UpdateTextsGroupId(IEnumerable<Text> texts);

        Task<bool> UpdatePdfsGroupId(IEnumerable<Pdf> pdfs);

        Task<bool> UpdateTextGroupId(Text text);
        Task<bool> UpdatePdfGroupId(Pdf pdf);

        Task<Application> GetApplicationAsync(int sectionId);

        Task<bool> AddNoteToGroup(int groupId, string note);

        Task<GroupApiModel> GetGroupAsync(int groupId, params Expression<Func<Group, object>>[] navigationPropertyPaths);
    }
}
