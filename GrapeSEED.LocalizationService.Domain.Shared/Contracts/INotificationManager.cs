﻿using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface INotificationManager
    {
        /// <summary>
        /// Synchronous notification.
        /// </summary>
        /// <param name="model">The input model.</param>
        /// <returns>Result of the operation containing message, exceptions and status code.</returns>
        NotificationResultModel Notify(NotificationApiModel model);

        /// <summary>
        /// <para>
        /// Does not block the calling thread and allows the caller to pass an object to
        /// the method that is invoked when the operation completes.
        /// </para>
        /// </summary>
        /// <param name="model">The input model</param>
        /// <param name="userToken">User defined object passed to handler when the operation completes</param>
        /// <returns>Result of the operation containing message, exceptions and status code.</returns>
        Task<NotificationResultModel> NotifyAsync(NotificationApiModel model, object userToken);
    }
}
