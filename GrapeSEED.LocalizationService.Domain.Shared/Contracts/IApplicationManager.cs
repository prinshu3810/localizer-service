﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface IApplicationManager
    {
        /// <summary>
        /// Edits the application as per the provided information in the model.
        /// </summary>
        /// <param name="model">the input containing the information of application to edit.</param>
        /// <returns>Task.</returns>
        Task EditApplicationAsync(ApplicationInfoApiModel model);

        /// <summary>
        /// Retuns the notification count
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <param name="languageId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        Task<object> GetNotifications(int userId, int roleId, int applicationId = 0, int languageId = 0, bool isGroupView = false, int? sectionId = null, int? groupId = null);

        Task ChangeAllowPublish(int applicationId, bool allowPublish);

        Task<dynamic> GetUngroupedKeysCount();
    }
}
