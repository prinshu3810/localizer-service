﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Domain.Shared.Contracts
{
    public interface ISectionManager
    {
        /// <summary>
        /// Gets the application for a given section.
        /// </summary>
        /// <param name="sectionId">The id of the section for which the application is required.</param>
        /// <returns>ApplicationInfoApiModel</returns>
        Task<ApplicationInfoApiModel> GetApplicationAsync(int sectionId);

        Task<SectionApiModel> GetSectionAsync(int sectionId, params Expression<Func<Section, object>>[] navigationPropertyPaths);

        /// <summary>
        /// Gets the texts for the section.
        /// </summary>
        /// <param name="sectionId">The id of the section for which texts are required.</param>
        /// <returns>Collection of texts</returns>
        Task<IEnumerable<Text>> GetTextsAsync(int sectionId, params Expression<Func<Text, object>>[] navigationPropertyPaths);

        /// <summary>
        /// Adds note to a given section
        /// </summary>
        /// <param name="sectionId">The id of the section for which note has to be added.</param>
        /// <param name="note">The note</param>
        /// <returns>Task.</returns>
        Task AddNoteToSection(int sectionId, string note);

        /// <summary>
        /// Adds sections to the application.
        /// </summary>
        /// <param name="sectionsToAdd">The api section model.</param>
        /// <returns>Task.</returns>
        Task AddSectionsAsync(IEnumerable<SectionApiModel> sectionsToAdd);

        /// <summary>
        /// Adds sections to the application.
        /// </summary>
        /// <param name="sectionsToAdd">The section entity.</param>
        /// <returns>Task.</returns>
        Task AddSectionsAsync(IEnumerable<Section> sectionsToAdd);

        /// <summary>
        /// Edits sections of the application.
        /// </summary>
        /// <param name="sectionsToEdit">The api models of section to be edited.</param>
        /// <returns>Task.</returns>
        Task EditSectionsAsync(IEnumerable<SectionApiModel> sectionsToEdit);

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection id ids of sections to delete.</param>
        /// <returns>Task.</returns>
        Task DeleteSectionsAsync(int translationTypeId, IEnumerable<int> sectionsToDelete);

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection of section api models to delete.</param>
        /// <returns>Task.</returns>
        Task DeleteSectionsAsync(int translationTypeId, IEnumerable<SectionApiModel> sectionsToDelete);

        /// <summary>
        /// Deletes sections from the application.
        /// </summary>
        /// <param name="translationTypeId">The translation type id of the section.</param>
        /// <param name="sectionsToDelete">collection of section entities to delete.</param>
        /// <returns>Task.</returns>
        Task DeleteSectionsAsync(int translationTypeId, IEnumerable<Section> sectionsToDelete);

        /// <summary>
        /// Gets sections for an applications.
        /// </summary>
        /// <param name="applicationId">Application Id to fetch sections for.</param>
        /// <returns></returns>
        Task<IEnumerable<SectionApiModel>> GetSectionsAndTextTranslations(int applicationId);

        Task<IEnumerable<SectionApiModel>> GetSectionAndPdfTranslations(int applicationId);
    }
}

