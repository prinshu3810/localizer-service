﻿using System;
using System.Net;
using GrapeSEED.LocalizationService.Domain.Shared;

namespace GrapeSEED.LocalizationService.Infrastructure.Exceptions
{
    public class ApiException : BaseException
    {
        public ApiException()
        {
        }

        public ApiException(string message) : base(message)
        {
        }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public virtual int Code { get { return (int)HttpStatusCode.InternalServerError; } }
    }

    public class UnauthenticatedException : ApiException
    {
        public UnauthenticatedException(string message) : base(message)
        {
        }

        public override int Code { get { return (int)HttpStatusCode.Unauthorized; } }
    }

    public class ResourceNotFoundException : ApiException
    {
        public ResourceNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public override int Code { get { return (int)HttpStatusCode.NotFound; } }
    }

    public class UnavailableResourceException : ApiException
    {
        public UnavailableResourceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public override int Code { get { return 430; } }
    }

    public class InvalidArgumentException : ApiException
    {
        public InvalidArgumentException(string message) : base(message)
        {

        }

        public override int Code { get { return 435; } }
    }

    public class BadRequestException : ApiException
    {
        public BadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BadRequestException(string message) : base(message)
        {

        }
        public BadRequestException(string message, Exception innerException, int code) : this(message, innerException)
        {
            _code = code;
        }

        private int _code = (int)HttpStatusCode.BadRequest;
        public override int Code { get { return _code; } }
    }

    public class NoAPIResponseException : ApiException
    {
        public NoAPIResponseException(string message) : base(message)
        {

        }

        public override int Code { get { return 437; } }
    }
}
