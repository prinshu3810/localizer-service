﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Controllers;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.DTO.RequestDto.Applications;
using GrapeSEED.LocalizationService.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace GrapeSEED.LocalizationService.Infrastructure.Filters
{
    public class CheckApplicationLock : ActionFilterAttribute
    {
        protected readonly IGeneralRepository<Application> _applicationRepository;
        protected readonly IGeneralRepository<Section> _sectionRepository;
        protected readonly IGeneralRepository<Text> _textRepository;
        protected readonly IGeneralRepository<Pdf> _pdfRepository;
        protected readonly IGeneralRepository<PdfTranslation> _pdfTranslationRepository;

        public CheckApplicationLock(
            IGeneralRepository<Application> applicationRepository,
            IGeneralRepository<Section> sectionRepository,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<Pdf> pdfRepository, 
            IGeneralRepository<PdfTranslation> pdfTranslationRepository)
        {
            _applicationRepository = applicationRepository;
            _sectionRepository = sectionRepository;
            _textRepository = textRepository;
            _pdfRepository = pdfRepository;
            _pdfTranslationRepository = pdfTranslationRepository;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var descriptor = context.ActionDescriptor;
            var arguments = context.ActionArguments;
            string content = null;

            foreach (var parameter in context.ActionDescriptor.Parameters)
            {
                string name = parameter.Name;
                Type type = parameter.ParameterType;
                context.ActionArguments.TryGetValue(name, out object value);
                Application application = null;

                if (value is Int32)
                {
                    int id = Convert.ToInt32(value);
                    
                    switch (name)
                    {
                        case "id": 
                        case "applicationId":
                            application = _applicationRepository.GetById(id);
                            content = ValidateApplication(application, name, Convert.ToString(value));
                            break;
                        case "sectionId":
                            application = _sectionRepository
                                            .GetAllIncluding(section => section.Application)
                                            .FirstOrDefault(section => section.Id == id).Application;
                            content = ValidateApplication(application, name, Convert.ToString(value));
                            break;
                        case "textId":
                            Text text = _textRepository.FirstOrDefault(txt => txt.Id == id);
                            application = _sectionRepository
                                            .GetAllIncluding(section => section.Application)
                                            .FirstOrDefault(section => section.Id == text.SectionId).Application;

                            content = ValidateApplication(application, name, Convert.ToString(value));
                            break;
                        case "pdfId":
                            Pdf pdf = _pdfRepository.FirstOrDefault(file => file.Id == id);
                            application = _sectionRepository
                                            .GetAllIncluding(section => section.Application)
                                            .FirstOrDefault(section => section.Id == pdf.SectionId).Application;
                            content = ValidateApplication(application, name, Convert.ToString(value));
                            break;
                        case "pdfTranslationId":
                            PdfTranslation translationApplication = _pdfTranslationRepository.FirstOrDefault(t => t.Id == id);
                            Pdf translationPdf = _pdfRepository.FirstOrDefault(file => file.Id == translationApplication.PdfId);
                            application = _sectionRepository
                                            .GetAllIncluding(section => section.Application)
                                            .FirstOrDefault(section => section.Id == translationPdf.SectionId).Application;
                            content = ValidateApplication(application, name, Convert.ToString(value));
                            break;
                        default:
                            break;
                    }
                }

                if (value is TextTranslationDTO)
                {
                    TextTranslationDTO dto = value as TextTranslationDTO;
                    application = _sectionRepository
                                .GetAllIncluding(section => section.Application)
                                .FirstOrDefault(section => section.Id == dto.SectionId).Application;
                    content = ValidateApplication(application, name, application.Id.ToString());
                }

                if (value is ApplicationDTO)
                {
                    ApplicationDTO dto = value as ApplicationDTO;
                    application = _applicationRepository.GetById(dto.Id);
                    content = ValidateApplication(application, name, dto.Id.ToString());
                }

                if (value is EditApplicationDto)
                {
                    EditApplicationDto dto = value as EditApplicationDto;
                    application = _applicationRepository.GetById(dto.Id);
                    content = ValidateApplication(application, name, dto.Id.ToString());
                }

                if (value is Application)
                {
                    Application dto = value as Application;
                    application = _applicationRepository.GetById(dto.Id);
                    content = ValidateApplication(application, name, dto.Id.ToString());
                }

                if (value is List<TextTranslation>)
                {
                    List<TextTranslation> dto = value as List<TextTranslation>;
                    int textId = dto.First().TextId;
                    Text translationText = _textRepository.FirstOrDefault(text => text.Id == textId);
                    application = _sectionRepository
                                .GetAllIncluding(section => section.Application)
                                .FirstOrDefault(section => section.Id == translationText.SectionId).Application;
                    content = ValidateApplication(application, name, application.Id.ToString());
                }

                if (value is List<Text>)
                {
                    List<Text> dto = value as List<Text>;
                    int sectionId = dto.First().SectionId;
                    application = _sectionRepository
                                .GetAllIncluding(section => section.Application)
                                .FirstOrDefault(section => section.Id == sectionId).Application;
                    content = ValidateApplication(application, name, application.Id.ToString());
                }

                if (value is IgnoreApplicationDto)
                {
                    IgnoreApplicationDto dto = value as IgnoreApplicationDto;

                    if (dto.ApplicationId > 0)
                    {
                        application = _applicationRepository.GetById(dto.ApplicationId);
                        content = ValidateApplication(application, name, application.Id.ToString());
                    }
                    if (dto.Sections.Any())
                    {
                        Section section = _sectionRepository
                                            .GetAllIncluding(s => s.Application)
                                            .FirstOrDefault(s => dto.Sections.First().Id == s.Id);
                        content = ValidateApplication(section.Application, name, section.Application.Id.ToString());
                    }
                    if (dto.TextTranslations.Any())
                    {
                        Text text = _textRepository
                                        .Where(txt => dto.TextTranslations.First().TextId == txt.Id)
                                        .Include(txt => txt.Section.Application)
                                        .FirstOrDefault();
                        content = ValidateApplication(text.Section.Application, name, text.Section.Application.Id.ToString());
                    }
                    if (dto.PdfTranslations.Any())
                    {
                        Pdf pdf = _pdfRepository
                                        .Where(file => file.PdfTranslation.First().PdfId == file.Id)
                                        .Include(file => file.Section.Application)
                                        .FirstOrDefault();
                        content = ValidateApplication(pdf.Section.Application, name, pdf.Section.Application.Id.ToString());
                    }
                }

                if (!string.IsNullOrEmpty(content))
                {
                    context.Result = new BadRequestObjectResult(content);
                    break;
                }
            }

            base.OnActionExecuting(context);
        }

        private string ValidateApplication(Application application, string name, string value)
        {
            if (application == null)
            {
                return JsonConvert.SerializeObject(new
                {
                    error = typeof(EntityDoesNotExistException).Name,
                    error_description = new EntityDoesNotExistException($"{nameof(Application)} with {name} = {value} does not exist."),
                    error_code = (long)Foundation.Common.Enums.StatusCodes.EntityDoesNotExist
                });
            }

            if (application.IsLocked)
            {
                return JsonConvert.SerializeObject(new
                {
                    error = typeof(ApplicationIsLockedException).Name,
                    error_description = new ApplicationIsLockedException($"{application.Name} is locked by developer"),
                    error_code = (long)Foundation.Common.Enums.StatusCodes.ApplicationIsLocked
                });
            }

            return null;
        }
    }
}
