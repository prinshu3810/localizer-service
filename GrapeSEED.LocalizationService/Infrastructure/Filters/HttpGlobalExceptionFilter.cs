﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Azure.Service.Contracts;
using GrapeSEED.LocalizationService.Azure.Service.Models;
using GrapeSEED.LocalizationService.Domain.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Infrastructure.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment env;
        private readonly ILogger<HttpGlobalExceptionFilter> logger;
        private readonly IAzureStorageRepository<LocalizerErrorLog> _errorLogRepository;

        public HttpGlobalExceptionFilter(IAzureStorageRepository<LocalizerErrorLog> errorLogRepository,
            IHostingEnvironment env, ILogger<HttpGlobalExceptionFilter> logger)
        {
            this._errorLogRepository = errorLogRepository;
            this.env = env;
            this.logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            HandleException(context);
            context.ExceptionHandled = true;
        }

        private async Task HandleException(ExceptionContext context)
        {
            BusinessException<long> exception = null;
            string json = null;
            
            if (context.Exception is BusinessException<long>)
            {
                exception = (BusinessException<long>)context.Exception;
                json = JsonConvert.SerializeObject(new
                {
                    error = exception.GetType().Name,
                    error_description = exception,
                    error_code = exception.Code
                });
            } 
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    error = context.Exception.GetType().Name,
                    error_description = context.Exception,
                    error_code = (long)Foundation.Common.Enums.StatusCodes.Unknown
                });
            }

            string requestBody = await new StreamReader(context.HttpContext.Request.Body).ReadToEndAsync(); // to do
            context.Result = new BadRequestObjectResult(json);
            await WriteExceptionAsync(context.HttpContext, context.Exception, requestBody, 500, customErrorCode: exception != null ? exception.Code : (long)Foundation.Common.Enums.StatusCodes.Unknown);
        }

        private async Task WriteExceptionAsync(HttpContext context, Exception exception, string requestBody, int statusCode = Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError, long? customErrorCode = null)
        {
            var now = DateTime.UtcNow;

            try
            {
                await _errorLogRepository.InsertAsync(new LocalizerErrorLog(Assembly.GetExecutingAssembly().GetName().Name, now.ToString("yyyyMMddHHmmss"))
                {
                    Timestamp = now,
                    Exception = $"{exception.Message}: {exception.ToString()}",
                    InnerException = exception.InnerException == null ? string.Empty : $"{exception.InnerException.Message}:{exception.InnerException.ToString()}",
                    Code = statusCode.ToString(),
                    HttpMethod = context.Request.Method,
                    RequestUrl = context.Request.Path.Value,
                    QueryString = context.Request.QueryString.Value,
                    Form = requestBody
                });
            }
            catch (Exception ex)
            {
            }
        }
    }
}
