﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : BaseController
    {
        private IConfiguration _config;
        private readonly IGeneralRepository<User> _userRepository;
        private readonly IUserManager _userManager;
        public LoginController(IConfiguration config, IGeneralRepository<User> userRepository, IUserManager userManager)
        {
            _config = config;
            _userRepository = userRepository;
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody]UserDTO login)
        {
            IActionResult response = Unauthorized();

            var user = AuthenticateUser(login);

            if (user != null)
            {
                var token = GenerateJSONWebToken(user);
                return Ok(new { token, id = user.Id, username = user.Username, Roles = user.UserRole.Select(role =>role.RoleId), IsPasswordVerified = user.IsPasswordVerified, NotificationsEnabled = user.NotificationsEnabled, Disabled = user.Disabled, Name = user.Name });
            }

            return response;
        }

        #region Private Methods

        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            IdentityOptions options = new IdentityOptions();
            IEnumerable<Claim> claims = new List<Claim>()
            {
                new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(options.ClaimsIdentity.RoleClaimType, user.UserRole.FirstOrDefault().RoleId.ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims: claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private User AuthenticateUser(UserDTO login)
        {
            string password = EncryptionService.Encrypt(_config.GetSection("Settings")["encryptionKey"], login.Password, true);

            var user = _userRepository
                        .Where(u => u.Username.ToLower() == login.Username.ToLower() && u.Password == password && u.InvitationStatus == true)
                        ?.Include(usr => usr.UserRole)
                        ?.FirstOrDefault();

            if (user != null)
            {
                return user;
            }

            return null;
        }

        #endregion

        #region Reset Password 
        /// <summary>
        /// To set OTP and send it to user's email
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("ForgotPassword")]
        [HttpGet]
        public async Task<IActionResult> ForgotPassword(string username)
        {
            ForgotPasswordStatus statusCode = await _userManager.ForgotPassword(username);
            return Ok(new { StatusCode = statusCode });
        }

        /// <summary>
        /// To update password
        /// </summary>
        /// <param name="passwordDetail"></param>
        /// <returns></returns>
        [Route("ChangePassword")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordDTO passwordDetail)
        {
            string token = null;
            UpdatePasswordStatus statusCode;

            if (ModelState.IsValid)
            {
                var userModel = new UserDTO
                {
                    Username = passwordDetail.Username,
                    Password = passwordDetail.CurrentPassword
                };
                var user = AuthenticateUser(userModel);

                if (user != null)
                {
                    var encryptedNewPassword = EncryptionService.Encrypt(_config.GetSection("Settings")["encryptionKey"], passwordDetail.NewPassword, true);
                    user.Password = encryptedNewPassword;
                    user.IsPasswordVerified = true;
                    await _userManager.UpdatePassword(user);
                    token = GenerateJSONWebToken(user);
                    statusCode = UpdatePasswordStatus.Success;
                }
                else
                {
                    statusCode = UpdatePasswordStatus.CurrentPasswordWrong;
                }
            }
            else
            {
                statusCode = UpdatePasswordStatus.InvalidParameters; 
            }

            return Ok(new { Token = token, StatusCode = statusCode });
        }


        #endregion

        #region Toggle Notification Enabled
        [Route("toggleEmailNotifications")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ToggleNotificationEnabled() {
            return Ok(await _userManager.ToggleNotificationEnabled(UserId));
        }
        #endregion
    }
}