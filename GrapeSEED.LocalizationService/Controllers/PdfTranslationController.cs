﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.DTO.RequestDto;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]

    public class PdfTranslationController : BaseController
    {
        private readonly ILocalizationStorageManager _storageManager;
        private readonly ITranslationManager _translationManager;
        private readonly IGeneralRepository<Application> _applicationRepository;
        private readonly IGeneralRepository<Pdf> _pdfRepository;
        private readonly IGeneralRepository<PdfTranslation> _pdfTranslationRepository;
        private readonly ISectionManager _sectionManager;
        private readonly IRelationsRepository<UserRole> _userRoleRepository;
        private readonly IGroupManager _groupManager;

        public PdfTranslationController(
            ITranslationManager translationManager,
            ILocalizationStorageManager storageManager,
            IGeneralRepository<Application> applicationRepository,
            IGeneralRepository<Pdf> pdfRepository,
            IRelationsRepository<UserRole> userRoleRepository,
            IGeneralRepository<PdfTranslation> pdfTranslationRepository,
            ISectionManager sectionManager,
            IGroupManager groupManager)

        {
            _translationManager = translationManager;
            _storageManager = storageManager;
            _applicationRepository = applicationRepository;
            _pdfRepository = pdfRepository;
            _userRoleRepository = userRoleRepository;
            _pdfTranslationRepository = pdfTranslationRepository;
            _sectionManager = sectionManager;
            _groupManager = groupManager;
        }

        [HttpPost]
        [Route("Upload")]
        public async Task<IActionResult> UploadAsync()
        {
            IFormFile file = Request.Form?.Files?.FirstOrDefault();
            Request?.Form?.TryGetValue("section", out var value);
            string section = Convert.ToString(value);

            Request?.Form?.TryGetValue("application", out value);
            string application = Convert.ToString(value);

            Request?.Form?.TryGetValue("sectionId", out value);
            int sectionId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("languageId", out value);
            int languageId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("pdfTranslation", out value);
            PdfTranslationApiModel pdfTranslation = JsonConvert.DeserializeObject<PdfTranslationApiModel>(value);

            var relatedSection = await _sectionManager.GetSectionAsync(sectionId, s => s.LanguageSectionIgnore);

            // If section is ignored/un-ignored, then new translation should also be ignored/un-ignored accordingly.
            if (relatedSection != null)
            {
                pdfTranslation.IsVisible = !relatedSection.LanguageSectionIgnores.Any(item => item.LanguageId == languageId);
            }

            if (file == null || sectionId == 0 || String.IsNullOrEmpty(section) || String.IsNullOrEmpty(application))
            {
                return null;
            }

            string fileName = file.FileName;
            string accessUrl = null;
            string path = Constants.PDF_TRNSLATION_FOLDER_NAME + "/" + pdfTranslation.Language + "/" +
                application + "/" + section + "/" + pdfTranslation.Key + "/" + file.FileName;

            using (Stream stream = file.OpenReadStream())
            {
                accessUrl = await _storageManager.UploadFileToStorageAsync(sectionId, path, stream);
            }
            pdfTranslation.Path = path;
            var pdf = await _translationManager.SaveEnglishPdf(pdfTranslation, sectionId, file.FileName, relatedSection.LanguageSectionIgnores.ToList());

            return Ok();
        }

        [HttpDelete]
        [Route("DeleteImage")]
        public async Task<IActionResult> DeleteImageAsync(int imageId, string fileName)
        {
            try
            {
                await _storageManager.DeleteFileFromStorageAsync(Convert.ToInt32(imageId), fileName);
                return Ok();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("GetPdfTranslations")]
        public async Task<IActionResult> GetPdfTranslations(int sectionId, int languageId, int userId)
        {
            int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
            var pdfTranslations = _pdfRepository.Where(t => t.SectionId == sectionId)
                                        .Join(_pdfTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         pdf => pdf.Id, trans => trans.PdfId,
                                         (pdf, pdfTrans) => new { Pdf = pdf, PdfTranslation = pdfTrans })
                                        .GroupBy(data => data.Pdf.Id)
                                        .Select(data => new
                                        {
                                            PdfId = data.Key,
                                            Key = data.Max(x => x.Pdf.Key),
                                            ReleaseId = data.Any() && data.FirstOrDefault().Pdf != null ? data.FirstOrDefault().Pdf.VersionId : 0,
                                            SectionId = data.Max(x => x.Pdf.SectionId),
                                            PdfTranslation = data.Select(x => new
                                            {
                                                PdfTranslationId = x.PdfTranslation.Id,
                                                x.PdfTranslation.PdfId,
                                                x.PdfTranslation.StatusId,
                                                x.PdfTranslation.LanguageId,
                                                x.PdfTranslation.IsVisible,
                                                x.PdfTranslation.Path
                                            })
                                        }).ToList();

            var application = await _sectionManager.GetApplicationAsync(sectionId);

            return Ok(new { PdfTranslations = pdfTranslations, Application = application });
        }

        [HttpGet]
        [Route("GetGroupPdfTranslations")]
        public async Task<IActionResult> GetGroupPdfTranslations(int groupId, int languageId, int userId)
        {
            if (groupId == 0)
            {
                return NoContent();
            }
            int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
            var pdfTranslations = _pdfRepository.Where(t => t.GroupId == groupId)
                                        .Join(_pdfTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         pdf => pdf.Id, trans => trans.PdfId,
                                         (pdf, pdfTrans) => new { Pdf = pdf, PdfTranslation = pdfTrans })
                                        .GroupBy(data => data.Pdf.Id)
                                        .Select(data => new
                                        {
                                            PdfId = data.Key,
                                            Key = data.Max(x => x.Pdf.Key),
                                            ReleaseId = data.Any() && data.FirstOrDefault().Pdf != null ? data.FirstOrDefault().Pdf.VersionId : 0,
                                            GroupId = data.Max(x => x.Pdf.GroupId),
                                            PdfTranslation = data.Select(x => new
                                            {
                                                PdfTranslationId = x.PdfTranslation.Id,
                                                x.PdfTranslation.PdfId,
                                                x.PdfTranslation.StatusId,
                                                x.PdfTranslation.LanguageId,
                                                x.PdfTranslation.IsVisible,
                                                x.PdfTranslation.Path
                                            })
                                        }).ToList();

            var application = await _groupManager.GetApplicationAsync(groupId);

            return Ok(new { PdfTranslations = pdfTranslations, Application = application });
        }

        [HttpPost]
        [Route("AddPdfTranslation")]
        public async Task<IActionResult> AddTranslation()
        {
            IFormFile file = Request.Form?.Files?.FirstOrDefault();
            Request?.Form?.TryGetValue("section", out var value);
            string section = Convert.ToString(value);

            Request?.Form?.TryGetValue("application", out value);
            string application = Convert.ToString(value);

            Request?.Form?.TryGetValue("sectionId", out value);
            int sectionId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("languageId", out value);
            int languageId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("pdfTranslation", out value);
            PdfTranslationApiModel pdfTranslation = JsonConvert.DeserializeObject<PdfTranslationApiModel>(value);
            var relatedSection = await _sectionManager.GetSectionAsync(sectionId, s => s.LanguageSectionIgnore);

            // If section is ignored/un-ignored, then new translation should also be ignored/un-ignored accordingly.
            if (relatedSection != null)
            {
                pdfTranslation.IsVisible = !relatedSection.LanguageSectionIgnores.Any(item => item.LanguageId == languageId);
            }

            string fileName = file.FileName;
            string accessUrl = null;
            string path = Constants.PDF_TRNSLATION_FOLDER_NAME + "/" + pdfTranslation.Language + "/" + application + "/"
                + section + "/" + pdfTranslation.Key + "/" + file.FileName;

            using (Stream stream = file.OpenReadStream())
            {
                accessUrl = await _storageManager.UploadFileToStorageAsync(sectionId, path, stream);
            }

            pdfTranslation.Path = path;
            PdfTranslation pdfTranslationResponse = await _translationManager.SaveTranslatedPdf(pdfTranslation, sectionId, file.FileName);

            return Ok(new Pdf
            {
                Key = pdfTranslation.Key,
                SectionId = pdfTranslation.SectionId,
                PdfTranslation = new List<PdfTranslation>()
                {
                    new PdfTranslation
                    {
                        Id = pdfTranslationResponse.Id,
                        StatusId = pdfTranslationResponse.StatusId,
                        PdfId = pdfTranslationResponse.PdfId,
                        Path = pdfTranslationResponse.Path,
                        LanguageId = pdfTranslationResponse.LanguageId,
                        IsVisible = pdfTranslationResponse.IsVisible
                    }
                }
            });
        }

        [HttpPost]
        [Route("AddPdfTranslationForGroup")]
        public async Task<IActionResult> AddTranslationForGroup()
        {
            IFormFile file = Request.Form?.Files?.FirstOrDefault();
            Request?.Form?.TryGetValue("group", out var value);
            string group = Convert.ToString(value);

            Request?.Form?.TryGetValue("application", out value);
            string application = Convert.ToString(value);

            Request?.Form?.TryGetValue("groupId", out value);
            int groupId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("languageId", out value);
            int languageId = Convert.ToInt32(value);

            Request?.Form?.TryGetValue("pdfTranslation", out value);
            PdfTranslationApiModel pdfTranslation = JsonConvert.DeserializeObject<PdfTranslationApiModel>(value);
            var relatedGroup = await _groupManager.GetGroupAsync(groupId, s => s.LanguageGroupIgnore);

            // If section is ignored/un-ignored, then new translation should also be ignored/un-ignored accordingly.
            if (relatedGroup != null)
            {
                pdfTranslation.IsVisible = !relatedGroup.LanguageGroupIgnores.Any(item => item.LanguageId == languageId);
            }

            string fileName = file.FileName;
            string accessUrl = null;
            string path = Constants.PDF_TRNSLATION_FOLDER_NAME + "/" + pdfTranslation.Language + "/" + application + "/"
                + group + "/" + pdfTranslation.Key + "/" + file.FileName;

            using (Stream stream = file.OpenReadStream())
            {
                accessUrl = await _storageManager.UploadFileToStorageAsync(groupId, path, stream);
            }

            pdfTranslation.Path = path;
            PdfTranslation pdfTranslationResponse = await _translationManager.SaveTranslatedPdf(pdfTranslation, groupId, file.FileName);

            return Ok(new Pdf
            {
                Key = pdfTranslation.Key,
                GroupId = pdfTranslation.GroupId,
                PdfTranslation = new List<PdfTranslation>()
                {
                    new PdfTranslation
                    {
                        Id = pdfTranslationResponse.Id,
                        StatusId = pdfTranslationResponse.StatusId,
                        PdfId = pdfTranslationResponse.PdfId,
                        Path = pdfTranslationResponse.Path,
                        LanguageId = pdfTranslationResponse.LanguageId,
                        IsVisible = pdfTranslationResponse.IsVisible
                    }
                }
            });
        }

        [HttpGet]
        [Route("DownloadAllFiles")]
        public IActionResult DownloadAllFiles(int sectionId, int languageId, bool includeIgnored, int? selectedLanguage)
        {
            return Ok(_translationManager.DownloadAllFiles(sectionId, languageId, includeIgnored, selectedLanguage));
        }

        [HttpGet]
        [Route("DownloadPdf")]
        public IActionResult DownloadPdf(string path)
        {
            return Ok(_storageManager.ReadFileFromStorage(path));
        }

        [HttpDelete]
        [Route("DeletePdf")]
        public async Task<IActionResult> DeletePdf(int pdfTranslationId, string path, bool isMultiple)
        {
            try
            {
                await _storageManager.DeleteFile(path);
                var pdfTranslation = await _pdfTranslationRepository.GetByIdAsync(pdfTranslationId);
                if (pdfTranslation.LanguageId == 1)
                {
                    if (isMultiple)
                    {
                        await _pdfTranslationRepository.DeleteAsync(pdfTranslationId);
                    }
                    else
                    {
                        pdfTranslation.Path = "";
                        pdfTranslation.FileName = "";
                        pdfTranslation.StatusId = (int)Enums.Status.New;
                        await _pdfTranslationRepository.EditAsync(pdfTranslation, nameof(pdfTranslation.Path), nameof(pdfTranslation.StatusId), nameof(pdfTranslation.UpdatedDate));
                    }

                    var entityList = _pdfTranslationRepository.Where(x => x.PdfId == pdfTranslation.PdfId && x.LanguageId != 1).ToList();
                    entityList.ForEach(entity =>
                    {
                        entity.StatusId = (int)Enums.Status.Updated;
                    });
                    await _pdfTranslationRepository.EditMultipleAsync(entityList);
                }
                else
                {
                    await _pdfTranslationRepository.DeleteAsync(pdfTranslationId);
                }

                return Ok();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("DeletePdfTranslation")]
        public async Task<IActionResult> DeletePdfTranslation(int pdfId)
        {
            Pdf pdf = _pdfRepository
                            .Where(p => p.Id == pdfId)
                            .Include(t => t.PdfTranslation)
                            .FirstOrDefault();

            pdf.PdfTranslation.ToList().ForEach(async (translation) =>
            {
                if (!String.IsNullOrEmpty(translation.Path))
                {
                    await _storageManager.DeleteFile(translation.Path);
                }
            });

            await _pdfTranslationRepository.DeleteMultipleAsync(pdf.PdfTranslation);
            await _pdfRepository.DeleteAsync(pdf);
            return Ok();
        }

        [HttpGet]
        [Route("PublishPdfTranslations")]
        public async Task<IActionResult> PublishTranslations([FromQuery] int languageId, [FromQuery] int applicationId, [FromQuery] int releaseId)
        {
            List<PdfTranslation> pdfTranslation = new List<PdfTranslation>();
            if (releaseId == 0)
            {
                pdfTranslation = await _pdfTranslationRepository
                                  .Where(item => item.StatusId == (int)Enums.Status.Updated && item.LanguageId == languageId
                                      && item.Pdf.Section.ApplicationId == applicationId).ToListAsync();
            }
            else
            {
                pdfTranslation = await _pdfTranslationRepository
                                   .Where(item => item.StatusId == (int)Enums.Status.Updated && item.LanguageId == languageId
                                       && item.Pdf.Section.ApplicationId == applicationId && item.Pdf.VersionId == releaseId).ToListAsync();
            }


            pdfTranslation.ForEach(t =>
            {
                t.StatusId = (int)Enums.Status.Published;
            });

            await _pdfTranslationRepository.EditMultipleAsync(pdfTranslation, nameof(PdfTranslation.StatusId));
            return Ok();
        }
        [HttpGet]
        [Route("SearchFileTranslation")]
        public IActionResult SearchFileTranslation(string keyword, string sectionIds, int languageId, int roleId)
        {
            if (string.IsNullOrWhiteSpace(keyword) || string.IsNullOrEmpty(sectionIds) || languageId == 0 || roleId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(keyword)}={keyword}, {nameof(sectionIds)}={sectionIds}, {nameof(languageId)}={languageId}, {nameof(roleId)}={roleId}");
            }

            keyword = keyword.ToLower();
            List<int> sectionList = sectionIds.Split(',').Select(int.Parse).ToList();
            var pdfTranslations = _pdfRepository.Where(t => sectionList.Contains(t.SectionId))
                                        .Join(_pdfTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         pdf => pdf.Id, trans => trans.PdfId,
                                         (pdf, pdfTrans) => new PdfTranslationGroupDTO { Pdf = pdf, PdfTranslation = pdfTrans }).ToList();

            List<PdfTranslationGroupDTO> filteredTranslations = null;

            filteredTranslations = pdfTranslations.Where(item => item.Pdf.Key.ToLower().Contains(keyword)
                                      || item.PdfTranslation.FileName.ToLower().Contains(keyword)).ToList();

            var groupedTranslations = filteredTranslations.GroupBy(data => data.Pdf.Id).ToList();
            List<PdfTranslationGroupDTO> newTranslations = new List<PdfTranslationGroupDTO>();

            if (languageId != 1)
            {
                groupedTranslations.ForEach(currentTranslation =>
                {
                    if (currentTranslation.Count() == 1)
                    {
                        if (currentTranslation.First().PdfTranslation.LanguageId != 1)
                        {
                            var pdfTranslation = pdfTranslations.Find(item => item.PdfTranslation.PdfId == currentTranslation.First().PdfTranslation.PdfId
                            && item.PdfTranslation.LanguageId == 1);
                            if (pdfTranslation != null)
                            {
                                newTranslations.Add(pdfTranslation);
                            }
                        }
                        else
                        {
                            var pdfTranslation = pdfTranslations.Find(item => item.PdfTranslation.PdfId == currentTranslation.First().PdfTranslation.PdfId
                           && item.PdfTranslation.LanguageId == languageId);
                            if (pdfTranslation != null)
                            {
                                newTranslations.Add(pdfTranslation);
                            }
                        }
                    }
                });
            }

            foreach (PdfTranslationGroupDTO translation in newTranslations)
            {
                filteredTranslations.Add(translation);
            }

            var translationResult = filteredTranslations
                                         .GroupBy(data => data.Pdf.Id)
                                        .Select(data => new
                                        {
                                            PdfId = data.Key,
                                            Key = data.Max(x => x.Pdf.Key),
                                            SectionId = data.Max(x => x.Pdf.SectionId),
                                            PdfTranslation = data.Select(x => new
                                            {
                                                PdfTranslationId = x.PdfTranslation.Id,
                                                x.PdfTranslation.IsVisible,
                                                x.PdfTranslation.PdfId,
                                                x.PdfTranslation.StatusId,
                                                x.PdfTranslation.LanguageId,
                                                x.PdfTranslation.Path
                                            })
                                        }).ToList();

            return Ok(new { PdfTranslations = translationResult });
        }
        [HttpPost]
        [Route("SearchFileTranslationV2")]

        public IActionResult SearchTextTranslationV2([FromBody] SearchFileTranslationsDTO model)
        {
            return SearchFileTranslation(model.Keyword, model.SectionIds, model.LanguageId, model.RoleId);
        }
        [HttpGet]
        [Route("SearchFileTranslationForGroup")]
        public IActionResult SearchFileTranslationForGroup(string keyword, string groupIds, int languageId, int roleId)
        {
            if (string.IsNullOrWhiteSpace(keyword) || string.IsNullOrEmpty(groupIds) || languageId == 0 || roleId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(keyword)}={keyword}, {nameof(groupIds)}={groupIds}, {nameof(languageId)}={languageId}, {nameof(roleId)}={roleId}");
            }

            keyword = keyword.ToLower();
            List<int> groupList = groupIds.Split(',').Select(int.Parse).ToList();
            var pdfTranslations = _pdfRepository.Where(t => groupList.Contains(t.GroupId ?? 0))
                                        .Join(_pdfTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         pdf => pdf.Id, trans => trans.PdfId,
                                         (pdf, pdfTrans) => new PdfTranslationGroupDTO { Pdf = pdf, PdfTranslation = pdfTrans }).ToList();

            List<PdfTranslationGroupDTO> filteredTranslations = null;

            filteredTranslations = pdfTranslations.Where(item => item.Pdf.Key.ToLower().Contains(keyword)
                                      || item.PdfTranslation.FileName.ToLower().Contains(keyword)).ToList();

            var groupedTranslations = filteredTranslations.GroupBy(data => data.Pdf.Id).ToList();
            List<PdfTranslationGroupDTO> newTranslations = new List<PdfTranslationGroupDTO>();

            if (languageId != 1)
            {
                groupedTranslations.ForEach(currentTranslation =>
                {
                    if (currentTranslation.Count() == 1)
                    {
                        if (currentTranslation.First().PdfTranslation.LanguageId != 1)
                        {
                            var pdfTranslation = pdfTranslations.Find(item => item.PdfTranslation.PdfId == currentTranslation.First().PdfTranslation.PdfId
                            && item.PdfTranslation.LanguageId == 1);
                            if (pdfTranslation != null)
                            {
                                newTranslations.Add(pdfTranslation);
                            }
                        }
                        else
                        {
                            var pdfTranslation = pdfTranslations.Find(item => item.PdfTranslation.PdfId == currentTranslation.First().PdfTranslation.PdfId
                           && item.PdfTranslation.LanguageId == languageId);
                            if (pdfTranslation != null)
                            {
                                newTranslations.Add(pdfTranslation);
                            }
                        }
                    }
                });
            }

            foreach (PdfTranslationGroupDTO translation in newTranslations)
            {
                filteredTranslations.Add(translation);
            }

            var translationResult = filteredTranslations
                                         .GroupBy(data => data.Pdf.Id)
                                        .Select(data => new
                                        {
                                            PdfId = data.Key,
                                            Key = data.Max(x => x.Pdf.Key),
                                            GroupId = data.Max(x => x.Pdf.GroupId),
                                            PdfTranslation = data.Select(x => new
                                            {
                                                PdfTranslationId = x.PdfTranslation.Id,
                                                x.PdfTranslation.IsVisible,
                                                x.PdfTranslation.PdfId,
                                                x.PdfTranslation.StatusId,
                                                x.PdfTranslation.LanguageId,
                                                x.PdfTranslation.Path
                                            })
                                        }).ToList();

            return Ok(new { PdfTranslations = translationResult });
        }
        [HttpPost]
        [Route("SearchFileTranslationForGroupV2")]

        public IActionResult SearchTextTranslationForGroupV2([FromBody] SearchTextTranslationsDTO model)
        {
            return SearchFileTranslationForGroup(model.Keyword, model.GroupIds, model.LanguageId, model.RoleId);
        }
    }
}