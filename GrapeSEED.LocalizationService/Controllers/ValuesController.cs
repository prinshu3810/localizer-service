﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ValuesController : ControllerBase
    {
        private readonly INotificationManager _notificationManager;

        public ValuesController(INotificationManager notificationManager)
        {
            _notificationManager = notificationManager;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet("NotificationTest")]
        public async Task<NotificationResultModel> NotificationTest()
        {
            var model = new Domain.Shared.Models.NotificationApiModel()
            {
                Body = "Test notify",
                Subject = "Test notify",
                //ToAddresses = new [] {"saurabh.pati@grapecity.com"},
                BccAddresses = new[] { "saurabh.pati@grapecity.com" },

            };

            NotificationResultModel result1 = _notificationManager.Notify(model);
            model.OperationEventHandler = (sender, e) =>
            {

            };
            model.Body = "Test NotifyAsync";
            model.Subject = "Test NotifyAsync";
            var result2 = await _notificationManager.NotifyAsync(model, model);

            return result2;
        }
    }
}
