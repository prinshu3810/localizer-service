using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilsController : BaseController
    {
        private readonly IUtilsManager _utilsManager;

        public UtilsController(IUtilsManager utilsManager)
        {
            _utilsManager = utilsManager;
        }

        [HttpGet]
        [Route("StatusCodes")]
        public IActionResult GetAllStatusCodesAsync() 
        {
            return Ok(_utilsManager.GetAllStatusCodesAsync());
        }
    }
}