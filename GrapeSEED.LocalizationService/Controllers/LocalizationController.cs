﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.DTO;
using GrapeSEED.LocalizationService.DTO.RequestDto;
using GrapeSEED.LocalizationService.DTO.RequestDto.Applications;
using GrapeSEED.LocalizationService.DTO.ResponseDto;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Infrastructure.Filters;
using GrapeSEED.LocalizationService.Repository.Shared;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;
using Status = GrapeSEED.LocalizationService.Foundation.Common.Enums.Status;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class LocalizationController : BaseController
    {
        private readonly ILocalizationStorageManager _storageService;
        private readonly IGeneralRepository<User> _userRepository;
        private readonly IGeneralRepository<Application> _applicationRepository;
        private readonly IGeneralRepository<Language> _languageRepository;
        private readonly IGeneralRepository<Text> _textRepository;
        private readonly IGeneralRepository<Repository.Shared.Models.Version> _versionRepository;
        private readonly IGeneralRepository<TextTranslation> _textTranslationRepository;
        private readonly IGeneralRepository<Pdf> _pdfRepository;
        private readonly IGeneralRepository<PdfTranslation> _pdfTranslationRepository;
        private readonly IGeneralRepository<UserRole> _userRoleRepository;
        private readonly IGeneralRepository<UserLanguage> _userLanguageRepository;
        private readonly IGeneralRepository<Group> _groupRepository;
        private readonly ITranslationManager _translationManager;
        private readonly ISectionManager _sectionManager;
        private readonly IRelationsRepository<LanguageSectionIgnore> _langSectionIgnoreRepository;
        private readonly IApplicationManager _applicationManager;
        private readonly ILocalizationStorageManager _storageManager;
        private readonly IGroupManager _groupManager;
        private readonly gclocalizationdbContext _dbContext;

        public LocalizationController(
            ILocalizationStorageManager storageService,
            IGeneralRepository<User> userRepository,
            IGeneralRepository<Application> applicationRepository,
            IGeneralRepository<Language> languageRepository,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<Pdf> pdfRepository,
            IGeneralRepository<Repository.Shared.Models.Version> versionRepository,
            IGeneralRepository<TextTranslation> textTranslationRepository,
            IGeneralRepository<PdfTranslation> pdfTranslationRepository,
            IRelationsRepository<UserRole> userRoleRepository,
            IGeneralRepository<UserLanguage> userLanguageRepository,
            ITranslationManager translationManager,
            ISectionManager sectionManager,
            IRelationsRepository<LanguageSectionIgnore> langSectionIgnoreRepository,
            IApplicationManager applicationManager,
            ILocalizationStorageManager storageManager,
            IGroupManager groupManager,
            IGeneralRepository<Group> groupRepository,
            gclocalizationdbContext dbContext
            )
        {
            _storageService = storageService;
            _userRepository = userRepository;
            _applicationRepository = applicationRepository;
            _languageRepository = languageRepository;
            _textRepository = textRepository;
            _textTranslationRepository = textTranslationRepository;
            _pdfRepository = pdfRepository;
            _pdfTranslationRepository = pdfTranslationRepository;
            _userRoleRepository = userRoleRepository;
            _versionRepository = versionRepository;
            _userLanguageRepository = userLanguageRepository;
            _translationManager = translationManager;
            _sectionManager = sectionManager;
            _langSectionIgnoreRepository = langSectionIgnoreRepository;
            _applicationManager = applicationManager;
            _storageManager = storageManager;
            _groupManager = groupManager;
            _groupRepository = groupRepository;
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("GetUserInfo")]
        public async Task<IActionResult> GetUserInfo(int id)
        {
            var user = await _userRepository
                            .Where(u => u.Id == id)
                            .AsNoTracking()
                            .Include(u => u.UserRole).Include(u => u.UserLanguage)
                            .Select(p => new
                            {
                                p.Username,
                                Role = p.UserRole.FirstOrDefault().RoleId,
                                p.Id,
                                p.UserLanguage
                            }).FirstOrDefaultAsync();

            List<Application> data = await _applicationRepository
                                        .GetAll()
                                        .Where(app => app.IsVisible)
                                        .AsNoTracking()
                                        .Include(app => app.LanguageApplicationIgnore)
                                        .Include(app => app.Group)
                                        .ThenInclude(group => group.LanguageGroupIgnore)
                                        .Include(app => app.Section)
                                        .ThenInclude(section => section.LanguageSectionIgnore)
                                        .ToListAsync();

            string json = JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Ok(new { Userinfo = user, Application = json });
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public IActionResult GetAllLanguages()
        {
            var languages = _languageRepository.GetAll().ToList().OrderBy(lan => lan.Name);
            return Ok(new { languages });
        }

        [HttpGet]
        [Route("CheckUniqueKey")]
        public IActionResult CheckUniqueKey(int sectionId, string key)
        {
            var text = _textRepository.Where(t => t.SectionId == sectionId && t.Key.ToLower().Trim() == key.ToLower().Trim()).FirstOrDefault();

            if (text != null)
            {
                return Ok(new { IsUnique = false });
            }
            return Ok(new { IsUnique = true });
        }

        [HttpGet]
        [Route("GetNotifications")]
        public async Task<IActionResult> GetNotifications(int userId, int roleId, int applicationId = 0, int languageId = 0, bool isGroupView = false, int? sectionId = null, int? groupId = null)
        {
            return Ok(await _applicationManager.GetNotifications(userId, roleId, applicationId, languageId, isGroupView, sectionId, groupId));
        }

        [HttpGet]
        [Route("GetNotificationsOld")]
        public IActionResult GetNotificationsOld(int userId, int roleId, int applicationId = 0, int languageId = 0, int? sectionId = null)
        {
            List<DTO.ResponseDto.NotificationResponse> response = new List<DTO.ResponseDto.NotificationResponse>();

            //Dev
            if (roleId == 1)
            {
                response = _textRepository.Where(text => text.Section.ApplicationId == (applicationId != 0 ? applicationId : text.Section.ApplicationId) && text.Section.IsVisible)
                                .Join(_textTranslationRepository
                                        .Where(translation => translation.StatusId == 2 && (languageId != 0 ? translation.LanguageId == languageId : true)),
                                        text => text.Id, translation => translation.TextId,
                                        (text, trans) => new { Text = text, Translations = trans })
                                .GroupBy(grpData => new { grpData.Text.Section.ApplicationId, grpData.Text.SectionId, grpData.Translations.LanguageId })
                                .Select(notData => new DTO.ResponseDto.NotificationResponse
                                {
                                    ApplicationId = notData.Key.ApplicationId,
                                    SectionId = notData.Key.SectionId,
                                    LanguageId = notData.Key.LanguageId,
                                    NotificationCount = notData.Where(x => x.Translations.IsVisible == true).Count()
                                }).ToList();

                var responsePdf = _pdfRepository.Where(pdf => pdf.Section.ApplicationId == (applicationId != 0 ? applicationId : pdf.Section.ApplicationId) && pdf.Section.IsVisible)
                                .Join(_pdfTranslationRepository
                                        .Where(translation => translation.StatusId == 2 && (languageId != 0 ? translation.LanguageId == languageId : true)),
                                        text => text.Id, translation => translation.PdfId,
                                        (pdf, trans) => new { Pdf = pdf, Translations = trans })
                                .GroupBy(grpData => new { grpData.Pdf.Section.ApplicationId, grpData.Pdf.SectionId, grpData.Translations.LanguageId })
                                .Select(notData => new DTO.ResponseDto.NotificationResponse
                                {
                                    ApplicationId = notData.Key.ApplicationId,
                                    SectionId = notData.Key.SectionId,
                                    LanguageId = notData.Key.LanguageId,
                                    NotificationCount = notData.Where(x => x.Translations.IsVisible == true).GroupBy(x => x.Pdf.Id).Count()
                                }).ToList();

                response.AddRange(responsePdf);
            }
            else if (roleId == 2) //Translator
            {
                List<int> allowedLanguages;

                if (languageId == 0)
                {
                    allowedLanguages = _userLanguageRepository.Where(ul => ul.UserId == userId).Select(ul => ul.LanguageId).ToList();
                }
                else
                {
                    allowedLanguages = _userLanguageRepository.Where(ul => ul.UserId == userId && ul.LanguageId == languageId).Select(ul => ul.LanguageId).ToList();
                }

                var data = _textRepository.Where(text => text.Section.ApplicationId == (applicationId != 0 ? applicationId : text.Section.ApplicationId) && text.Section.IsVisible)
                                .Join(_textTranslationRepository
                                    .Where(translation => (translation.LanguageId == 1 || allowedLanguages.Contains(translation.LanguageId))),
                                    text => text.Id, translation => translation.TextId,
                                    (text, trans) => new { Text = text, Translations = trans })
                                .GroupBy(grpData => new { grpData.Text.Section.ApplicationId, grpData.Text.SectionId, grpData.Translations.LanguageId })
                                .Select(notData => new
                                {
                                    notData.Key.ApplicationId,
                                    notData.Key.SectionId,
                                    notData.Key.LanguageId,
                                    TranslationsCount = notData.Count(),
                                    NewEnglishCount = notData.Where(x => x.Translations.StatusId != 3 && x.Translations.LanguageId == 1 && x.Translations.IsVisible == true).Count()
                                }).ToList();

                var dataPdf = _pdfRepository.Where(pdf => pdf.Section.ApplicationId == (applicationId != 0 ? applicationId : pdf.Section.ApplicationId) && pdf.Section.IsVisible)
                                .Join(_pdfTranslationRepository
                                    .Where(translation => (translation.LanguageId == 1 || allowedLanguages.Contains(translation.LanguageId))),
                                    text => text.Id, translation => translation.PdfId,
                                    (pdf, trans) => new { Pdf = pdf, Translations = trans })
                                .GroupBy(grpData => new { grpData.Pdf.Section.ApplicationId, grpData.Pdf.SectionId, grpData.Translations.LanguageId })
                                .Select(notData => new
                                {
                                    notData.Key.ApplicationId,
                                    notData.Key.SectionId,
                                    notData.Key.LanguageId,
                                    TranslationsCount = notData.GroupBy(x => x.Pdf.Id).Count(),
                                    NewEnglishCount = notData.Where(x => x.Translations.StatusId != 3
                                    && x.Translations.LanguageId == 1 && x.Translations.IsVisible == true)
                                    .GroupBy(x => x.Pdf.Id).Count()
                                }).ToList();

                data.AddRange(dataPdf);

                var dataEnglish = data.Where(d => d.LanguageId == 1)/*.ToList()*/;
                var dataNotEnglish = data.Where(d => d.LanguageId != 1)/*.ToList()*/;

                allowedLanguages.ForEach(language =>
                {
                    if (language == 1)
                    {
                        var sections = data.Where(d => d.LanguageId == language && d.NewEnglishCount > 0).Select(d => new DTO.ResponseDto.NotificationResponse
                        {
                            ApplicationId = d.ApplicationId,
                            SectionId = d.SectionId,
                            LanguageId = language,
                            NotificationCount = d.NewEnglishCount
                        });

                        response.AddRange(sections);
                    }
                    else
                    {
                        /* Common Sections: Sections where translation(s) for both english and current non-english translation is present.
                         * Uncommon Sections: Sections where translation(s) for english english is present but 
                         * does not have any value for the current non-english translation */
                        var commonSections = dataEnglish.Join(dataNotEnglish.Where(noteng => noteng.LanguageId == language),
                                                    eng => new { eng.ApplicationId, eng.SectionId }, noteng => new { noteng.ApplicationId, noteng.SectionId },
                                                    (eng, noteng) => new { eng, noteng })
                                                    .Select(grpData => new DTO.ResponseDto.NotificationResponse
                                                    {
                                                        ApplicationId = grpData.eng.ApplicationId,
                                                        SectionId = grpData.eng.SectionId,
                                                        LanguageId = language,
                                                        NotificationCount = grpData.eng.TranslationsCount - grpData.noteng.TranslationsCount
                                                    }).ToList();
                        var sectionsCovered = commonSections.Select(d => d.SectionId)/*.Distinct()*//*.ToList()*/;
                        var uncommonSections = dataEnglish.Where(dt => !sectionsCovered.Contains(dt.SectionId)).Select(dt => new DTO.ResponseDto.NotificationResponse
                        {
                            ApplicationId = dt.ApplicationId,
                            SectionId = dt.SectionId,
                            LanguageId = language,
                            NotificationCount = dt.TranslationsCount
                        });

                        response.AddRange(commonSections);
                        response.AddRange(uncommonSections);
                    }
                });
            }

            return Ok(new { Notifications = response });
        }

        // GET: api/Applications
        [HttpGet]
        public List<Application> GetApplication()
        {
            var data = _applicationRepository.Where(p => p.IsVisible == true && p.Section.Any(a => a.IsVisible)).Include("Pages").ToList();
            return data;
        }

        [HttpGet]
        [Route("GetTranslations")]
        public async Task<IActionResult> GetTranslations(int sectionId, int languageId, int userId)
        {
            int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
            var translations = _textRepository.Where(t => t.SectionId == sectionId)
                                        .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         text => text.Id, trans => trans.TextId,
                                         (text, trans) => new { Text = text, Translation = trans })
                                        .GroupBy(data => data.Text.Id)
                                        .OrderBy(group => group.Min(data => data.Text.Sno))
                                        .Select(data => new
                                        {
                                            TextId = data.Key,
                                            Key = data.Max(x => x.Text.Key),
                                            ReleaseId = data.Any() && data.FirstOrDefault().Text != null ? data.FirstOrDefault().Text.VersionId : 0,
                                            Translations = data.Select(x => new
                                            {
                                                TranslationId = x.Translation.Id,
                                                x.Translation.StatusId,
                                                x.Translation.LanguageId,
                                                x.Translation.Translation,
                                                x.Translation.PreviousTranslation,
                                                x.Translation.IsVisible,
                                                PreviousEnglish = x.Translation.LanguageId != 1 ? data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null,
                                                x.Text.Sno
                                            })
                                        }).ToList();

            var application = await _sectionManager.GetApplicationAsync(sectionId);

            return Ok(new { Translations = translations, Application = application });
        }

        [HttpGet]
        [Route("GetGroupTranslations")]
        public async Task<IActionResult> GetGroupTranslations(int groupId, int languageId, int userId)
        {
            if (groupId == 0)
            {
                return NoContent();
            }
            try
            {
                int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
                var translations = _textRepository.Where(t => t.GroupId == groupId)
                                            .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                             text => text.Id, trans => trans.TextId,
                                             (text, trans) => new { Text = text, Translation = trans })
                                            .GroupBy(data => data.Text.Id)
                                            .OrderBy(group => group.Min(data => data.Text.Sno))
                                            .Select(data => new
                                            {
                                                TextId = data.Key,
                                                Key = data.Max(x => x.Text.Key),
                                                ReleaseId = data.Any() && data.FirstOrDefault().Text != null ? data.FirstOrDefault().Text.VersionId : 0,
                                                Translations = data.Select(x => new
                                                {
                                                    TranslationId = x.Translation.Id,
                                                    x.Translation.StatusId,
                                                    x.Translation.LanguageId,
                                                    x.Translation.Translation,
                                                    x.Translation.PreviousTranslation,
                                                    x.Translation.IsVisible,
                                                    PreviousEnglish = x.Translation.LanguageId != 1 ? data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null,
                                                    x.Text.Sno
                                                })
                                            }).ToList();

                var application = await _groupManager.GetApplicationAsync(groupId);

                return Ok(new { Translations = translations, Application = application });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("GetAllGroupsTranslation")]
        public IActionResult GetAllGroupsTranslation(int languageId, string groupIds, int userId)
        {
            List<int> groupList = groupIds.Split(',').Select(int.Parse).ToList();
            int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
            if (roleId == 2)
            {
                var translations = _textRepository.Where(t => groupList.Contains(t.SectionId))
                                        .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         text => text.Id, trans => trans.TextId,
                                         (text, trans) => new { Text = text, Translation = trans })
                                        .GroupBy(data => data.Text.Id)
                                        .OrderBy(t => t.Min(d => d.Text.SectionId)).ThenBy(group => group.Min(data => data.Text.Sno))
                                        .Select(data => new
                                        {
                                            TextId = data.Key,
                                            Key = data.Max(x => x.Text.Key),
                                            Translations = data.Select(x => new
                                            {
                                                TranslationId = x.Translation.Id,
                                                x.Translation.StatusId,
                                                x.Translation.LanguageId,
                                                x.Translation.Translation,
                                                x.Translation.PreviousTranslation,
                                                PreviousEnglish = x.Translation.LanguageId != 1 ? data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null,
                                                x.Text.Sno
                                            })
                                        })
                                        .ToList();
                return Ok(new { Translations = translations });
            }

            return NotFound();
        }

        [HttpGet]
        [Route("GetAllSectionsTranslation")]
        public IActionResult GetAllSectionsTranslation(int languageId, string sectionIds, int userId)
        {
            List<int> sectionList = sectionIds.Split(',').Select(int.Parse).ToList();
            int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
            if (roleId == 1)
            {
                var translations = _textRepository.Where(t => sectionList.Contains(t.SectionId))
                                        .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                         text => text.Id, trans => trans.TextId,
                                         (text, trans) => new { Text = text, Translation = trans })
                                        .GroupBy(data => data.Text.Id)
                                        .OrderBy(t => t.Min(d => d.Text.SectionId)).ThenBy(group => group.Min(data => data.Text.Sno))
                                        .Select(data => new
                                        {
                                            TextId = data.Key,
                                            Key = data.Max(x => x.Text.Key),
                                            Translations = data.Select(x => new
                                            {
                                                TranslationId = x.Translation.Id,
                                                x.Translation.StatusId,
                                                x.Translation.LanguageId,
                                                x.Translation.Translation,
                                                x.Translation.PreviousTranslation,
                                                PreviousEnglish = x.Translation.LanguageId != 1 ? data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null,
                                                x.Text.Sno
                                            })
                                        })
                                        .ToList();
                return Ok(new { Translations = translations });
            }

            return NotFound();
        }

        [HttpGet]
        [Route("PublishToEnv/{env}")]
        public async Task<IActionResult> PublishToEnv([FromQuery] PublishToEnv env, int languageId, string sectionIds, int applicationId, string languageCode, int userId, string groupIds)
        {
            if (!String.IsNullOrEmpty(sectionIds))
            {
                List<int> sectionList = sectionIds.Split(',').Select(int.Parse).ToList();
                int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
                if (roleId == 1 || roleId == 2)
                {
                    var translations = _textRepository.Where(t => sectionList.Contains(t.SectionId))
                                            .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                             text => text.Id, trans => trans.TextId,
                                             (text, trans) => new { Text = text, Translation = trans })
                                            .GroupBy(data => data.Text.Key)
                                            .OrderBy(t => t.Min(d => d.Text.SectionId)).ThenBy(group => group.Min(data => data.Text.Sno))
                                            .Select(data => new
                                            {
                                                Key = data.Max(x => x.Text.Key),
                                                Value = data.Where(x => x.Translation.LanguageId == languageId).Select(x => x.Translation.Translation).FirstOrDefault(),
                                                EnglishValue = data.Where(x => x.Translation.LanguageId == 1).Select(x => x.Translation.Translation).FirstOrDefault()
                                            })
                                            .ToList();
                    var translationDict = translations.ToDictionary(x => x.Key, x => string.IsNullOrWhiteSpace(x.Value) ? x.EnglishValue : x.Value);
                    //Create the Json
                    string json = JsonConvert.SerializeObject(translationDict, Formatting.Indented);
                    //Convert to Stream
                    byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    Stream stream = new MemoryStream(byteArray);

                    //Save                
                    if (env == Enums.PublishToEnv.Test)
                    {
                        await _storageManager.CreateOrReplaceBlob(Constants.LocalizationFiles + "\\" + applicationId + "\\" + languageCode + ".json", stream, "gl-localizer-json");
                    }
                    else if(env == Enums.PublishToEnv.Prod)
                    {
                        await _storageManager.CreateOrReplaceBlob(Constants.ProdLocalizationFiles + "\\" + applicationId + "\\" + languageCode + ".json", stream, "gl-localizer-json");
                    }

                }

                return Ok();
            }
            else if (!String.IsNullOrEmpty(groupIds))
            {
                List<int> groupList = groupIds.Split(',').Select(int.Parse).ToList();
                int roleId = _userRoleRepository.Where(ur => ur.UserId == userId).FirstOrDefault().RoleId;
                if (roleId == 1 || roleId == 2)
                {
                    var translations = _textRepository.Where(t => groupList.Contains(t.GroupId ?? 0))
                                            .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId || t.LanguageId == 1),
                                             text => text.Id, trans => trans.TextId,
                                             (text, trans) => new { Text = text, Translation = trans })
                                            .GroupBy(data => data.Text.Key)
                                            .OrderBy(t => t.Min(d => d.Text.GroupId)).ThenBy(group => group.Min(data => data.Text.Sno))
                                            .Select(data => new
                                            {
                                                Key = data.Max(x => x.Text.Key),
                                                Value = data.Where(x => x.Translation.LanguageId == languageId).Select(x => x.Translation.Translation).FirstOrDefault(),
                                                EnglishValue = data.Where(x => x.Translation.LanguageId == 1).Select(x => x.Translation.Translation).FirstOrDefault()
                                            })
                                            .ToList();
                    var translationDict = translations.ToDictionary(x => x.Key, x => string.IsNullOrWhiteSpace(x.Value) ? x.EnglishValue : x.Value);
                    //Create the Json
                    string json = JsonConvert.SerializeObject(translationDict, Formatting.Indented);
                    //Convert to Stream
                    byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    Stream stream = new MemoryStream(byteArray);
                    //Save                
                    await _storageManager.CreateOrReplaceBlob(Constants.LocalizationFiles + "\\" + applicationId + "\\" + languageCode + ".json", stream, "gl-localizer-json");
                }

                return Ok();
            }

            return NotFound();

        }

        [HttpGet]
        [Route("PublishTranslations")]
        public async Task<IActionResult> PublishTranslations([FromQuery] int languageId, [FromQuery] int applicationId, [FromQuery] int releaseId)
        {
            List<TextTranslation> textTranslation = new List<TextTranslation>();
            if (releaseId == 0)
            {
                textTranslation = await _textTranslationRepository
                                  .Where(item => item.StatusId == (int)Enums.Status.Updated && item.LanguageId == languageId
                                      && item.Text.Section.ApplicationId == applicationId).ToListAsync();
            }
            else
            {
                textTranslation = await _textTranslationRepository
                                   .Where(item => item.StatusId == (int)Enums.Status.Updated && item.LanguageId == languageId
                                       && item.Text.Section.ApplicationId == applicationId && item.Text.VersionId == releaseId).ToListAsync();
            }


            textTranslation.ForEach(t =>
            {
                t.StatusId = (int)Enums.Status.Published;
            });

             await _textTranslationRepository.EditMultipleAsync(textTranslation, nameof(TextTranslation.StatusId));
            return Ok();
        }

        [HttpPost]
        [Route("SaveTranslationGroup")]
        public async Task<IActionResult> SaveTranslationGroup([FromBody] TextEditDTO data)
        {
            var text = _textRepository.FirstOrDefault(t => (int)t.Id == (int)data.TextId);
            if (text != null)
            {
                text.VersionId = data.ReleaseId;
            }
            await _textRepository.EditAsync(text);
            var textTranslation = _textTranslationRepository.FirstOrDefault(translation => translation.TextId == data.TextId && translation.LanguageId == data.LanguageId);

            textTranslation.Translation = data.Translation;
            textTranslation.StatusId = (int)Status.Updated;

            await _textTranslationRepository.EditAsync(textTranslation);
            return Ok();
        }

        [HttpPost]
        [Route("SaveTranslation")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> SaveTranslation([FromBody] TextTranslationDTO data)
        {
            var text = _textRepository.FirstOrDefault(t => (int)t.Id == (int)data.TextId);
            if (text != null)
            {
                text.VersionId = data.ReleaseId;
            }
            await _textRepository.EditAsync(text);

            var entityList = _textTranslationRepository.Where(x => (int)x.TextId == (int)data.TextId).ToList();

            if (data.TranslationId > 0)
            {
                ///if translation length is 0 and translation id is greater than zero we will delete the current translation.
                if (data.Translation.Length == 0)
                {
                    TextTranslation textTranslation = _textTranslationRepository
                            .Where(txt => txt.Id == data.TranslationId && txt.LanguageId != 1)
                            .FirstOrDefault();

                    await _textTranslationRepository.DeleteAsync(textTranslation);
                    return Ok();
                }

                entityList.ForEach(entity =>
                {
                    if (data.LanguageId == 1)
                    {
                        entity.StatusId = (int)Enums.Status.Updated;
                    }
                    if (entity.Id == (int)data.TranslationId)
                    {
                        entity.PreviousTranslation = entity.Translation;
                        entity.Translation = data.Translation;
                        entity.StatusId = (int)Enums.Status.Updated;
                    }

                });
                await _textTranslationRepository.EditMultipleAsync(entityList);
                return Ok();
            }

            else
            {
                //if tranlationId is null and translation length is greater than zero we will add a translation.
                if (data.Translation.Length != 0)
                {
                    TextTranslation t = new TextTranslation()
                    {
                        TextId = data.TextId,
                        StatusId = (int)Enums.Status.Updated,
                        Translation = data.Translation,
                        LanguageId = data.LanguageId
                    };

                    await _textTranslationRepository.AddAsync(t);
                    return Ok(new
                    {
                        t.Id
                    });
                }
                ///if translation length is not greater than zero we will just update the releaseId.
                else
                {
                    return Ok();
                }
            }
        }

        [HttpPost]
        [Route("AddTranslation")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> AddTranslation([FromBody] TextTranslationDTO data)
        {
            bool isSectionVisible = false;
            SectionApiModel sectionModel = await _sectionManager.GetSectionAsync(data.SectionId, s => s.LanguageSectionIgnore);
            bool sequenceContainsElements = _textRepository.Where(txt => txt.SectionId == data.SectionId).Any();
            var texts = _textRepository.Where(txt => txt.SectionId == data.SectionId).Include(txt => txt.Section.LanguageSectionIgnore);

            // If section is ignored/un-ignored, then new translation should also be ignored/un-ignored accordingly.
            if (texts.Any())
            {
                isSectionVisible = !texts.First().Section.LanguageSectionIgnore.Any(item => item.LanguageId == data.LanguageId);
            }
            else
            {
                isSectionVisible = !sectionModel.LanguageSectionIgnores.Any(item => item.LanguageId == data.LanguageId);
            }

            int sno = sequenceContainsElements ? texts.Max(txt => txt.Sno) + 1 : 1;

            Text text = new Text()
            {
                Key = data.Key,
                SectionId = data.SectionId,
                TextTranslation = new List<TextTranslation>
                {
                    new TextTranslation
                    {
                        LanguageId = data.LanguageId,
                        StatusId = data.StatusId,
                        Translation = data.Translation,
                        IsVisible = isSectionVisible
                    }
                },
                Sno = sno,
                VersionId = data.ReleaseId
            };

            // Add new translations with isVisble = false for ignored sections for other languages.
            if (data.LanguageId == 1)
            {

                var languageIdList = sectionModel.LanguageSectionIgnores.Where(x => x.SectionId == data.SectionId && x.LanguageId != 1).Select(x => x.LanguageId).ToList();
                languageIdList.ForEach(languageId =>
                {
                    text.TextTranslation.Add(new TextTranslation
                    {
                        LanguageId = languageId,
                        StatusId = 1,
                        Translation = string.Empty,
                        IsVisible = false
                    });
                });
            }
            await _textRepository.AddAsync(text);
            return Ok(new { text.Sno, text.Id, TranslationId = text.TextTranslation.FirstOrDefault().Id });
        }

        [HttpGet]
        [Route("DeleteTranslation")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> DeleteTranslation(int textId)
        {
            Text text = _textRepository
                            .Where(txt => txt.Id == textId)
                            .Include(t => t.TextTranslation)
                            .FirstOrDefault();

            await _textTranslationRepository.DeleteMultipleAsync(text.TextTranslation);
            await _textRepository.DeleteAsync(text);
            return Ok();
        }

        [HttpDelete]
        [Route("DeleteTextTranslation")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> DeleteTextTranslation(int translationId)
        {
            TextTranslation textTranslation = _textTranslationRepository
                            .Where(txt => txt.Id == translationId && txt.LanguageId != 1)
                            .FirstOrDefault();

            await _textTranslationRepository.DeleteAsync(textTranslation);
            return Ok();
        }

        // GET: api/Applications/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApplication([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var application = await _applicationRepository.GetByIdAsync(id);

            if (application == null)
            {
                return NotFound();
            }

            return Ok(application);
        }

        // PUT: api/Applications/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApplication([FromRoute] int id, [FromBody] Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != application.Id)
            {
                return BadRequest();
            }

            try
            {
                await _applicationRepository.EditAsync(application);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await ApplicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Applications
        [HttpPost]
        public async Task<IActionResult> PostApplication([FromBody] Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _applicationRepository.AddAsync(application);
            return CreatedAtAction("GetApplication", new { id = application.Id }, application);
        }

        // DELETE: api/Applications/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApplication([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var application = await _applicationRepository.GetByIdAsync(id);

            if (application == null)
            {
                return NotFound();
            }

            await _applicationRepository.DeleteAsync(application);

            return Ok(application);
        }

        [HttpGet]
        [Route("Images")]
        public async Task<IActionResult> GetStoredImageAsync(int groupId)
        {
            return Ok(await _storageService.GetFileFromStorageAsync(groupId));
        }

        private Task<bool> ApplicationExists(int id)
        {
            return _applicationRepository.AnyAsync(e => e.Id == id);
        }

        [HttpPost]
        [Route("ToggleLock")]
        public async Task<IActionResult> ToggleApplicationLockAsync([FromBody] ApplicationLockDTO appLockDto)
        {
            long code = await _translationManager.ToggleApplicationLockAsync(appLockDto.Id, appLockDto.IsLocked);
            return Ok(code);
        }

        /// <summary>
        /// To check application lock
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("IsApplicationLocked")]
        public async Task<IActionResult> IsApplicationLocked(int appId)
        {
            bool isLocked = await _applicationRepository.AnyAsync(app => app.Id == appId && app.IsLocked);
            return Ok(new { IsLocked = isLocked });
        }

        [HttpGet]
        [Route("SearchTextTranslation")]
        public async Task<IActionResult> SearchTextTranslation(string keyword, string sectionIds, int languageId, int roleId, string groupIds)
        {
            if (string.IsNullOrWhiteSpace(keyword) || string.IsNullOrEmpty(sectionIds) || languageId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(keyword)}={keyword}, {nameof(sectionIds)}={sectionIds}, {nameof(groupIds)}={groupIds}, {nameof(languageId)}={languageId}");
            }

            keyword = keyword.ToLower();
            List<TextTranslationGroupDTO> filteredTranslations = null;
            List<Image> imageList = null;
            List<int> sectionList = sectionIds.Split(',').Select(int.Parse).ToList();
            var translations = _textRepository.Where(t => sectionList.Contains(t.SectionId))
                                    .Join(_textTranslationRepository.Where(t => (t.LanguageId == languageId || t.LanguageId == 1)),
                                     text => text.Id, trans => trans.TextId,
                                     (text, trans) => new TextTranslationGroupDTO { Text = text, Translation = trans })
                                    .ToList();

            if (roleId == (int)Enums.Role.Developer)
            {
                filteredTranslations = translations.Where(item => item.Text.Key.ToLower().Contains(keyword) || item.Translation.Translation.ToLower().Contains(keyword)).ToList();
            }
            else
            {
                filteredTranslations = translations.Where(item => item.Translation.Translation.ToLower().Contains(keyword)).ToList();
            }

            var groupedTranslations = filteredTranslations.GroupBy(data => data.Text.Id).ToList();
            List<TextTranslationGroupDTO> newTranslations = new List<TextTranslationGroupDTO>();

            if (languageId != 1)
            {
                groupedTranslations.ForEach(currentTranslation =>
                {
                    if (currentTranslation.Count() == 1)
                    {
                        if (currentTranslation.First().Translation.LanguageId != 1)
                        {
                            var textTranslation = translations.Find(item => item.Translation.TextId == currentTranslation.First().Translation.TextId
                            && item.Translation.LanguageId == 1);
                            if (textTranslation != null)
                            {
                                newTranslations.Add(textTranslation);
                            }
                        }
                        else
                        {
                            var textTranslation = translations.Find(item => item.Translation.TextId == currentTranslation.First().Translation.TextId
                           && item.Translation.LanguageId == languageId);
                            if (textTranslation != null)
                            {
                                newTranslations.Add(textTranslation);
                            }
                        }
                    }
                });
            }

            foreach (TextTranslationGroupDTO translation in newTranslations)
            {
                filteredTranslations.Add(translation);
            }

            var translationResult = filteredTranslations
                .GroupBy(data => data.Text.Id)
                .OrderBy(t => t.Min(d => d.Text.SectionId)).ThenBy(group => group.Min(data => data.Text.Sno))
                .Select(data => new
                {
                    TextId = data.Key,
                    Key = data.Max(x => x.Text.Key),
                    SectionId = data.Max(x => x.Text.SectionId),
                    Translations = data.Select(x => new
                    {
                        TranslationId = x.Translation.Id,
                        x.Translation.StatusId,
                        x.Translation.IsVisible,
                        x.Translation.LanguageId,
                        x.Translation.Translation,
                        x.Translation.PreviousTranslation,
                        PreviousEnglish = x.Translation.LanguageId != 1 ?
                        (data.FirstOrDefault(item => item.Translation.LanguageId == 1) != null ?
                        data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null) : null,
                        x.Text.Sno
                    })
                }).ToList();

            return Ok(new { Translations = translationResult, ImageList = imageList });
        }

        [HttpPost] 
        [Route("SearchTextTranslationV2")]

        public  Task<IActionResult> SearchTextTranslationV2([FromBody] SearchTextTranslationsDTO model)
        {
            return SearchTextTranslation(model.Keyword, model.SectionIds, model.LanguageId, model.RoleId, model.GroupIds);
        }

        [HttpGet]
        [Route("SearchTextTranslationForGroup")]
        public async Task<IActionResult> SearchTextTranslationForGroup(string keyword, string groupIds, int languageId, int roleId)
        {
            if (string.IsNullOrWhiteSpace(keyword) || string.IsNullOrEmpty(groupIds) || languageId == 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(keyword)}={keyword}, {nameof(groupIds)}={groupIds}, {nameof(groupIds)}={groupIds}, {nameof(languageId)}={languageId}");
            }

            keyword = keyword.ToLower();
            List<TextTranslationGroupDTO> filteredTranslations = null;
            List<GroupImage> imageList = null;
            List<int> groupList = groupIds.Split(',').Select(int.Parse).ToList();
            if (groupList.Any(id => id == 0))
            {
                throw new ArgumentIsInvalidException($"{nameof(groupIds)}={groupIds}");
            }

            var translations = _textRepository.Where(t => groupList.Contains(t.GroupId ?? 0))
                                    .Join(_textTranslationRepository.Where(t => (t.LanguageId == languageId || t.LanguageId == 1)),
                                     text => text.Id, trans => trans.TextId,
                                     (text, trans) => new TextTranslationGroupDTO { Text = text, Translation = trans })
                                    .ToList();

            if (roleId == (int)Enums.Role.Developer)
            {
                filteredTranslations = translations.Where(item => item.Text.Key.ToLower().Contains(keyword) || item.Translation.Translation.ToLower().Contains(keyword)).ToList();
            }
            else
            {
                filteredTranslations = translations.Where(item => item.Translation.Translation.ToLower().Contains(keyword)).ToList();
            }

            var groupedTranslations = filteredTranslations.GroupBy(data => data.Text.Id).ToList();
            List<TextTranslationGroupDTO> newTranslations = new List<TextTranslationGroupDTO>();

            if (languageId != 1)
            {
                groupedTranslations.ForEach(currentTranslation =>
                {
                    if (currentTranslation.Count() == 1)
                    {
                        if (currentTranslation.First().Translation.LanguageId != 1)
                        {
                            var textTranslation = translations.Find(item => item.Translation.TextId == currentTranslation.First().Translation.TextId
                            && item.Translation.LanguageId == 1);
                            if (textTranslation != null)
                            {
                                newTranslations.Add(textTranslation);
                            }
                        }
                        else
                        {
                            var textTranslation = translations.Find(item => item.Translation.TextId == currentTranslation.First().Translation.TextId
                           && item.Translation.LanguageId == languageId);
                            if (textTranslation != null)
                            {
                                newTranslations.Add(textTranslation);
                            }
                        }
                    }
                });
            }

            foreach (TextTranslationGroupDTO translation in newTranslations)
            {
                filteredTranslations.Add(translation);
            }

            var translationResult = filteredTranslations
                .GroupBy(data => data.Text.Id)
                .OrderBy(t => t.Min(d => d.Text.GroupId)).ThenBy(group => group.Min(data => data.Text.Sno))
                .Select(data => new
                {
                    TextId = data.Key,
                    Key = data.Max(x => x.Text.Key),
                    GroupId = data.Max(x => x.Text.GroupId),
                    Translations = data.Select(x => new
                    {
                        TranslationId = x.Translation.Id,
                        x.Translation.StatusId,
                        x.Translation.IsVisible,
                        x.Translation.LanguageId,
                        x.Translation.Translation,
                        x.Translation.PreviousTranslation,
                        PreviousEnglish = x.Translation.LanguageId != 1 ?
                        (data.FirstOrDefault(item => item.Translation.LanguageId == 1) != null ?
                        data.FirstOrDefault(item => item.Translation.LanguageId == 1).Translation.PreviousTranslation : null) : null,
                        x.Text.Sno
                    })
                }).ToList();

            if (groupList != null && groupList.Count > 0)
            {
                imageList = await _storageService.GetMultipleImagesFromStorageAsync(groupList);
            }

            return Ok(new { Translations = translationResult, ImageList = imageList });
        }

        [HttpPost]
        [Route("SearchTextTranslationForGroupV2")]

        public Task<IActionResult> SearchTextTranslationForGroupV2([FromBody] SearchTextTranslationsDTO model)
        {
            return SearchTextTranslationForGroup(model.Keyword, model.GroupIds, model.LanguageId, model.RoleId);
        }

        [HttpPost("ToggleIgnore")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> ToggleIgnore([FromBody] IgnoreApplicationDto ignoreDto)
        {
            await _translationManager.ToggleIgnoreAsync(ignoreDto.ToApiModel());
            return Ok();
        }

        [HttpPost]
        [Route("AddNoteToSection")]
        public async Task AddNoteToSection([FromBody] NoteDTO note)
        {
            await _sectionManager.AddNoteToSection(note.SectionId, note.Note);
        }

        [HttpPost]
        [Route("AddNoteToGroup")]
        public async Task AddNoteToGroup([FromBody] GroupNoteDTO note)
        {
            await _groupManager.AddNoteToGroup(note.GroupId, note.Note);
        }

        [HttpGet]
        [Route("{languageId}/TextTranslation/{key}")]
        public IActionResult GetTextTranslation(int languageId, string key)
        {
            var data = _textRepository.Where(t => t.Key == key && t.Section.Application.IsVisible)
                                        .Join(_textTranslationRepository.Where(t => t.LanguageId == languageId),
                                        tr => tr.Id, ttr => ttr.TextId,
                                        (tr, ttr) => new { Text = tr, Translation = ttr }).
                                        Select(d => new
                                        {
                                            Id = d.Translation.Id,
                                            LanguageId = d.Translation.LanguageId,
                                            Key = d.Text.Key,
                                            Translation = d.Translation.Translation
                                        }).FirstOrDefault();

            return Ok(new { Translations = data });
        }

        [HttpGet]
        [Route("AppUngroupedKeysCount")]
        public async Task<IActionResult> GetUngroupedKeysCount()
        {
            var ungroupedKeys = await _applicationManager.GetUngroupedKeysCount();

            return Ok(ungroupedKeys);
        }

        [HttpGet]
        [Route("GetVersionDetails")]
        public IActionResult GetVersionDetails()
        {
            var versions = _versionRepository.GetAll();
            return Ok(versions);
        }

        [HttpPost]
        [Route("SaveVersion")]

        public async Task<IActionResult> SaveVersion([FromBody] VersionDTO data)
        {
            var pdf = _pdfRepository.FirstOrDefault(t => t.Id == data.Id);
            if (pdf != null)
            {
                pdf.VersionId = data.ReleaseId;
            }
            await _pdfRepository.EditAsync(pdf);
            return Ok();
        }

    }
}