﻿using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static GrapeSEED.LocalizationService.Foundation.Common.Enums;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IGeneralRepository<User> _userRepository;
        private readonly IUserManager _userManager;

        public UserController(IConfiguration config, IGeneralRepository<User> userRepository, IUserManager userManager)
        {
            _config = config;
            _userRepository = userRepository;
            _userManager = userManager;
        }

        /// <summary>
        /// Activates a user account to use the localizer application.
        /// </summary>
        /// <param name="invitationCode">Invitation code</param>
        /// <param name="activateAccountDTO">Information containing user's new password</param>
        /// <returns>async Task<IActionResult></returns>
        [HttpPut]
        [AllowAnonymous]
        [Route("Activate/{invitationCode}")]
        public async Task<IActionResult> ActivateAccount(string invitationCode,[FromBody]ActivateAccountDTO activateAccountDTO)
        {
            UpdatePasswordStatus statusCode;

            if (ModelState.IsValid)
            {
                var result = await _userManager.ActivateAccount(invitationCode, activateAccountDTO.NewPassword);

                if (result)
                {
                    statusCode = UpdatePasswordStatus.Success;
                }
                else
                {
                    statusCode = UpdatePasswordStatus.InvalidParameters;
                }
            }
            else
            {
                statusCode = UpdatePasswordStatus.InvalidParameters;
            }

            return Ok(new { StatusCode = statusCode });
        }
    }
}