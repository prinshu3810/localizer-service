﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.DTO.RequestDto.Applications;
using GrapeSEED.LocalizationService.Foundation.Common;
using GrapeSEED.LocalizationService.Infrastructure.Filters;
using GrapeSEED.LocalizationService.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class AdminController : BaseController
    {
        private readonly ILocalizationStorageManager _storageManager;
        private readonly ITranslationManager _translationManager;
        private readonly IGeneralRepository<Application> _applicationRepository;
        private readonly IApplicationManager _applicationManager;
        private readonly IUserManager _userManager;
        private readonly ILanguageManager _languageManager;
        private readonly IRoleManager _roleManager;
        private readonly IGroupManager _groupManager;
        private readonly ISectionManager _sectionManager;
        private readonly IGeneralRepository<Text> _textRepository;
        private readonly IGeneralRepository<Group> _groupRepository;

        public AdminController(
            ILocalizationStorageManager storageManager,
            ITranslationManager translationManager,
            IGeneralRepository<Application> applicationRepository,
            IApplicationManager applicationManager,
            IUserManager userManager,
            ILanguageManager languageManager,
            IRoleManager roleManager,
            IGroupManager groupManager,
            ISectionManager sectionManager,
            IGeneralRepository<Text> textRepository,
            IGeneralRepository<Group> groupRepository
            )
        {
            _storageManager = storageManager;
            _translationManager = translationManager;
            _applicationRepository = applicationRepository;
            _applicationManager = applicationManager;
            _userManager = userManager;
            _languageManager = languageManager;
            _roleManager = roleManager;
            _groupManager = groupManager;
            _sectionManager = sectionManager;
            _textRepository = textRepository;
            _groupRepository = groupRepository;
        }

        [HttpPost]
        [Route("Upload")]
        public async Task<IActionResult> UploadAsync()
        {
            IFormFile file = Request.Form?.Files?.FirstOrDefault();
            Request?.Form?.TryGetValue("groupId", out var value);
            int groupId = Convert.ToInt32(value);

            if (file == null || groupId == 0)
            {
                return null;
            }

            string fileName = file.FileName;
            string accessUrl = null;
            string path = Constants.UPLOADED_IMAGES + "/" + groupId + "/" + file.FileName;
            using (Stream stream = file.OpenReadStream())
            {
                accessUrl = await _storageManager.UploadFileToStorageAsync(groupId, path, stream);
            }

            int imageId = await _translationManager.SaveUploadedImage(groupId, path);
            return Ok(new { AccessUrl = accessUrl, ImageId = imageId });
        }

        [HttpDelete]
        [Route("DeleteImage")]
        public async Task<IActionResult> DeleteImageAsync(int groupId, int imageId, string fileName)
        {
            try
            {
                string path = Constants.UPLOADED_IMAGES + "/" + groupId + "/" + fileName;
                await _storageManager.DeleteFileFromStorageAsync(Convert.ToInt32(imageId), path);
                return Ok();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("DeleteApp")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> DeleteApp(int id)
        {
            var entity = _applicationRepository.Where(app => app.Id == id)?.Include(app => app.Section)?.FirstOrDefault();

            if (entity != null)
            {
                entity.IsVisible = false;
                entity?.Section.ToList().ForEach(page => page.IsVisible = false);
            }

            await _applicationRepository.EditAsync(entity);
            return Ok();
        }

        [HttpPost]
        [Route("AddApplicationAndSections")]
        public async Task<IActionResult> AddApplicationAndSections([FromBody] ApplicationDTO site)
        {
            try
            {
                var temp = new Application()
                {
                    Name = site.Name,
                    IsVisible = site.IsVisible,
                    Section = site.Sections,
                    IsLocked = site.IsLocked,
                    TranslationTypeId = site.TranslationTypeId
                };

                await _applicationRepository.AddAsync(temp);

                //create a list of groups as many as sections.
                List<Group> groups = new List<Group>();
                // to keep the sno of groups
                int sno = 1;
                foreach (var section in site.Sections)
                {
                    groups.Add(new Group()
                    {
                        ApplicationId = temp.Id,
                        Name = section.Name,
                        IsVisible = true,
                        Sno = sno,
                        Note = null
                    });

                    sno++;
                }

                await _groupRepository.AddMultipleAsync(groups);

                //take out the texts from sections and update the groupId matching section name and group name 
                List<Text> texts = new List<Text>();

                List<Text> textsToEdit = new List<Text>();

                foreach (var section in site.Sections)
                {
                    texts = section.Text.ToList();

                    foreach (var group in groups)
                    {
                        if (group.Name == section.Name)
                        {
                            texts.ForEach(text => text.GroupId = group.Id);
                        }
                    }

                    textsToEdit.AddRange(texts);
                }

                // update the texts
                await _textRepository.EditMultipleAsync(textsToEdit);

                return Ok(new
                {
                    temp.Id,
                    temp.Name,
                    Section = temp.Section.Select(section => new
                    {
                        section.Id,
                        section.Name,
                        section.DisplayName
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Edits the application as per the information recieved in transfer object.
        /// </summary>
        /// <param name="editAppDto">Information regarding the application to edit with its new sections, sections to delete and sections to uplpoad.</param>
        /// <returns>Action result.</returns>
        [HttpPost]
        [Route("EditApplication")]
        [TypeFilter(typeof(CheckApplicationLock))]
        public async Task<IActionResult> EditApplication([FromBody] EditApplicationDto editAppDto)
        {
            if (editAppDto.Id <= 0)
            {
                throw new ArgumentIsInvalidException($"{nameof(editAppDto.Id)} is {editAppDto.Id}");
            }

            await _applicationManager.EditApplicationAsync(editAppDto.ToApiModel());
            return Ok();
        }


        [HttpGet]
        [Route("Users")]
        public async Task<IActionResult> GetUsers()
        {
            return Ok(await _userManager.GetUsers());
        }

        [HttpGet]
        [Route("Languages")]
        public async Task<IActionResult> GetLanguages()
        {
            return Ok(await _languageManager.GetLanguages());
        }

        [HttpGet]
        [Route("Roles")]
        public async Task<IActionResult> GetRoles()
        {
            return Ok(await _roleManager.GetRoles());
        }

        /// <summary>
        /// Save the user details (languages and roles).
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <param name="languageRoleDTO">Information containing language ids and role ids of the user</param>
        /// <returns></returns>
        [HttpPut]
        [Route("save/{id}")]
        public async Task<IActionResult> Save(int id, [FromBody] LanguageRoleDTO languageRoleDTO)
        {
            if (ModelState.IsValid)
            {
                return Ok(await _userManager.Save(id, languageRoleDTO.languageIds, languageRoleDTO.roleIds));
            }
            else
            {
                return Ok(false);
            }

        }

        /// <summary>
        /// Sends an invite link to the user based on the email id received and saves the detail in user table.
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <param name="languageRoleDTO">Information containing language ids and role ids of the user</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Invite/{email}")]
        public async Task<IActionResult> InviteUser(string email, [FromBody] InviteUserDTO inviteUserDTO)
        {
            if (ModelState.IsValid)
            {
                return Ok(await _userManager.InviteUser(email, inviteUserDTO.languageIds, inviteUserDTO.roleIds, inviteUserDTO.Name));
            }
            else
            {
                return Ok(false);
            }

        }

        /// <summary>
        /// Sets the Disabled property of the user to true or false.
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>ActionResult</returns>
        [HttpPut]
        [Route("ToggleUserDisabled/{id}")]
        public async Task<IActionResult> ToggleUserDisabled(int id, [FromBody] bool disabled)
        {
            return Ok(await _userManager.ToggleDisabled(id, disabled));
        }

        [HttpPost]
        [Route("{applicationId}/ChangeAllowPublish")]
        public async Task<IActionResult> ChangeAllowPublish([FromRoute] int applicationId, [FromQuery] bool allowPublish)
        {
            await _applicationManager.ChangeAllowPublish(applicationId, allowPublish);
            return Ok();
        }
    }
}