﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Contracts;
using GrapeSEED.LocalizationService.DTO.RequestDto;
using GrapeSEED.LocalizationService.Repository.Shared.Contracts;
using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GrapeSEED.LocalizationService.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class GroupController : BaseController
    {
        private readonly IGroupManager _groupManager;
        private readonly ISectionManager _sectionManager;
        private readonly IGeneralRepository<Group> _groupRepository;

        public GroupController(
            IGroupManager groupManager,
            ISectionManager sectionManager,
            IGeneralRepository<Group> groupRepository
        )
        {
            _groupManager = groupManager;
            _sectionManager = sectionManager;
            _groupRepository = groupRepository;
        }

        [HttpGet]
        [Route("Groups/{applicationId}")]
        public async Task<IActionResult> GetGroups([FromRoute]int applicationId)
        {
            return Ok(await _groupManager.GetGroups(applicationId));
        }

        [HttpPut]
        [Route("ChangeGroupName")]
        public async Task<IActionResult> ChangeGroupName([FromBody] Group group)
        {
            return Ok(await _groupManager.ChangeGroupName(group));
        }

        /// <summary>
        /// To Delete the group and move its texts and images to a new groupd if newGroupId is not 0.
        /// </summary>
        /// <param name="currentGroupId">Group Id of group to be deleted.</param>
        /// <param name="newGroupId">Group Id of new group to which texts and images are to be moved.</param>
        /// <returns>IActionResult</returns>
        [HttpDelete]
        [Route("DeleteGroup")]
        public async Task<IActionResult> DeleteGroup([FromQuery] int currentGroupId, [FromQuery] int newGroupId)
        {
            return Ok(await _groupManager.DeleteGroup(currentGroupId, newGroupId));
        }

        /// <summary>
        /// To change the order(Sno) of the groups.
        /// </summary>
        /// <param name="groupsOrderDTO">Information regarding the order of the groups.</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Route("ChangeGroupOrder")]
        public async Task<IActionResult> ChangeGroupOrder([FromBody] GroupsOrderDTO groupsOrderDTO)
        {
            bool result = false;

            if (ModelState.IsValid)
            {
                //Get the group with the applicatoin id.
                var groups = await _groupRepository.List(group => group.ApplicationId == groupsOrderDTO.ApplicationId);

                if (groups != null)
                {
                    foreach (var group in groups)
                    {
                        foreach (var item in groupsOrderDTO.GroupsOrder)
                        {
                            if (item.GroupId == group.Id)
                            {
                                group.Sno = item.Sno;
                            }
                        }
                    }

                    await _groupRepository.EditMultipleAsync(groups);

                    result = true;
                }
            }
            else
            {
                result = false;
            }

            return Ok(result);
        }

        /// <summary>
        /// Gets the number of texts which which are not related to any group for an application.
        /// </summary>
        /// <param name="applicationId">Application Id of the application.</param>
        /// <returns>IActionResult</returns>
        [HttpGet]
        [Route("OrphanTextsCount/{applicationId}")]
        public async Task<IActionResult> GetOrphanTextsCount([FromRoute]int applicationId)
        {
            return Ok(await _groupManager.GetOrphanTextsCount(applicationId));
        }

        [HttpGet]
        [Route("OrphanPdfsCount/{applicationId}")]
        public async Task<IActionResult> OrphanPdfsCount([FromRoute]int applicationId)
        {
            return Ok(await _groupManager.GetOrphanPdfsCount(applicationId));
        }

        [HttpPost]
        [Route("Group")]
        public async Task<IActionResult> AddGroup([FromBody] Group group)
        {
            return Ok(await _groupManager.AddGroup(group));
        }

        /// <summary>
        /// To Get all the groups, related texts and translation for an application Id.
        /// </summary>
        /// <param name="applicationId">Application Id of the application.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GroupsAndTranslations/{applicationId}/{isText}")]
        public async Task<IActionResult> GetGroupsAndTranslations([FromRoute] int applicationId, [FromRoute] bool isText)
        {
            if (isText)
            {
                return Ok(await _groupManager.GetGroupsAndTranslations(applicationId));
            }
            else
            {
                return Ok(await _groupManager.GetGroupsAndPdfTranslations(applicationId));
            }

        }

        /// <summary>
        /// To Get all the sections, related texts and translation for an application Id.
        /// </summary>
        /// <param name="applicationId">Application Id of the application.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SectionsAndTranslations/{applicationId}/{isText}")]
        public async Task<IActionResult> GetSectionsAndTranslations([FromRoute] int applicationId, [FromRoute] bool isText)
        {
            if (isText)
            {
                return Ok(await _sectionManager.GetSectionsAndTextTranslations(applicationId));
            }
            else
            {
                return Ok(await _sectionManager.GetSectionAndPdfTranslations(applicationId));
            }

        }

        [HttpPut]
        [Route("RemoveTextFromGroup/{textId}")]
        public async Task<IActionResult> RemoveTextFromGroup([FromRoute] int textId)
        {
            return Ok(await _groupManager.RemoveTextFromGroup(textId));
        }

        [HttpPut]
        [Route("RemovePdfFromGroup/{pdfId}")]
        public async Task<IActionResult> RemovePdfFromGroup([FromRoute] int pdfId)
        {
            return Ok(await _groupManager.RemovePdfFromGroup(pdfId));
        }

        /// <summary>
        /// To change the group Id of a Text.
        /// </summary>
        /// <param name="newGroupId">New groupId for the text.</param>
        /// <param name="textId">Id of the Text.</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Route("MoveTextToAnotherGroup")]
        public async Task<IActionResult> MoveTextToAnotherGroup([FromQuery] int newGroupId, [FromQuery] int textId)
        {
            return Ok(await _groupManager.MoveTextToAnotherGroup(newGroupId, textId));
        }

        [HttpPut]
        [Route("MovePdfToAnotherGroup")]
        public async Task<IActionResult> MovePdfToAnotherGroup([FromQuery] int newGroupId, [FromQuery] int pdfId)
        {
            return Ok(await _groupManager.MovePdfToAnotherGroup(newGroupId, pdfId));
        }

        /// <summary>
        /// To change the group Ids of more than one text.
        /// </summary>
        /// <param name="texts">Information regarding Texts for which groupId is to be changed.</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Route("ChangeTextsGroupId")]
        public async Task<IActionResult> UpdateTextsGroupId([FromBody] IEnumerable<Text> texts)
        {
            return Ok(await _groupManager.UpdateTextsGroupId(texts));
        }

        [HttpPut]
        [Route("ChangePdfsGroupId")]
        public async Task<IActionResult> UpdatePdfsGroupId([FromBody] IEnumerable<Pdf> Pdfs)
        {
            return Ok(await _groupManager.UpdatePdfsGroupId(Pdfs));
        }

        /// <summary>
        /// To update the GroupId for a single Text.
        /// </summary>
        /// <param name="text">Text for which groupId has to be updated.</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Route("ChangeTextGroupId")]
        public async Task<IActionResult> UpdateTextGroupId([FromBody] Text text)
        {
            return Ok(await _groupManager.UpdateTextGroupId(text));
        }

        [HttpPut]
        [Route("ChangePdfGroupId")]
        public async Task<IActionResult> UpdatePdfGroupId([FromBody] Pdf pdf)
        {
            return Ok(await _groupManager.UpdatePdfGroupId(pdf));
        }
    }
}