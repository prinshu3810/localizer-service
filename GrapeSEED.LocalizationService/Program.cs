﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GrapeSEED.LocalizationService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
             .UseKestrel(o => {
                 o.Limits.KeepAliveTimeout = TimeSpan.FromHours(10);
                 o.Limits.MaxRequestBodySize = null;
                 o.Limits.RequestHeadersTimeout = TimeSpan.FromHours(10);
             })
                .UseStartup<Startup>();
    }
}
