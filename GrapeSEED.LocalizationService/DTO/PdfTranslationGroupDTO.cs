﻿using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Controllers
{
    public class PdfTranslationGroupDTO
    {
        public Pdf Pdf { get; set; }

        public PdfTranslation PdfTranslation { get; set; }
    }
}