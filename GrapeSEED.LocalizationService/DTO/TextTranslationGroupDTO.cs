﻿using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Controllers
{
    public class TextTranslationGroupDTO
    {
        public Text Text { get; set; }

        public TextTranslation Translation { get; set; }
    }
}