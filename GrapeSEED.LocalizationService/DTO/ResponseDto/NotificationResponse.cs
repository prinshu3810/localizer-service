﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO.ResponseDto
{
    public class NotificationResponse
    {
        public int ApplicationId { get; set; }
        public int SectionId { get; set; }
        public int LanguageId { get; set; }
        public int NotificationCount { get; set; }

    }
}
