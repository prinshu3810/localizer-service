﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class TextEditDTO
    {
        public int SectionId { get; set; }
        public int TextId { get; set; }
        public int LanguageId { get; set; }
        public string Translation { get; set; }
        public int ReleaseId { get; set; }
    }
}
