﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class GroupNoteDTO
    {
        public int GroupId { get; set; }
        public string Note { get; set; }
    }
}
