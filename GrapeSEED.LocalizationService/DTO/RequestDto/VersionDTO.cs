﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class VersionDTO
    {
        public int Id { get; set; }
        public int ReleaseId { get; set; }
    }
}
