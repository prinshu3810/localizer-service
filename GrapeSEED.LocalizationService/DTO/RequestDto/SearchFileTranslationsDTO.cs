﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class SearchFileTranslationsDTO
    {
        public string Keyword { get; set; }
        public string SectionIds { get; set; }
        public string GroupIds { get; set; }
        public int LanguageId { get; set; }
        public int RoleId { get; set; }
    }
}
