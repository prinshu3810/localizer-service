﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class GroupsOrderDTO
    {
        [Required]
        public int ApplicationId { get; set; }
        public IEnumerable<GroupsOrder> GroupsOrder { get; set; }
    }

    public class GroupsOrder
    {
        [Required]
        [Key]
        public int GroupId { get; set; }

        [Required]
        [Key]
        public int Sno { get; set; }
    }
}
