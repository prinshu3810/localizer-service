﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class GroupDTO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupDTO"/> class.
        /// </summary>
        public GroupDTO()
        {
            Texts = new HashSet<TextDto>();
        }

        /// <summary>
        /// Gets or sets the id of the Group.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the application id of the Group.
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the name of the Group.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a note related to the Group.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Group is visible.
        /// </summary>
        public bool IsVisible { get; set; } = true;

        /// <summary>
        /// Gets or sets the texts belonging to the Group.
        /// </summary>
        public IEnumerable<TextDto> Texts { get; set; }

        /// <summary>
        /// Converts data transfer object to its equivalent api model.
        /// </summary>
        /// <returns>GroupApiModel</returns>
        internal GroupApiModel ToApiModel()
        {
            return new GroupApiModel()
            {
                Id = Id,
                Name = Name,
                ApplicationId = ApplicationId,
                Note = Note,
                IsVisible = IsVisible,
                Texts = Texts.Select(text => text.ToApiModel())
            };
        }
    }
}
