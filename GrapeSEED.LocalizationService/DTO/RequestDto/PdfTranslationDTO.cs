﻿using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.Models
{
    public class PdfTranslationDTO
    {
        public int? PdfTranslationId { get; set; }

        public int LanguageId { get; set; }

        public string Key { get; set; }

        public int SectionId { get; set; }

        public int StatusId { get; set; }

        public string Path { get; set; }

        public int PdfId { get; set; }

        public bool IsVisible { get; set; } = true;

        internal PdfTranslationApiModel ToApiModel()
        {
            return new PdfTranslationApiModel()
            {
                PdfTranslationId = PdfTranslationId,
                LanguageId = LanguageId,
                Key = Key,
                SectionId = SectionId,
                StatusId = StatusId,
                Path = Path,
                PdfId = PdfId,
                IsVisible = IsVisible
            };
        }
    }
}
