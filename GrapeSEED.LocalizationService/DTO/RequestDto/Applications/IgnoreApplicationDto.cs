﻿using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Controllers;
using GrapeSEED.LocalizationService.Domain.Shared.Models;
using GrapeSEED.LocalizationService.Models;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto.Applications
{
    public class IgnoreApplicationDto
    {
        public IgnoreApplicationDto()
        {
            Sections = new HashSet<SectionDto>();
            Groups = new HashSet<GroupDTO>();
            TextTranslations = new HashSet<TextTranslationDTO>();
            PdfTranslations = new HashSet<PdfTranslationDTO>();
        }

        public int ApplicationId { get; set; }

        public bool IsVisible { get; set; }

        public int LanguageId { get; set; }

        public IEnumerable<SectionDto> Sections { get; set; }

        public IEnumerable<TextTranslationDTO> TextTranslations { get; set; }

        public IEnumerable<PdfTranslationDTO> PdfTranslations { get; set; }
        public IEnumerable<GroupDTO> Groups { get; set; }

        internal IgnoreApiModel ToApiModel()
        {
            return new IgnoreApiModel()
            {
                ApplicationId = ApplicationId,
                IsVisible = IsVisible,
                LanguageId = LanguageId,
                Sections = Sections.Select(section => section.ToApiModel()),
                TextTranslations = TextTranslations.Select(translation => translation.ToApiModel()),
                PdfTranslations = PdfTranslations.Select(translation => translation.ToApiModel()),
                Groups = Groups.Select(group => group.ToApiModel())
            };
        }
    }
}
