﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto.Applications
{
    public class EditApplicationDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditApplicationDto"/> class.
        /// </summary>
        public EditApplicationDto()
        {
            SectionsToDelete = new HashSet<int>();
            NewSections = new HashSet<SectionDto>();
            SectionsToUpload = new HashSet<SectionDto>();
            SectionsToEdit = new HashSet<SectionDto>();
        }

        /// <summary>
        /// Gets or sets the Id of the application.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the collection of section Ids to be deleted.
        /// </summary>
        public IEnumerable<int> SectionsToDelete { get; set; }

        /// <summary>
        /// Gets or sets the sections to be edited.
        /// </summary>
        public IEnumerable<SectionDto> SectionsToEdit { get; set; }

        /// <summary>
        /// Gets or sets the new sections to be added to the application.
        /// </summary>
        public IEnumerable<SectionDto> NewSections { get; set; }

        /// <summary>
        /// Gets or sets the sections to be uploaded to the application.
        /// </summary>
        public IEnumerable<SectionDto> SectionsToUpload { get; set; }

        /// <summary>
        /// Gets or sets the translation type id of the application.
        /// <para>1 if text, 2 if pdf.</para>
        /// </summary>
        public int TranslationTypeId { get; set; }

        public bool IsVisible { get; set; } = true;

        public bool RemoveEnglishForOtherLang { get; set; }
    }

    /// <summary>
    /// Application model extensions.
    /// </summary>
    internal static class ApplicationModelExtensions
    {
        /// <summary>
        /// Converts data transfer object to its equivalent api model.
        /// </summary>
        /// <returns>ApplicationInfoApiModel</returns>
        internal static ApplicationInfoApiModel ToApiModel(this EditApplicationDto dto)
        {
            return new ApplicationInfoApiModel()
            {
                ApplicationId = dto.Id,
                Name = dto.Name,
                SectionsToAdd = dto.NewSections.Select(section => section.ToApiModel()),
                SectionsToEdit = dto.SectionsToEdit.Select(section => section.ToApiModel()),
                SectionsToDelete = dto.SectionsToDelete,
                SectionsToUpload = dto.SectionsToUpload.Select(section => section.ToApiModel()),
                TranslationTypeId = dto.TranslationTypeId,
                IsVisible = dto.IsVisible,
                RemoveEnglishForOtherLang = dto.RemoveEnglishForOtherLang
            };
        }
    }
}
