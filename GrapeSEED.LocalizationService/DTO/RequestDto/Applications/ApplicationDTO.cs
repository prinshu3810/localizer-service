﻿using System.Collections.Generic;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Models
{
    public class ApplicationDTO
    {
        public ApplicationDTO()
        {
            Sections = new List<Section>();
            Groups = new List<Group>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsVisible { get; set; } = true;

        public bool IsLocked { get; set; }

        public int TranslationTypeId { get; set; }

        public bool AllowPublish { get; set; }

        public List<Section> Sections { get; set; }

        public List<Group> Groups { get; set; }
    }
}
