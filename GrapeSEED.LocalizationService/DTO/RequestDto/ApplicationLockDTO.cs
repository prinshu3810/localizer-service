﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.DTO
{
    public class ApplicationLockDTO
    {
        public int Id { get; set; }

        public bool IsLocked { get; set; }
    }
}
