﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GrapeSEED.LocalizationService.Models
{
    public class UserDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public bool Disabled { get; set; }
    }

    public class ChangePasswordDTO
    {   
        [Required]
        public string Username { get; set; }
        [Required]        
        public string CurrentPassword { get; set; }
        [Required]
        [MinLength(7)]
        [MaxLength(30)]
        public string NewPassword { get; set; }
    }

    public class ActivateAccountDTO
    {
        [Required]
        [MinLength(8)]
        public string NewPassword { get; set; }
    }

    public class LanguageRoleDTO
    {
        [Required]
        public List<int> languageIds;

        [Required]
        public List<int> roleIds;
    }

    public class InviteUserDTO
    {
        public string Name { get; set; }
        [Required]
        public List<int> languageIds;

        [Required]
        public List<int> roleIds;
    }
}