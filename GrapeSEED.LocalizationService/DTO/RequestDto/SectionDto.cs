﻿using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class SectionDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SectionDto"/> class.
        /// </summary>
        public SectionDto()
        {
            Texts = new HashSet<TextDto>();
        }

        /// <summary>
        /// Gets or sets the id of the section.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the application id of the section.
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the name of the section.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display name of the section.
        /// <para>This is the actual name that has to be displayed on the screen for the section</para>
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets a note related to the section.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the section is visible.
        /// </summary>
        public bool IsVisible { get; set; } = true;

        /// <summary>
        /// Gets or sets the texts belonging to the section.
        /// </summary>
        public IEnumerable<TextDto> Texts { get; set; }

        /// <summary>
        /// Converts data transfer object to its equivalent api model.
        /// </summary>
        /// <returns>SectionApiModel</returns>
        internal SectionApiModel ToApiModel()
        {
            return new SectionApiModel()
            {
                Id = Id,
                Name = Name,
                DisplayName = DisplayName,
                ApplicationId = ApplicationId,
                Note = Note,
                IsVisible = IsVisible,
                Texts = Texts.Select(text => text.ToApiModel())
            };
        }
    }
}
