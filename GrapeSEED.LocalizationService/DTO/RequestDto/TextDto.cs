﻿using System.Collections.Generic;
using System.Linq;
using GrapeSEED.LocalizationService.Controllers;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.DTO.RequestDto
{
    public class TextDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextDto"/> class.
        /// </summary>
        public TextDto()
        {
            TextTranslations = new HashSet<TextTranslationDTO>();
        }

        /// <summary>
        /// Gets or sets the id of the section.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the section id of the section.
        /// </summary>
        public int SectionId { get; set; }

        /// <summary>
        /// Gets or sets the key of the text.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the text translations.
        /// </summary>
        public IEnumerable<TextTranslationDTO> TextTranslations { get; set; }

        /// <summary>
        /// Converts data transfer object to its equivalent api model.
        /// </summary>
        /// <returns>TextApiModel</returns>
        internal TextApiModel ToApiModel()
        {
            return new TextApiModel()
            {
                Id = Id,
                SectionId = SectionId,
                Key = Key,
                TextTranslations = TextTranslations.Select(translation => translation.ToApiModel())
            };
        }
    }
}