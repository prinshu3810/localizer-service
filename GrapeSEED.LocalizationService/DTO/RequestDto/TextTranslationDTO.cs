﻿using System;
using GrapeSEED.LocalizationService.Domain.Shared.Models;

namespace GrapeSEED.LocalizationService.Controllers
{
    public class TextTranslationDTO
    {
        public int? RoleId { get; set; }

        public int? TranslationId { get; set; }

        public int LanguageId { get; set; }

        public string Key { get; set; }

        public int SectionId { get; set; }

        public int StatusId { get; set; }

        public string Translation { get; set; }

        public int TextId { get; set; }

        public bool IsVisible { get; set; } = true;
        public int ReleaseId { get; set; }

        internal TextTranslationApiModel ToApiModel()
        {
            return new TextTranslationApiModel()
            {
                Id = Convert.ToInt32(TranslationId),
                TextId = TextId,
                LanguageId = LanguageId,
                StatusId = StatusId,
                Translation = Translation,
                IsVisible = IsVisible
            };
        }
    }
}