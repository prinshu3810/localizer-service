﻿using System;
using GrapeSEED.LocalizationService.Azure.Service;
using GrapeSEED.LocalizationService.Domain.Implementation;
using GrapeSEED.LocalizationService.Foundation;
using GrapeSEED.LocalizationService.Infrastructure.Filters;
using GrapeSEED.LocalizationService.Repository.Implementation;
using GrapeSEED.LocalizationService.Repository.Shared;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace GrapeSEED.LocalizationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IConfigurationSection Settings { get { return Configuration.GetSection("Settings"); } }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectString = Environment.GetEnvironmentVariable("LocalizerConnectionString") ?? Configuration.GetConnectionString("LocalizerConnectionString");

            services
                .AddDbContextPool<gclocalizationdbContext>(options => options.UseSqlServer(connectString))
                .AddSingleton(_ => Configuration)
                .Configure<FormOptions>(option =>
                {
                    option.ValueLengthLimit = int.MaxValue;
                    option.MultipartBodyLengthLimit = int.MaxValue;
                    option.MultipartHeadersLengthLimit = int.MaxValue;
                })
                .Configure<Domain.Implementation.StorageSettingOptions>(Configuration.GetSection("StorageSettings"))
                .Configure<Azure.Service.StorageSettingOptions>(Configuration.GetSection("StorageSettings"))
                .Configure<NotificationSettingOptions>(Configuration.GetSection("NotificationSettings"))
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.Configure<AuthMessageSenderOptions>(Settings);
            services.AddSwaggerGen((options) =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v3", new Info { Title = "Localizer API", Version = "v3" });
            });

            services
                .AddLogging()
                .AddMvc(options => 
                {
                    options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services
                .RegisterRepositoryServices()
                .RegisterDomainServices()
                .RegisterAzureServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(actions =>
            {
                actions.SwaggerEndpoint("/swagger/v3/swagger.json", "Localizer API V3");
            });

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
