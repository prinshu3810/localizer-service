﻿using System;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public abstract class AuditedEntityBase : AuditedEntityBase<int>
    {
    }

    public abstract class AuditedEntityBase<TKey> : EntityBase<TKey>, IAuditedEntityBase where TKey : IEquatable<TKey>
    {
        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }

    public interface IAuditedEntityBase : IEntityBase
    {
        DateTime CreatedDate { get; set; }

        DateTime? UpdatedDate { get; set; }
    }
}
