﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class PdfTranslation : AuditedEntityBase
    {
        public int PdfId { get; set; }
        public int StatusId { get; set; }
        public int LanguageId { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public bool? IsVisible { get; set; }

        public Language Language { get; set; }
        public Pdf Pdf { get; set; }
        public Status Status { get; set; }
    }
}
