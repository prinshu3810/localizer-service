﻿using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class NotificationStatus : EntityBase
    {
        public NotificationStatus()
        {
            Notification = new HashSet<Notification>();
        }

        public string Name { get; set; }

        public ICollection<Notification> Notification { get; set; }
    }
}
