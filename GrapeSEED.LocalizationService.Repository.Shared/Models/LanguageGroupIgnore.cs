﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public class LanguageGroupIgnore : IEntityBase
    {
        public int LanguageId { get; set; }
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Language Language { get; set; }
    }
}
