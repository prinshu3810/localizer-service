﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Version : EntityBase
    {
        public Version()
        {
            Pdf = new HashSet<Pdf>();
            Text = new HashSet<Text>();
        }

        public int Id { get; set; }
        public string VersionName { get; set; }

        public virtual ICollection<Pdf> Pdf { get; set; }
        public virtual ICollection<Text> Text { get; set; }
    }
}
