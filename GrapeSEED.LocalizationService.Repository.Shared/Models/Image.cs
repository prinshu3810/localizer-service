﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Image : EntityBase
    {
        public int SectionId { get; set; }
        public string Path { get; set; }

        public Section Section { get; set; }
    }
}
