﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class GroupImage : EntityBase
    {
        public int GroupId { get; set; }
        public string Path { get; set; }

        public virtual Group Group { get; set; }
    }
}
