﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class UserApplication : IEntityBase
    {
        public int UserId { get; set; }
        public int ApplicationId { get; set; }

        public Application Application { get; set; }
        public User User { get; set; }
    }
}
