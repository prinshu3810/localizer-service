﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Section : EntityBase
    {
        public Section()
        {
            Image = new HashSet<Image>();
            LanguageSectionIgnore = new HashSet<LanguageSectionIgnore>();
            Pdf = new HashSet<Pdf>();
            Text = new HashSet<Text>();
        }

        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public string Note { get; set; }
        public string DisplayName { get; set; }

        public Application Application { get; set; }
        public ICollection<Image> Image { get; set; }
        public ICollection<LanguageSectionIgnore> LanguageSectionIgnore { get; set; }
        public ICollection<Pdf> Pdf { get; set; }
        public ICollection<Text> Text { get; set; }
    }
}
