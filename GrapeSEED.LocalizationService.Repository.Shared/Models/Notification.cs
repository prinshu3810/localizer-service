﻿using System;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Notification
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public string Recipients { get; set; }
        public int RoleId { get; set; }
        public int NotificationStatusId { get; set; }
        public DateTime CreatedDate { get; set; }

        public NotificationStatus NotificationStatus { get; set; }
        public Role Role { get; set; }
    }
}
