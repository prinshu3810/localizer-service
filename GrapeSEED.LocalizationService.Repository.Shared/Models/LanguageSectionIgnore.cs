﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class LanguageSectionIgnore : IEntityBase
    {
        public int LanguageId { get; set; }
        public int SectionId { get; set; }

        public Language Language { get; set; }
        public Section Section { get; set; }
    }
}
