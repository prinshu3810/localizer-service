﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class User : EntityBase<int>
    {
        public User()
        {
            UserApplication = new HashSet<UserApplication>();
            UserLanguage = new HashSet<UserLanguage>();
            UserRole = new HashSet<UserRole>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public bool? IsPasswordVerified { get; set; }
        public bool? NotificationsEnabled { get; set; }
        public bool Disabled { get; set; }
        public bool? InvitationStatus { get; set; }
        public string InvitationCode { get; set; }
        public string Name { get; set; }


        public ICollection<UserApplication> UserApplication { get; set; }
        public ICollection<UserLanguage> UserLanguage { get; set; }
        public ICollection<UserRole> UserRole { get; set; }
    }
}
