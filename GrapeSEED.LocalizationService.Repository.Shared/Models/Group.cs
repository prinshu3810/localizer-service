﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Group : EntityBase
    {
        public Group()
        {
            GroupImage = new HashSet<GroupImage>();
            LanguageGroupIgnore = new HashSet<LanguageGroupIgnore>();
            Pdf = new HashSet<Pdf>();
            Text = new HashSet<Text>();
        }

        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public int Sno { get; set; }
        public string Note { get; set; }

        public virtual Application Application { get; set; }
        public virtual ICollection<GroupImage> GroupImage { get; set; }
        public virtual ICollection<LanguageGroupIgnore> LanguageGroupIgnore { get; set; }
        public virtual ICollection<Pdf> Pdf { get; set; }
        public virtual ICollection<Text> Text { get; set; }
    }
}
