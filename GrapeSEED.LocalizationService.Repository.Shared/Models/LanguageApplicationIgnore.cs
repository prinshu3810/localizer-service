﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class LanguageApplicationIgnore : IEntityBase
    {
        public int LanguageId { get; set; }
        public int ApplicationId { get; set; }

        public Application Application { get; set; }
        public Language Language { get; set; }
    }
}
