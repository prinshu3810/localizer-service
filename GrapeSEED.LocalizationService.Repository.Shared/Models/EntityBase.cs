﻿using System;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public abstract class EntityBase<TKey> : IEntityBase where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }

    public abstract class EntityBase : EntityBase<int>
    {
    }

    public interface IEntityBase
    {
    }
}
