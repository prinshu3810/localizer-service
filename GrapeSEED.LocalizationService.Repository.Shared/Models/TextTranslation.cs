﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class TextTranslation : AuditedEntityBase
    {
        public int TextId { get; set; }
        public int StatusId { get; set; }
        public int LanguageId { get; set; }
        public string Translation { get; set; }
        public string PreviousTranslation { get; set; }
        public bool? IsVisible { get; set; }

        public Language Language { get; set; }
        public Status Status { get; set; }
        public Text Text { get; set; }
    }
}
