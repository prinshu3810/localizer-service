﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Role : EntityBase
    {
        public Role()
        {
            Notification = new HashSet<Notification>();
            UserRole = new HashSet<UserRole>();
        }

        public string Name { get; set; }

        public ICollection<Notification> Notification { get; set; }
        public ICollection<UserRole> UserRole { get; set; }
    }
}
