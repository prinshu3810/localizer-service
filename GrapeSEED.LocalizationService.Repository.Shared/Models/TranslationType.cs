﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class TranslationType : EntityBase
    {
        public TranslationType()
        {
            Application = new HashSet<Application>();
        }

        public string Value { get; set; }

        public ICollection<Application> Application { get; set; }
    }
}
