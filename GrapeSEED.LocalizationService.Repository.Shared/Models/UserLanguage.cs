﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class UserLanguage : IEntityBase
    {
        public int UserId { get; set; }
        public int LanguageId { get; set; }

        public Language Language { get; set; }
        public User User { get; set; }
    }
}
