﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Pdf : EntityBase
    {
        public Pdf()
        {
            PdfTranslation = new HashSet<PdfTranslation>();
        }

        public string Key { get; set; }
        public int SectionId { get; set; }
        public int? GroupId { get; set; }
        public int VersionId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Section Section { get; set; }
        public virtual Version Version { get; set; }
        public virtual ICollection<PdfTranslation> PdfTranslation { get; set; }
    }
}
