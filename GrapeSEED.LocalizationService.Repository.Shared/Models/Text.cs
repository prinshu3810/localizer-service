﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Text : EntityBase
    {
        public Text()
        {
            TextTranslation = new HashSet<TextTranslation>();
        }
     
        public string Key { get; set; }
        public int SectionId { get; set; }
        public int Sno { get; set; }
        public int? GroupId { get; set; }
        public int VersionId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Section Section { get; set; }
        public virtual Version Version { get; set; }
        public virtual ICollection<TextTranslation> TextTranslation { get; set; }
    }
}
