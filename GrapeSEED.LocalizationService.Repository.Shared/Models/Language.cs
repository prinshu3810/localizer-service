﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Language : EntityBase
    {
        public Language()
        {
            LanguageApplicationIgnore = new HashSet<LanguageApplicationIgnore>();
            LanguageGroupIgnore = new HashSet<LanguageGroupIgnore>();
            LanguageSectionIgnore = new HashSet<LanguageSectionIgnore>();
            PdfTranslation = new HashSet<PdfTranslation>();
            TextTranslation = new HashSet<TextTranslation>();
            UserLanguage = new HashSet<UserLanguage>();
        }

        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public string LanguageCode { get; set; }

        public ICollection<LanguageApplicationIgnore> LanguageApplicationIgnore { get; set; }
        public ICollection<LanguageGroupIgnore> LanguageGroupIgnore { get; set; }
        public ICollection<LanguageSectionIgnore> LanguageSectionIgnore { get; set; }
        public ICollection<PdfTranslation> PdfTranslation { get; set; }
        public ICollection<TextTranslation> TextTranslation { get; set; }
        public ICollection<UserLanguage> UserLanguage { get; set; }
    }
}
