﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Application : EntityBase
    {
        public Application()
        {
            Group = new HashSet<Group>();
            LanguageApplicationIgnore = new HashSet<LanguageApplicationIgnore>();
            Section = new HashSet<Section>();
            UserApplication = new HashSet<UserApplication>();
        }

        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public int TranslationTypeId { get; set; }
        public bool IsLocked { get; set; }
        public bool AllowPublish { get; set; }

        public TranslationType TranslationType { get; set; }
        public virtual ICollection<Group> Group { get; set; }
        public ICollection<LanguageApplicationIgnore> LanguageApplicationIgnore { get; set; }
        public ICollection<Section> Section { get; set; }
        public ICollection<UserApplication> UserApplication { get; set; }
    }
}
