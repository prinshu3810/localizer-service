﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class UserRole : IEntityBase
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}
