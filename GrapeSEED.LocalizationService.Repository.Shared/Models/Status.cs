﻿using System;
using System.Collections.Generic;

namespace GrapeSEED.LocalizationService.Repository.Shared.Models
{
    public partial class Status : EntityBase
    {
        public Status()
        {
            PdfTranslation = new HashSet<PdfTranslation>();
            TextTranslation = new HashSet<TextTranslation>();
        }

        public string Name { get; set; }
        public ICollection<PdfTranslation> PdfTranslation { get; set; }
        public ICollection<TextTranslation> TextTranslation { get; set; }
    }
}
