﻿using GrapeSEED.LocalizationService.Repository.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Repository.Shared
{
    public partial class gclocalizationdbContext : DbContext
    {
        public gclocalizationdbContext(DbContextOptions<gclocalizationdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Application> Application { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupImage> GroupImage { get; set; }
        public virtual DbSet<Image> Image { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<LanguageApplicationIgnore> LanguageApplicationIgnore { get; set; }
        public virtual DbSet<LanguageGroupIgnore> LanguageGroupIgnore { get; set; }
        public virtual DbSet<LanguageSectionIgnore> LanguageSectionIgnore { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<NotificationStatus> NotificationStatus { get; set; }
        public virtual DbSet<Pdf> Pdf { get; set; }
        public virtual DbSet<PdfTranslation> PdfTranslation { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Text> Text { get; set; }
        public virtual DbSet<TextTranslation> TextTranslation { get; set; }
        public virtual DbSet<TranslationType> TranslationType { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserApplication> UserApplication { get; set; }
        public virtual DbSet<UserLanguage> UserLanguage { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Version> Version { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:gl-test.database.windows.net,1433;Initial Catalog=gclocalizationdb1;Persist Security Info=False;User ID=gssys;Password=xA123456;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=60;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Application>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.TranslationType)
                    .WithMany(p => p.Application)
                    .HasForeignKey(d => d.TranslationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TranslationTypeApplication");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.IsVisible)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Sno).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ApplicationGroup");
            });

            modelBuilder.Entity<GroupImage>(entity =>
            {
                entity.Property(e => e.Path).IsRequired();

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupImage)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupImageGroup");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.Property(e => e.Path).IsRequired();

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.Image)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Section_Image");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.HasIndex(e => e.LanguageCode)
                    .HasName("UQ__Language__8B8C8A34831040A7")
                    .IsUnique();

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LanguageApplicationIgnore>(entity =>
            {
                entity.HasKey(e => new { e.LanguageId, e.ApplicationId });

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.LanguageApplicationIgnore)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Application_LanguageApplicationIgnore");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.LanguageApplicationIgnore)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Language_LanguageApplicationIgnore");
            });

            modelBuilder.Entity<LanguageGroupIgnore>(entity =>
            {
                entity.HasKey(e => new { e.LanguageId, e.GroupId });

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.LanguageGroupIgnore)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LanguageGroupIgnore_Group");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.LanguageGroupIgnore)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LanguageGroupIgnore_Language");
            });

            modelBuilder.Entity<LanguageSectionIgnore>(entity =>
            {
                entity.HasKey(e => new { e.LanguageId, e.SectionId });

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.LanguageSectionIgnore)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Language_LanguageSectionIgnore");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.LanguageSectionIgnore)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Application_LanguageSectionIgnore");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Recipients).IsRequired();

                entity.HasOne(d => d.NotificationStatus)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.NotificationStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificationStatus_Notification");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Role_Notification");
            });

            modelBuilder.Entity<NotificationStatus>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Pdf>(entity =>
            {
                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Pdf)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_PdfGroup");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.Pdf)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Section_Pdf");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Pdf)
                    .HasForeignKey(d => d.VersionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pdf_Version");
            });

            modelBuilder.Entity<PdfTranslation>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.FileName).HasMaxLength(300);

                entity.Property(e => e.IsVisible)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Path).IsRequired();

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.PdfTranslation)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Language_PdfTranslations");

                entity.HasOne(d => d.Pdf)
                    .WithMany(p => p.PdfTranslation)
                    .HasForeignKey(d => d.PdfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pdf_PdfTranslations");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PdfTranslation)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PdfTranslations_Status");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.Section)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ApplicationSections");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Text>(entity =>
            {
                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Sno)
                    .HasColumnName("SNo")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Text)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_TextGroup");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.Text)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Section_Text");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Text)
                    .HasForeignKey(d => d.VersionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Text_Version");
            });

            modelBuilder.Entity<TextTranslation>(entity =>
            {
                entity.HasIndex(e => new { e.TextId, e.LanguageId })
                    .HasName("uq_yourtablename")
                    .IsUnique();

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.IsVisible)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PreviousTranslation)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Translation).IsRequired();

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.TextTranslation)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Language");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TextTranslation)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Translations_Status");

                entity.HasOne(d => d.Text)
                    .WithMany(p => p.TextTranslation)
                    .HasForeignKey(d => d.TextId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Text_Translations");
            });

            modelBuilder.Entity<TranslationType>(entity =>
            {
                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.Username)
                    .HasName("UQ_User_Username")
                    .IsUnique();

                entity.Property(e => e.InvitationCode)
                    .HasMaxLength(26)
                    .IsUnicode(false);

                entity.Property(e => e.InvitationStatus)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsPasswordVerified)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.NotificationsEnabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);
                
                entity.Property(e => e.Name)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserApplication>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ApplicationId });

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.UserApplication)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Application_UserApplication");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserApplication)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_UserApplication");
            });

            modelBuilder.Entity<UserLanguage>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LanguageId });

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.UserLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserLang");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLanguage)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LangUSer");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Role_User");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Role");
            });

            modelBuilder.Entity<Version>(entity =>
            {
                entity.Property(e => e.VersionName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

        }
    }
}
