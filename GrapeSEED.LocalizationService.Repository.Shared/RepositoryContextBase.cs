﻿using Microsoft.EntityFrameworkCore;

namespace GrapeSEED.LocalizationService.Repository.Shared
{
    public abstract class RepositoryContextBase
    {
        public RepositoryContextBase(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        public DbContext DbContext { get; private set; }
    }
}
