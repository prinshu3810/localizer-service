﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrapeSEED.LocalizationService.Repository.Shared.Models;

namespace GrapeSEED.LocalizationService.Repository.Shared.Contracts
{
    public interface IApplicationRepository : IGeneralRepository<Application>
    {
        Task<dynamic> GetUngroupedKeysCount(); 
    }
}
