using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Repository.Shared
{
    public interface ICrudRepository<TEntity> : ICrudRepository<TEntity, int>
    {

    }

    public interface ICrudRepository<TEntity, TKey> 
    {
        TEntity GetById(TKey id);

        Task<TEntity> GetByIdAsync(TKey id);

        Task<TEntity> GetById(params object[] ids);

        IQueryable<TEntity> GetAll();

        Task<IQueryable<TEntity>> GetAllAsync();

        Task AddAsync(TEntity entity);

        Task AddMultipleAsync(IEnumerable<TEntity> entity);

        Task DeleteAsync(TEntity entity);

        Task DeleteAsync(TKey id);

        Task DeleteMultipleAsync(IEnumerable<TEntity> entities);

        Task EditAsync(TEntity entity, params string[] properties);

        Task EditAsync(TEntity entity);

        Task EditMultipleAsync(IEnumerable<TEntity> entities);
        Task EditMultipleAsync(IEnumerable<TEntity> entities, params string[] properties);

    }
}