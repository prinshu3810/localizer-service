﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeSEED.LocalizationService.Repository.Shared.Contracts
{
    public interface IGeneralRepository<TEntity> : IGeneralRepository<TEntity, int>, ICrudRepository<TEntity>
    {
    }

    public interface IGeneralRepository<TEntity, TKey> : ICrudRepository<TEntity, TKey>
    {
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> List(Expression<Func<TEntity, bool>> predicate = null, bool needTracking = false);

        Task<IQueryable<TEntity>> GetAllIncludingAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] propertySelectors);

        IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] propertySelectors);

        Task SaveChangesAsync();
    }
}
