﻿using System.Data.Common;

namespace GrapeSEED.LocalizationService.Repository.Shared.Contracts
{
    public interface IStoredProcedureRespository
    {
        DbCommand GetStoredProcedure(string name, params (string, object)[] nameValueParams);
        DbCommand GetStoredProcedure(string name);
    }
}
