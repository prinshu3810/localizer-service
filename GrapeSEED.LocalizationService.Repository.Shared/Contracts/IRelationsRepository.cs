﻿namespace GrapeSEED.LocalizationService.Repository.Shared.Contracts
{
    public interface IRelationsRepository<TEntity> : IGeneralRepository<TEntity>
    {
    }
}
